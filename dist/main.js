!function(e){var t={};function s(i){if(t[i])return t[i].exports;var a=t[i]={i:i,l:!1,exports:{}};return e[i].call(a.exports,a,a.exports,s),a.l=!0,a.exports}s.m=e,s.c=t,s.d=function(e,t,i){s.o(e,t)||Object.defineProperty(e,t,{enumerable:!0,get:i})},s.r=function(e){"undefined"!=typeof Symbol&&Symbol.toStringTag&&Object.defineProperty(e,Symbol.toStringTag,{value:"Module"}),Object.defineProperty(e,"__esModule",{value:!0})},s.t=function(e,t){if(1&t&&(e=s(e)),8&t)return e;if(4&t&&"object"==typeof e&&e&&e.__esModule)return e;var i=Object.create(null);if(s.r(i),Object.defineProperty(i,"default",{enumerable:!0,value:e}),2&t&&"string"!=typeof e)for(var a in e)s.d(i,a,function(t){return e[t]}.bind(null,a));return i},s.n=function(e){var t=e&&e.__esModule?function(){return e.default}:function(){return e};return s.d(t,"a",t),t},s.o=function(e,t){return Object.prototype.hasOwnProperty.call(e,t)},s.p="",s(s.s=0)}([function(e,t,s){"use strict";s.r(t);
/**
 * @license
 * Copyright (c) 2017 The Polymer Project Authors. All rights reserved.
 * This code may only be used under the BSD style license found at
 * http://polymer.github.io/LICENSE.txt
 * The complete set of authors may be found at
 * http://polymer.github.io/AUTHORS.txt
 * The complete set of contributors may be found at
 * http://polymer.github.io/CONTRIBUTORS.txt
 * Code distributed by Google as part of the polymer project is also
 * subject to an additional IP rights grant found at
 * http://polymer.github.io/PATENTS.txt
 */
const i="undefined"!=typeof window&&null!=window.customElements&&void 0!==window.customElements.polyfillWrapFlushCallback,a=(e,t,s=null)=>{for(;t!==s;){const s=t.nextSibling;e.removeChild(t),t=s}},r=`{{lit-${String(Math.random()).slice(2)}}}`,o=`\x3c!--${r}--\x3e`,n=new RegExp(`${r}|${o}`);class l{constructor(e,t){this.parts=[],this.element=t;const s=[],i=[],a=document.createTreeWalker(t.content,133,null,!1);let o=0,l=-1,u=0;const{strings:h,values:{length:m}}=e;for(;u<m;){const e=a.nextNode();if(null!==e){if(l++,1===e.nodeType){if(e.hasAttributes()){const t=e.attributes,{length:s}=t;let i=0;for(let e=0;e<s;e++)c(t[e].name,"$lit$")&&i++;for(;i-- >0;){const t=h[u],s=p.exec(t)[2],i=s.toLowerCase()+"$lit$",a=e.getAttribute(i);e.removeAttribute(i);const r=a.split(n);this.parts.push({type:"attribute",index:l,name:s,strings:r}),u+=r.length-1}}"TEMPLATE"===e.tagName&&(i.push(e),a.currentNode=e.content)}else if(3===e.nodeType){const t=e.data;if(t.indexOf(r)>=0){const i=e.parentNode,a=t.split(n),r=a.length-1;for(let t=0;t<r;t++){let s,r=a[t];if(""===r)s=d();else{const e=p.exec(r);null!==e&&c(e[2],"$lit$")&&(r=r.slice(0,e.index)+e[1]+e[2].slice(0,-"$lit$".length)+e[3]),s=document.createTextNode(r)}i.insertBefore(s,e),this.parts.push({type:"node",index:++l})}""===a[r]?(i.insertBefore(d(),e),s.push(e)):e.data=a[r],u+=r}}else if(8===e.nodeType)if(e.data===r){const t=e.parentNode;null!==e.previousSibling&&l!==o||(l++,t.insertBefore(d(),e)),o=l,this.parts.push({type:"node",index:l}),null===e.nextSibling?e.data="":(s.push(e),l--),u++}else{let t=-1;for(;-1!==(t=e.data.indexOf(r,t+1));)this.parts.push({type:"node",index:-1}),u++}}else a.currentNode=i.pop()}for(const e of s)e.parentNode.removeChild(e)}}const c=(e,t)=>{const s=e.length-t.length;return s>=0&&e.slice(s)===t},u=e=>-1!==e.index,d=()=>document.createComment(""),p=/([ \x09\x0a\x0c\x0d])([^\0-\x1F\x7F-\x9F "'>=/]+)([ \x09\x0a\x0c\x0d]*=[ \x09\x0a\x0c\x0d]*(?:[^ \x09\x0a\x0c\x0d"'`<>=]*|"[^"]*|'[^']*))$/;function h(e,t){const{element:{content:s},parts:i}=e,a=document.createTreeWalker(s,133,null,!1);let r=g(i),o=i[r],n=-1,l=0;const c=[];let u=null;for(;a.nextNode();){n++;const e=a.currentNode;for(e.previousSibling===u&&(u=null),t.has(e)&&(c.push(e),null===u&&(u=e)),null!==u&&l++;void 0!==o&&o.index===n;)o.index=null!==u?-1:o.index-l,r=g(i,r),o=i[r]}c.forEach(e=>e.parentNode.removeChild(e))}const m=e=>{let t=11===e.nodeType?0:1;const s=document.createTreeWalker(e,133,null,!1);for(;s.nextNode();)t++;return t},g=(e,t=-1)=>{for(let s=t+1;s<e.length;s++){const t=e[s];if(u(t))return s}return-1};
/**
 * @license
 * Copyright (c) 2017 The Polymer Project Authors. All rights reserved.
 * This code may only be used under the BSD style license found at
 * http://polymer.github.io/LICENSE.txt
 * The complete set of authors may be found at
 * http://polymer.github.io/AUTHORS.txt
 * The complete set of contributors may be found at
 * http://polymer.github.io/CONTRIBUTORS.txt
 * Code distributed by Google as part of the polymer project is also
 * subject to an additional IP rights grant found at
 * http://polymer.github.io/PATENTS.txt
 */
const y=new WeakMap,b=e=>"function"==typeof e&&y.has(e),v={},f={};
/**
 * @license
 * Copyright (c) 2017 The Polymer Project Authors. All rights reserved.
 * This code may only be used under the BSD style license found at
 * http://polymer.github.io/LICENSE.txt
 * The complete set of authors may be found at
 * http://polymer.github.io/AUTHORS.txt
 * The complete set of contributors may be found at
 * http://polymer.github.io/CONTRIBUTORS.txt
 * Code distributed by Google as part of the polymer project is also
 * subject to an additional IP rights grant found at
 * http://polymer.github.io/PATENTS.txt
 */
class _{constructor(e,t,s){this.__parts=[],this.template=e,this.processor=t,this.options=s}update(e){let t=0;for(const s of this.__parts)void 0!==s&&s.setValue(e[t]),t++;for(const e of this.__parts)void 0!==e&&e.commit()}_clone(){const e=i?this.template.element.content.cloneNode(!0):document.importNode(this.template.element.content,!0),t=[],s=this.template.parts,a=document.createTreeWalker(e,133,null,!1);let r,o=0,n=0,l=a.nextNode();for(;o<s.length;)if(r=s[o],u(r)){for(;n<r.index;)n++,"TEMPLATE"===l.nodeName&&(t.push(l),a.currentNode=l.content),null===(l=a.nextNode())&&(a.currentNode=t.pop(),l=a.nextNode());if("node"===r.type){const e=this.processor.handleTextExpression(this.options);e.insertAfterNode(l.previousSibling),this.__parts.push(e)}else this.__parts.push(...this.processor.handleAttributeExpressions(l,r.name,r.strings,this.options));o++}else this.__parts.push(void 0),o++;return i&&(document.adoptNode(e),customElements.upgrade(e)),e}}
/**
 * @license
 * Copyright (c) 2017 The Polymer Project Authors. All rights reserved.
 * This code may only be used under the BSD style license found at
 * http://polymer.github.io/LICENSE.txt
 * The complete set of authors may be found at
 * http://polymer.github.io/AUTHORS.txt
 * The complete set of contributors may be found at
 * http://polymer.github.io/CONTRIBUTORS.txt
 * Code distributed by Google as part of the polymer project is also
 * subject to an additional IP rights grant found at
 * http://polymer.github.io/PATENTS.txt
 */const S=window.trustedTypes&&trustedTypes.createPolicy("lit-html",{createHTML:e=>e}),C=` ${r} `;class w{constructor(e,t,s,i){this.strings=e,this.values=t,this.type=s,this.processor=i}getHTML(){const e=this.strings.length-1;let t="",s=!1;for(let i=0;i<e;i++){const e=this.strings[i],a=e.lastIndexOf("\x3c!--");s=(a>-1||s)&&-1===e.indexOf("--\x3e",a+1);const n=p.exec(e);t+=null===n?e+(s?C:o):e.substr(0,n.index)+n[1]+n[2]+"$lit$"+n[3]+r}return t+=this.strings[e],t}getTemplateElement(){const e=document.createElement("template");let t=this.getHTML();return void 0!==S&&(t=S.createHTML(t)),e.innerHTML=t,e}}
/**
 * @license
 * Copyright (c) 2017 The Polymer Project Authors. All rights reserved.
 * This code may only be used under the BSD style license found at
 * http://polymer.github.io/LICENSE.txt
 * The complete set of authors may be found at
 * http://polymer.github.io/AUTHORS.txt
 * The complete set of contributors may be found at
 * http://polymer.github.io/CONTRIBUTORS.txt
 * Code distributed by Google as part of the polymer project is also
 * subject to an additional IP rights grant found at
 * http://polymer.github.io/PATENTS.txt
 */
const x=e=>null===e||!("object"==typeof e||"function"==typeof e),A=e=>Array.isArray(e)||!(!e||!e[Symbol.iterator]);class T{constructor(e,t,s){this.dirty=!0,this.element=e,this.name=t,this.strings=s,this.parts=[];for(let e=0;e<s.length-1;e++)this.parts[e]=this._createPart()}_createPart(){return new E(this)}_getValue(){const e=this.strings,t=e.length-1,s=this.parts;if(1===t&&""===e[0]&&""===e[1]){const e=s[0].value;if("symbol"==typeof e)return String(e);if("string"==typeof e||!A(e))return e}let i="";for(let a=0;a<t;a++){i+=e[a];const t=s[a];if(void 0!==t){const e=t.value;if(x(e)||!A(e))i+="string"==typeof e?e:String(e);else for(const t of e)i+="string"==typeof t?t:String(t)}}return i+=e[t],i}commit(){this.dirty&&(this.dirty=!1,this.element.setAttribute(this.name,this._getValue()))}}class E{constructor(e){this.value=void 0,this.committer=e}setValue(e){e===v||x(e)&&e===this.value||(this.value=e,b(e)||(this.committer.dirty=!0))}commit(){for(;b(this.value);){const e=this.value;this.value=v,e(this)}this.value!==v&&this.committer.commit()}}class N{constructor(e){this.value=void 0,this.__pendingValue=void 0,this.options=e}appendInto(e){this.startNode=e.appendChild(d()),this.endNode=e.appendChild(d())}insertAfterNode(e){this.startNode=e,this.endNode=e.nextSibling}appendIntoPart(e){e.__insert(this.startNode=d()),e.__insert(this.endNode=d())}insertAfterPart(e){e.__insert(this.startNode=d()),this.endNode=e.endNode,e.endNode=this.startNode}setValue(e){this.__pendingValue=e}commit(){if(null===this.startNode.parentNode)return;for(;b(this.__pendingValue);){const e=this.__pendingValue;this.__pendingValue=v,e(this)}const e=this.__pendingValue;e!==v&&(x(e)?e!==this.value&&this.__commitText(e):e instanceof w?this.__commitTemplateResult(e):e instanceof Node?this.__commitNode(e):A(e)?this.__commitIterable(e):e===f?(this.value=f,this.clear()):this.__commitText(e))}__insert(e){this.endNode.parentNode.insertBefore(e,this.endNode)}__commitNode(e){this.value!==e&&(this.clear(),this.__insert(e),this.value=e)}__commitText(e){const t=this.startNode.nextSibling,s="string"==typeof(e=null==e?"":e)?e:String(e);t===this.endNode.previousSibling&&3===t.nodeType?t.data=s:this.__commitNode(document.createTextNode(s)),this.value=e}__commitTemplateResult(e){const t=this.options.templateFactory(e);if(this.value instanceof _&&this.value.template===t)this.value.update(e.values);else{const s=new _(t,e.processor,this.options),i=s._clone();s.update(e.values),this.__commitNode(i),this.value=s}}__commitIterable(e){Array.isArray(this.value)||(this.value=[],this.clear());const t=this.value;let s,i=0;for(const a of e)s=t[i],void 0===s&&(s=new N(this.options),t.push(s),0===i?s.appendIntoPart(this):s.insertAfterPart(t[i-1])),s.setValue(a),s.commit(),i++;i<t.length&&(t.length=i,this.clear(s&&s.endNode))}clear(e=this.startNode){a(this.startNode.parentNode,e.nextSibling,this.endNode)}}class ${constructor(e,t,s){if(this.value=void 0,this.__pendingValue=void 0,2!==s.length||""!==s[0]||""!==s[1])throw new Error("Boolean attributes can only contain a single expression");this.element=e,this.name=t,this.strings=s}setValue(e){this.__pendingValue=e}commit(){for(;b(this.__pendingValue);){const e=this.__pendingValue;this.__pendingValue=v,e(this)}if(this.__pendingValue===v)return;const e=!!this.__pendingValue;this.value!==e&&(e?this.element.setAttribute(this.name,""):this.element.removeAttribute(this.name),this.value=e),this.__pendingValue=v}}class P extends T{constructor(e,t,s){super(e,t,s),this.single=2===s.length&&""===s[0]&&""===s[1]}_createPart(){return new k(this)}_getValue(){return this.single?this.parts[0].value:super._getValue()}commit(){this.dirty&&(this.dirty=!1,this.element[this.name]=this._getValue())}}class k extends E{}let O=!1;(()=>{try{const e={get capture(){return O=!0,!1}};window.addEventListener("test",e,e),window.removeEventListener("test",e,e)}catch(e){}})();class q{constructor(e,t,s){this.value=void 0,this.__pendingValue=void 0,this.element=e,this.eventName=t,this.eventContext=s,this.__boundHandleEvent=e=>this.handleEvent(e)}setValue(e){this.__pendingValue=e}commit(){for(;b(this.__pendingValue);){const e=this.__pendingValue;this.__pendingValue=v,e(this)}if(this.__pendingValue===v)return;const e=this.__pendingValue,t=this.value,s=null==e||null!=t&&(e.capture!==t.capture||e.once!==t.once||e.passive!==t.passive),i=null!=e&&(null==t||s);s&&this.element.removeEventListener(this.eventName,this.__boundHandleEvent,this.__options),i&&(this.__options=D(e),this.element.addEventListener(this.eventName,this.__boundHandleEvent,this.__options)),this.value=e,this.__pendingValue=v}handleEvent(e){"function"==typeof this.value?this.value.call(this.eventContext||this.element,e):this.value.handleEvent(e)}}const D=e=>e&&(O?{capture:e.capture,passive:e.passive,once:e.once}:e.capture)
/**
 * @license
 * Copyright (c) 2017 The Polymer Project Authors. All rights reserved.
 * This code may only be used under the BSD style license found at
 * http://polymer.github.io/LICENSE.txt
 * The complete set of authors may be found at
 * http://polymer.github.io/AUTHORS.txt
 * The complete set of contributors may be found at
 * http://polymer.github.io/CONTRIBUTORS.txt
 * Code distributed by Google as part of the polymer project is also
 * subject to an additional IP rights grant found at
 * http://polymer.github.io/PATENTS.txt
 */;function U(e){let t=j.get(e.type);void 0===t&&(t={stringsArray:new WeakMap,keyString:new Map},j.set(e.type,t));let s=t.stringsArray.get(e.strings);if(void 0!==s)return s;const i=e.strings.join(r);return s=t.keyString.get(i),void 0===s&&(s=new l(e,e.getTemplateElement()),t.keyString.set(i,s)),t.stringsArray.set(e.strings,s),s}const j=new Map,M=new WeakMap;
/**
 * @license
 * Copyright (c) 2017 The Polymer Project Authors. All rights reserved.
 * This code may only be used under the BSD style license found at
 * http://polymer.github.io/LICENSE.txt
 * The complete set of authors may be found at
 * http://polymer.github.io/AUTHORS.txt
 * The complete set of contributors may be found at
 * http://polymer.github.io/CONTRIBUTORS.txt
 * Code distributed by Google as part of the polymer project is also
 * subject to an additional IP rights grant found at
 * http://polymer.github.io/PATENTS.txt
 */const R=new
/**
 * @license
 * Copyright (c) 2017 The Polymer Project Authors. All rights reserved.
 * This code may only be used under the BSD style license found at
 * http://polymer.github.io/LICENSE.txt
 * The complete set of authors may be found at
 * http://polymer.github.io/AUTHORS.txt
 * The complete set of contributors may be found at
 * http://polymer.github.io/CONTRIBUTORS.txt
 * Code distributed by Google as part of the polymer project is also
 * subject to an additional IP rights grant found at
 * http://polymer.github.io/PATENTS.txt
 */
class{handleAttributeExpressions(e,t,s,i){const a=t[0];if("."===a){return new P(e,t.slice(1),s).parts}if("@"===a)return[new q(e,t.slice(1),i.eventContext)];if("?"===a)return[new $(e,t.slice(1),s)];return new T(e,t,s).parts}handleTextExpression(e){return new N(e)}};
/**
 * @license
 * Copyright (c) 2017 The Polymer Project Authors. All rights reserved.
 * This code may only be used under the BSD style license found at
 * http://polymer.github.io/LICENSE.txt
 * The complete set of authors may be found at
 * http://polymer.github.io/AUTHORS.txt
 * The complete set of contributors may be found at
 * http://polymer.github.io/CONTRIBUTORS.txt
 * Code distributed by Google as part of the polymer project is also
 * subject to an additional IP rights grant found at
 * http://polymer.github.io/PATENTS.txt
 */"undefined"!=typeof window&&(window.litHtmlVersions||(window.litHtmlVersions=[])).push("1.3.0");const I=(e,...t)=>new w(e,t,"html",R),L=(e,t)=>`${e}--${t}`;let F=!0;void 0===window.ShadyCSS?F=!1:void 0===window.ShadyCSS.prepareTemplateDom&&(console.warn("Incompatible ShadyCSS version detected. Please update to at least @webcomponents/webcomponentsjs@2.0.2 and @webcomponents/shadycss@1.3.1."),F=!1);const z=e=>t=>{const s=L(t.type,e);let i=j.get(s);void 0===i&&(i={stringsArray:new WeakMap,keyString:new Map},j.set(s,i));let a=i.stringsArray.get(t.strings);if(void 0!==a)return a;const o=t.strings.join(r);if(a=i.keyString.get(o),void 0===a){const s=t.getTemplateElement();F&&window.ShadyCSS.prepareTemplateDom(s,e),a=new l(t,s),i.keyString.set(o,a)}return i.stringsArray.set(t.strings,a),a},B=["html","svg"],G=new Set,J=(e,t,s)=>{G.add(e);const i=s?s.element:document.createElement("template"),a=t.querySelectorAll("style"),{length:r}=a;if(0===r)return void window.ShadyCSS.prepareTemplateStyles(i,e);const o=document.createElement("style");for(let e=0;e<r;e++){const t=a[e];t.parentNode.removeChild(t),o.textContent+=t.textContent}(e=>{B.forEach(t=>{const s=j.get(L(t,e));void 0!==s&&s.keyString.forEach(e=>{const{element:{content:t}}=e,s=new Set;Array.from(t.querySelectorAll("style")).forEach(e=>{s.add(e)}),h(e,s)})})})(e);const n=i.content;s?function(e,t,s=null){const{element:{content:i},parts:a}=e;if(null==s)return void i.appendChild(t);const r=document.createTreeWalker(i,133,null,!1);let o=g(a),n=0,l=-1;for(;r.nextNode();){l++;for(r.currentNode===s&&(n=m(t),s.parentNode.insertBefore(t,s));-1!==o&&a[o].index===l;){if(n>0){for(;-1!==o;)a[o].index+=n,o=g(a,o);return}o=g(a,o)}}}(s,o,n.firstChild):n.insertBefore(o,n.firstChild),window.ShadyCSS.prepareTemplateStyles(i,e);const l=n.querySelector("style");if(window.ShadyCSS.nativeShadow&&null!==l)t.insertBefore(l.cloneNode(!0),t.firstChild);else if(s){n.insertBefore(o,n.firstChild);const e=new Set;e.add(o),h(s,e)}};window.JSCompiler_renameProperty=(e,t)=>e;const V={toAttribute(e,t){switch(t){case Boolean:return e?"":null;case Object:case Array:return null==e?e:JSON.stringify(e)}return e},fromAttribute(e,t){switch(t){case Boolean:return null!==e;case Number:return null===e?null:Number(e);case Object:case Array:return JSON.parse(e)}return e}},H=(e,t)=>t!==e&&(t==t||e==e),W={attribute:!0,type:String,converter:V,reflect:!1,hasChanged:H};class X extends HTMLElement{constructor(){super(),this.initialize()}static get observedAttributes(){this.finalize();const e=[];return this._classProperties.forEach((t,s)=>{const i=this._attributeNameForProperty(s,t);void 0!==i&&(this._attributeToPropertyMap.set(i,s),e.push(i))}),e}static _ensureClassProperties(){if(!this.hasOwnProperty(JSCompiler_renameProperty("_classProperties",this))){this._classProperties=new Map;const e=Object.getPrototypeOf(this)._classProperties;void 0!==e&&e.forEach((e,t)=>this._classProperties.set(t,e))}}static createProperty(e,t=W){if(this._ensureClassProperties(),this._classProperties.set(e,t),t.noAccessor||this.prototype.hasOwnProperty(e))return;const s="symbol"==typeof e?Symbol():"__"+e,i=this.getPropertyDescriptor(e,s,t);void 0!==i&&Object.defineProperty(this.prototype,e,i)}static getPropertyDescriptor(e,t,s){return{get(){return this[t]},set(i){const a=this[e];this[t]=i,this.requestUpdateInternal(e,a,s)},configurable:!0,enumerable:!0}}static getPropertyOptions(e){return this._classProperties&&this._classProperties.get(e)||W}static finalize(){const e=Object.getPrototypeOf(this);if(e.hasOwnProperty("finalized")||e.finalize(),this.finalized=!0,this._ensureClassProperties(),this._attributeToPropertyMap=new Map,this.hasOwnProperty(JSCompiler_renameProperty("properties",this))){const e=this.properties,t=[...Object.getOwnPropertyNames(e),..."function"==typeof Object.getOwnPropertySymbols?Object.getOwnPropertySymbols(e):[]];for(const s of t)this.createProperty(s,e[s])}}static _attributeNameForProperty(e,t){const s=t.attribute;return!1===s?void 0:"string"==typeof s?s:"string"==typeof e?e.toLowerCase():void 0}static _valueHasChanged(e,t,s=H){return s(e,t)}static _propertyValueFromAttribute(e,t){const s=t.type,i=t.converter||V,a="function"==typeof i?i:i.fromAttribute;return a?a(e,s):e}static _propertyValueToAttribute(e,t){if(void 0===t.reflect)return;const s=t.type,i=t.converter;return(i&&i.toAttribute||V.toAttribute)(e,s)}initialize(){this._updateState=0,this._updatePromise=new Promise(e=>this._enableUpdatingResolver=e),this._changedProperties=new Map,this._saveInstanceProperties(),this.requestUpdateInternal()}_saveInstanceProperties(){this.constructor._classProperties.forEach((e,t)=>{if(this.hasOwnProperty(t)){const e=this[t];delete this[t],this._instanceProperties||(this._instanceProperties=new Map),this._instanceProperties.set(t,e)}})}_applyInstanceProperties(){this._instanceProperties.forEach((e,t)=>this[t]=e),this._instanceProperties=void 0}connectedCallback(){this.enableUpdating()}enableUpdating(){void 0!==this._enableUpdatingResolver&&(this._enableUpdatingResolver(),this._enableUpdatingResolver=void 0)}disconnectedCallback(){}attributeChangedCallback(e,t,s){t!==s&&this._attributeToProperty(e,s)}_propertyToAttribute(e,t,s=W){const i=this.constructor,a=i._attributeNameForProperty(e,s);if(void 0!==a){const e=i._propertyValueToAttribute(t,s);if(void 0===e)return;this._updateState=8|this._updateState,null==e?this.removeAttribute(a):this.setAttribute(a,e),this._updateState=-9&this._updateState}}_attributeToProperty(e,t){if(8&this._updateState)return;const s=this.constructor,i=s._attributeToPropertyMap.get(e);if(void 0!==i){const e=s.getPropertyOptions(i);this._updateState=16|this._updateState,this[i]=s._propertyValueFromAttribute(t,e),this._updateState=-17&this._updateState}}requestUpdateInternal(e,t,s){let i=!0;if(void 0!==e){const a=this.constructor;s=s||a.getPropertyOptions(e),a._valueHasChanged(this[e],t,s.hasChanged)?(this._changedProperties.has(e)||this._changedProperties.set(e,t),!0!==s.reflect||16&this._updateState||(void 0===this._reflectingProperties&&(this._reflectingProperties=new Map),this._reflectingProperties.set(e,s))):i=!1}!this._hasRequestedUpdate&&i&&(this._updatePromise=this._enqueueUpdate())}requestUpdate(e,t){return this.requestUpdateInternal(e,t),this.updateComplete}async _enqueueUpdate(){this._updateState=4|this._updateState;try{await this._updatePromise}catch(e){}const e=this.performUpdate();return null!=e&&await e,!this._hasRequestedUpdate}get _hasRequestedUpdate(){return 4&this._updateState}get hasUpdated(){return 1&this._updateState}performUpdate(){if(!this._hasRequestedUpdate)return;this._instanceProperties&&this._applyInstanceProperties();let e=!1;const t=this._changedProperties;try{e=this.shouldUpdate(t),e?this.update(t):this._markUpdated()}catch(t){throw e=!1,this._markUpdated(),t}e&&(1&this._updateState||(this._updateState=1|this._updateState,this.firstUpdated(t)),this.updated(t))}_markUpdated(){this._changedProperties=new Map,this._updateState=-5&this._updateState}get updateComplete(){return this._getUpdateComplete()}_getUpdateComplete(){return this._updatePromise}shouldUpdate(e){return!0}update(e){void 0!==this._reflectingProperties&&this._reflectingProperties.size>0&&(this._reflectingProperties.forEach((e,t)=>this._propertyToAttribute(t,this[t],e)),this._reflectingProperties=void 0),this._markUpdated()}updated(e){}firstUpdated(e){}}X.finalized=!0;const Y=Element.prototype;Y.msMatchesSelector||Y.webkitMatchesSelector;
/**
@license
Copyright (c) 2019 The Polymer Project Authors. All rights reserved.
This code may only be used under the BSD style license found at
http://polymer.github.io/LICENSE.txt The complete set of authors may be found at
http://polymer.github.io/AUTHORS.txt The complete set of contributors may be
found at http://polymer.github.io/CONTRIBUTORS.txt Code distributed by Google as
part of the polymer project is also subject to an additional IP rights grant
found at http://polymer.github.io/PATENTS.txt
*/
const K=window.ShadowRoot&&(void 0===window.ShadyCSS||window.ShadyCSS.nativeShadow)&&"adoptedStyleSheets"in Document.prototype&&"replace"in CSSStyleSheet.prototype,Q=Symbol();class Z{constructor(e,t){if(t!==Q)throw new Error("CSSResult is not constructable. Use `unsafeCSS` or `css` instead.");this.cssText=e}get styleSheet(){return void 0===this._styleSheet&&(K?(this._styleSheet=new CSSStyleSheet,this._styleSheet.replaceSync(this.cssText)):this._styleSheet=null),this._styleSheet}toString(){return this.cssText}}
/**
 * @license
 * Copyright (c) 2017 The Polymer Project Authors. All rights reserved.
 * This code may only be used under the BSD style license found at
 * http://polymer.github.io/LICENSE.txt
 * The complete set of authors may be found at
 * http://polymer.github.io/AUTHORS.txt
 * The complete set of contributors may be found at
 * http://polymer.github.io/CONTRIBUTORS.txt
 * Code distributed by Google as part of the polymer project is also
 * subject to an additional IP rights grant found at
 * http://polymer.github.io/PATENTS.txt
 */
(window.litElementVersions||(window.litElementVersions=[])).push("2.4.0");const ee={};class te extends X{static getStyles(){return this.styles}static _getUniqueStyles(){if(this.hasOwnProperty(JSCompiler_renameProperty("_styles",this)))return;const e=this.getStyles();if(Array.isArray(e)){const t=(e,s)=>e.reduceRight((e,s)=>Array.isArray(s)?t(s,e):(e.add(s),e),s),s=t(e,new Set),i=[];s.forEach(e=>i.unshift(e)),this._styles=i}else this._styles=void 0===e?[]:[e];this._styles=this._styles.map(e=>{if(e instanceof CSSStyleSheet&&!K){const t=Array.prototype.slice.call(e.cssRules).reduce((e,t)=>e+t.cssText,"");return new Z(String(t),Q)}return e})}initialize(){super.initialize(),this.constructor._getUniqueStyles(),this.renderRoot=this.createRenderRoot(),window.ShadowRoot&&this.renderRoot instanceof window.ShadowRoot&&this.adoptStyles()}createRenderRoot(){return this.attachShadow({mode:"open"})}adoptStyles(){const e=this.constructor._styles;0!==e.length&&(void 0===window.ShadyCSS||window.ShadyCSS.nativeShadow?K?this.renderRoot.adoptedStyleSheets=e.map(e=>e instanceof CSSStyleSheet?e:e.styleSheet):this._needsShimAdoptedStyleSheets=!0:window.ShadyCSS.ScopingShim.prepareAdoptedCssText(e.map(e=>e.cssText),this.localName))}connectedCallback(){super.connectedCallback(),this.hasUpdated&&void 0!==window.ShadyCSS&&window.ShadyCSS.styleElement(this)}update(e){const t=this.render();super.update(e),t!==ee&&this.constructor.render(t,this.renderRoot,{scopeName:this.localName,eventContext:this}),this._needsShimAdoptedStyleSheets&&(this._needsShimAdoptedStyleSheets=!1,this.constructor._styles.forEach(e=>{const t=document.createElement("style");t.textContent=e.cssText,this.renderRoot.appendChild(t)}))}render(){return ee}}te.finalized=!0,te.render=(e,t,s)=>{if(!s||"object"!=typeof s||!s.scopeName)throw new Error("The `scopeName` option is required.");const i=s.scopeName,r=M.has(t),o=F&&11===t.nodeType&&!!t.host,n=o&&!G.has(i),l=n?document.createDocumentFragment():t;if(((e,t,s)=>{let i=M.get(t);void 0===i&&(a(t,t.firstChild),M.set(t,i=new N(Object.assign({templateFactory:U},s))),i.appendInto(t)),i.setValue(e),i.commit()})(e,l,Object.assign({templateFactory:z(i)},s)),n){const e=M.get(l);M.delete(l);const s=e.value instanceof _?e.value.template:void 0;J(i,l,s),a(t,t.firstChild),t.appendChild(l),M.set(t,e)}!r&&o&&window.ShadyCSS.styleElement(t.host)};class se{async getUsers(e){let t=this.getUrlParams("https://fierce-reaches-83730.herokuapp.com/techU/v1/users",e),s={},i=await fetch(t);if(i.ok)if(200==i.status){let e=await i.json();s.objects=e,s.status=i.status}else s.objects=[],s.status=204;else s.objects=[],s.status=500;return s}async getUserById(e){let t="https://fierce-reaches-83730.herokuapp.com/techU/v1/users/"+e,s={},i=await fetch(t,{method:"GET",headers:{"Content-Type":"application/json"}});if(i.ok)if(200==i.status){let e=await i.json();s.objects=e,s.status=i.status}else s.objects=[],s.status=204;else s.objects=[],s.status=500;return s}async singIn(e){let t={},s=await fetch("https://fierce-reaches-83730.herokuapp.com/techU/v1/singin",{method:"POST",body:JSON.stringify(e),headers:{"Content-Type":"application/json"}});if(s.ok)if(200==s.status){let e=await s.json();t.json=e,t.status=s.status}else t.json={},t.status=204;else t.json={},t.status=500;return console.log(t),t}async updateUser(e,t){let s={},i="https://fierce-reaches-83730.herokuapp.com/techU/v1/users/"+t,a=await fetch(i,{method:"PUT",body:JSON.stringify(e),headers:{"Content-Type":"application/json"}});if(a.ok)if(200==a.status){let e=await a.json();s.json=e,s.status=a.status}else s.json="{}",s.status=a.status;else s.json="{}",s.status=a.status;return s}async deleteUser(e){let t={},s="https://fierce-reaches-83730.herokuapp.com/techU/v1/users/"+e,i=await fetch(s,{method:"DELETE",headers:{"Content-Type":"application/json"}});if(i.ok)if(204==i.status){let e={};t.json=e,t.status=i.status}else t.json="{}",t.status=i.status;else t.json="{}",t.status=i.status;return t}async insertUser2(e){return await fetch("https://fierce-reaches-83730.herokuapp.com/techU/v1/usersDB2",{method:"POST",body:JSON.stringify(e),headers:{"Content-Type":"application/json"}}).then(e=>({jsonRes:e.json(),status:e.status,statusMessage:e.statusText}))}async deleteUser2(e){let t="https://fierce-reaches-83730.herokuapp.com/techU/v1/usersDB/"+e;return await fetch(t,{method:"DELETE",headers:{"Content-Type":"application/json"}}).then(e=>({jsonRes:e.json(),status:e.status,statusMessage:e.statusText}))}getUrlParams(e,t){let s=Object.keys(t).map((function(e){return encodeURIComponent(e)+"="+encodeURIComponent(t[e])})).join("&");return 0===s.length?""+e:`${e}?${s}`}}let ie=()=>new se;class ae{_getTodayDate(){return this._formatDate(new Date)}_getPreMonthDate(){let e=new Date-62208e6;return this._formatDate(e)}_getStringDate(){var e=new Date,t=e.getDate(),s=e.getMonth()+1,i=e.getFullYear();return t<10&&(t="0"+t),s<10&&(s="0"+s),e=s+"-"+t+"-"+i,console.log(e),e=s+"/"+t+"/"+i,console.log(e),e=t+"-"+s+"-"+i,console.log(e),console.log(i+"/"+s+"/"+t),i+"-"+s+"-"+t}_formatDate(e){var t=new Date(e),s=""+(t.getMonth()+1),i=""+t.getDate(),a=t.getFullYear();return s.length<2&&(s="0"+s),i.length<2&&(i="0"+i),[a,s,i].join("-")}_zfill(e,t){var s=Math.abs(e),i=e.toString().length;return t<=i?e<0?"-"+s.toString():s.toString():e<0?"-"+"0".repeat(t-i)+s.toString():"0".repeat(t-i)+s.toString()}}let re=()=>new ae;customElements.define("cmp-user",class extends te{createRenderRoot(){return this}get userDm(){return ie()}get numberUtil(){return re()}static get properties(){return{id:{type:String},idClient:{type:String},firstName:{type:String},lastName:{type:String},email:{type:String},password:{type:String},documentType:{type:String},documentNumber:{type:String},mobilePhone:{type:String},address:{type:String},phone:{type:String},city:{type:String},country:{type:String},creationDate:{type:String},updateDate:{type:String},visibleAmounts:{type:Boolean},maximumAmountOperation:{type:String},type:{type:String},profileCode:{type:String},profileName:{type:String},employeeCode:{type:String},employeePosition:{type:String},disabledDate:{type:Boolean},message:{type:String},typeClass:{type:String},errors:{type:Array},isActiveSave:{type:Boolean},listCountry:{type:Array},listCity:{type:Array},listProfile:{type:Array},listDocumentType:{type:Array},listType:{type:Array}}}constructor(){super(),this.id="",this.idClient="",this.firstName="",this.lastName="",this.email="",this.password="",this.documentType="",this.documentNumber="",this.mobilePhone="",this.address="",this.phone="",this.city="",this.country="",this.creationDate="",this.updateDate="",this.visibleAmounts=!1,this.maximumAmountOperation="0.00",this.type="",this.profileCode="",this.profileName="",this.employeeCode="",this.employeePosition="",this.disabledDate=!0,this.typeMessage="",this.message="",this.typeClass="",this.errors=[],this.isActiveSave=!1,this.addEventListener("load-user",({detail:e})=>this._loadUser(e)),this.addEventListener("load-parameters",({detail:e})=>this._loadParameters(e)),this.listCountry=[],this.listCity=[],this.listProfile=[],this.listDocumentType=[],this.listType=[]}_loadParameters(){this.listCountry=JSON.parse(sessionStorage.getItem("parametersCountry")),this.listProfile=JSON.parse(sessionStorage.getItem("parametersProfiles")),this.listDocumentType=JSON.parse(sessionStorage.getItem("parametersDocumentType")),this.listDocumentType=JSON.parse(sessionStorage.getItem("parametersDocumentType")),this.listType=JSON.parse(sessionStorage.getItem("parametersType"))}_initLoading(){const e=this.parentElement.parentElement.parentElement.parentElement.querySelector("cmp-loading");if(null!=e){const t=new CustomEvent("init-loading",{detail:{},bubbles:!0,composed:!0});e.dispatchEvent(t)}}_endLoading(){const e=this.parentElement.parentElement.parentElement.parentElement.querySelector("cmp-loading");if(null!=e){const t=new CustomEvent("end-loading",{detail:{},bubbles:!0,composed:!0});e.dispatchEvent(t)}}_loadUser(e){this.id=e.id,this.idClient=e.id_client,this.firstName=e.first_name,this.lastName=e.last_name,this.email=e.email,this.password=e.password,this.documentType=e.document_type,this.documentNumber=e.document_number,this.mobilePhone=e.mobile_phone,this.address=e.address,this.phone=e.phone,this.city=e.city,this.country=e.country,this.creationDate=e.creation_date,this.updateDate=e.update_date,this.visibleAmounts=e.visible_amounts,this.maximumAmountOperation=e.maximum_amount_operations,this.type=e.type,this.profileCode=e.profile.code,this.profileName=e.profile.name,this.employeeCode=e.employee.code,this.employeePosition=e.employee.position,this.isActiveSave=!1,this._getLoadCity(this.country),this.querySelector("#document_type").value=this.documentType,this.querySelector("#country").value=this.country,this.querySelector("#profile_code").value=this.profileCode,this.querySelector("#type").value=this.type,this.querySelector("#city").value=this.city,this._setCity()}async _setCity(){await this.updateComplete,this.querySelector("#city").value=this.city}_getLoadCity(e){this.listCity=[];let t=e,s=JSON.parse(sessionStorage.getItem("parametersCity"));for(let e=0;e<s.length;e++)s[e].parameter_parent_code==t&&this.listCity.push(s[e])}_mapperUser(){let e={id:this.id,id_client:this.querySelector("#id_client").value,document_type:this.querySelector("#document_type").value,document_number:this.querySelector("#document_number").value,first_name:this.querySelector("#first_name").value,last_name:this.querySelector("#last_name").value,country:this.querySelector("#country").value,city:this.querySelector("#city").value,address:this.querySelector("#address").value,mobile_phone:this.querySelector("#mobile_phone").value,phone:this.querySelector("#phone").value,email:this.querySelector("#email").value,type:this.querySelector("#type").value,maximum_amount_operations:this.querySelector("#maximum_amount_operations").value,employee:{code:this.querySelector("#employee_code").value,position:this.querySelector("#employee_position").value},profile:{code:this.querySelector("#profile_code").value},visible_amounts:"on"==this.querySelector("#visible_amounts").value,creation_date:this.querySelector("#creation_date").value,update_date:this.numberUtil._getTodayDate()};return console.log("userObject",e),e}_errorOnUpdateUser(){let e=[],t=/^\d+$/,s=this._mapperUser();return""==s.id_client&&e.push({message:"El id de Cliente es requerido.",typeMessage:"¡Danger! ",typeClass:"alert alert-danger"}),""==s.document_type&&e.push({message:"El tipo de documento es requerido.",typeMessage:"¡Danger! ",typeClass:"alert alert-danger"}),""==s.document_number&&e.push({message:"El número de documento es requerido.",typeMessage:"¡Danger! ",typeClass:"alert alert-danger"}),""==s.first_name&&e.push({message:"Los nombres son requeridos",typeMessage:"¡Danger! ",typeClass:"alert alert-danger"}),""==s.last_name&&e.push({message:"Los apellidos son requeridos",typeMessage:"¡Danger! ",typeClass:"alert alert-danger"}),""==s.country&&e.push({message:"Es pais es requerido",typeMessage:"¡Danger! ",typeClass:"alert alert-danger"}),""==s.city&&e.push({message:"La ciudad es requerida",typeMessage:"¡Danger! ",typeClass:"alert alert-danger"}),""==s.address&&e.push({message:"La direccion es requerida",typeMessage:"¡Danger! ",typeClass:"alert alert-danger"}),""==s.mobile_phone&&e.push({message:"El númer de celular es requerido",typeMessage:"¡Danger! ",typeClass:"alert alert-danger"}),""==s.email&&e.push({message:"El email es requerido",typeMessage:"¡Danger! ",typeClass:"alert alert-danger"}),""==s.phone&&e.push({message:"El telefono fijo es requerido",typeMessage:"¡Danger! ",typeClass:"alert alert-danger"}),""==s.profile.code&&e.push({message:"El código de perfil es requerido",typeMessage:"¡Danger! ",typeClass:"alert alert-danger"}),""==s.type&&e.push({message:"El tipo de cliente es requerido.",typeMessage:"¡Danger! ",typeClass:"alert alert-danger"}),""==s.maximum_amount_operations&&e.push({message:"El monto máximo de operaciones es requerido.",typeMessage:"¡Danger! ",typeClass:"alert alert-danger"}),/^(([^<>()[\]\.,;:\s@\"]+(\.[^<>()[\]\.,;:\s@\"]+)*)|(\".+\"))@(([^<>()[\]\.,;:\s@\"]+\.)+[^<>()[\]\.,;:\s@\"]{2,})$/i.test(s.email)||e.push({message:"Ingrese un email valido.",typeMessage:"¡Danger! ",typeClass:"alert alert-danger"}),/^([0-9]+\.?[0-9]{0,2})$/.test(s.maximum_amount_operations)||e.push({message:"Ingrese un monto limite de operaciones valido.",typeMessage:"¡Danger! ",typeClass:"alert alert-danger"}),t.test(s.mobile_phone)||e.push({message:"Ingrese un numero de celular valido.",typeMessage:"¡Danger! ",typeClass:"alert alert-danger"}),t.test(s.phone)||e.push({message:"Ingrese un numero de telèfono fijo valido.",typeMessage:"¡Danger! ",typeClass:"alert alert-danger"}),e}async _updateUser(){this._initLoading();let e=this._errorOnUpdateUser();if(0===e.length){let e=this._mapperUser();200==(await this.userDm.updateUser(e,e.id)).status?(this.errors=[],this.errors.push({message:"El registro se actualizó correctamente.",typeMessage:"¡Success! ",typeClass:"alert alert-success"}),this._setEventReloadTableUser()):(this.errors=[],this.errors.push({message:"No se pudo actualizar el registro.",typeMessage:"Danger! ",typeClass:"alert alert-danger"}))}else this.errors=e;this._endLoading()}_setEventReloadTableUser(){let e=new CustomEvent("reload-table-user",{detail:{status:"200"}});this.dispatchEvent(e)}_cleanUser(){this.id="",this.idClient="",this.firstName="",this.lastName="",this.email="",this.password="",this.documentType="",this.documentNumber="",this.mobilePhone="",this.address="",this.phone="",this.city="",this.country="",this.creationDate="",this.updateDate="",this.visibleAmounts=!1,this.maximumAmountOperation=0,this.type="",this.profileCode="",this.profileName="",this.employeeCode="",this.employeePosition="",this.errors=[],this.isActiveSave=!1,this.listCity=[],this.querySelector("#document_type").value="",this.querySelector("#country").value="",this.querySelector("#profile_code").value="",this.querySelector("#type").value="",this.querySelector("#city").value=""}_exitUser(){this._cleanUser(),this.renderRoot.querySelector("#idDialogUser").style.display="none"}render(){return I`
      <div class="popup" id="idDialogUser">
        <div class="modal-dialog modal-xl" name="dialogUser">
          <div class="card">
          <div class="card-header text-dark bg-light text-uppercase font-weight-light">
          <h5 class="card-title">
            <svg class="bi" width="24" height="24" fill="currentColor">
              <use
                xlink:href="./resource/bootstrap-icons.svg#pencil-fill"
              />
            </svg>

            Usuario
          </h5>
        </div>
            <div class="card-body ">
              ${this.errors&&this.errors.map(e=>I`
                    <div class="${e.typeClass}">
                      <strong>${e.typeMessage}</strong> ${e.message}
                    </div>
                  `)}
              <form>
              <div class="form-row">
              <div class="form-group col-md-2">
                <label for="document_type">Tipo de documento</label>

                <select
                  name="document_type"
                  class="custom-select mr-sm-2"
                  id="document_type"
                  name="document_type"
                >
                  <option value="">--Seleccione uno--</option>
                  ${this.listDocumentType&&this.listDocumentType.map(e=>I`<option .value="${e.parameter_value}">
                        ${e.parameter_name}
                      </option>`)}
                </select>
              </div>
              <div class="form-group col-md-2">
                <label for="document_number">Nº de documento</label>
                <input
                  type="text"
                  class="form-control inputForm"
                  id="document_number"
                  name="document_number"
                  .value="${this.documentNumber}"
                />
              </div>
              <div class="form-group col-md-4">
                <label for="first_name">Nombres</label>
                <input
                  type="text"
                  class="form-control inputForm"
                  id="first_name"
                  name="first_name"
                  .value="${this.firstName}"
                />
              </div>

              <div class="form-group col-md-4">
                <label for="last_name">Apellidos</label>
                <input
                  type="text"
                  class="form-control inputForm"
                  id="last_name"
                  name="last_name"
                  .value="${this.lastName}"
                />
              </div>
            </div>
            <div class="form-row">
              <div class="form-group col-md-2">
                <label for="country">Pais</label>
                <select
                  name="country"
                  class="custom-select mr-sm-2"
                  id="country"
                  @input=${this._getLoadCity}
                >
                  <option value="">--Seleccione uno--</option>
                  ${this.listCountry&&this.listCountry.map(e=>I`<option .value="${e.parameter_value}">
                        ${e.parameter_name}
                      </option>`)}
                </select>
              </div>

              <div class="form-group col-md-2">
                <label for="city">Ciudad</label>
                <select
                  name="city"
                  class="custom-select mr-sm-2"
                  id="city"
                >
                  <option value="">--Seleccione uno--</option>
                  ${this.listCity&&this.listCity.map(e=>I`<option .value="${e.parameter_value}">
                        ${e.parameter_name}
                      </option>`)}
                </select>
              </div>
              <div class="form-group col-md-4">
                <label for="address">Dirección</label>
                <input
                  type="text"
                  class="form-control inputForm"
                  id="address"
                  name="address"
                  .value="${this.address}"
                />
              </div>
              <div class="form-group col-md-2">
                <label for="mobile_phone">N° Celular</label>
                <input
                  type="text"
                  class="form-control inputForm"
                  id="mobile_phone"
                  name="mobile_phone"
                  .value="${this.mobilePhone}"
                />
              </div>
              <div class="form-group col-md-2">
                <label for="phone">Teléfono fijo</label>
                <input
                  type="text"
                  class="form-control inputForm"
                  id="phone"
                  name="phone"
                  .value="${this.phone}"
                />
              </div>
              
            </div>
            <div class="form-row">
            <div class="form-group col-md-4">
              

            <label for="email">Email</label>
      <div class="input-group">
        <div class="input-group-prepend">
          <span class="input-group-text" id="validationTooltipUsernamePrepend">@</span>
        </div>
        <input type="text" class="form-control" id="email" .value="${this.email}" name="email" aria-describedby="validationTooltipUsernamePrepend" >
       
      </div>
              </div>
              
              <div class="form-group col-md-4">
                <label for="profile_code">Perfil</label>
                <select
                  name="profile_code"
                  class="custom-select mr-sm-2"
                  id="profile_code"
                >
                  <option value="">--Seleccione uno--</option>
                  ${this.listProfile&&this.listProfile.map(e=>I`<option .value="${e.parameter_value}">
                        ${e.parameter_name}
                      </option>`)}
                </select>
              </div>
              
              <div class="form-group col-md-2">
                <label for="type">Tipo cliente</label>
                <select
                  name="type"
                  class="custom-select mr-sm-2"
                  id="type"
                >
                  <option value="">--Seleccione uno--</option>
                  ${this.listType&&this.listType.map(e=>I`<option .value="${e.parameter_value}">
                        ${e.parameter_name}
                      </option>`)}
                </select>
              </div>
              <div class="form-group col-md-2">
                <label for="maximum_amount_operations">Monto Max. Ope.</label>
                <input 
                  type="text"
                  class="form-control inputForm"
                  id="maximum_amount_operations"
                  name="maximum_amount_operations"

                  data-inputmask="'alias': 'currency'"
                  .value="${this.maximumAmountOperation}"
                  
                />
              </div>
              
            </div>
            
            <div class="form-row">
            <div class="form-group col-md-2">
                <label for="employee_code">Cod. Empleado</label>
                <input
                  type="text"
                  class="form-control inputForm"
                  id="employee_code"
                  name="employee_code"
                  .value="${this.employeeCode}"
                />
              </div>
              <div class="form-group col-md-2">
                <label for="employee_position">Cargo</label>
                <input
                  type="text"
                  class="form-control inputForm"
                  id="employee_position"
                  name="employee_position"
                  .value="${this.employeePosition}"
                />
              </div>
            
              <div class="form-group col-md-2">
                <label for="id_client">Cod. Cliente</label>
                <input
                  type="text"
                  class="form-control inputForm"
                  id="id_client"
                  name="id_client"
                  .value="${this.idClient}"
                />
              </div>
              <div class="form-group col-md-2">
              <div class="custom-control custom-checkbox">
              <input class="form-check-input" type="checkbox" id="visible_amounts" ?checked="${this.visibleAmounts}" />
              <label class="form-check-label" for="visible_amounts">
                Activar visualizar montos.
              </label>
            </div>
        </div>   
        
        

        <div class="form-group col-md-2">
                <label for="creation_date">F. creación</label>
                <input ?disabled="${this.disabledDate}"
                  type="date"
                  id="creation_date"
                  name="creation_date"
                  class="form-control inputForm"
                  
                  .value="${this.creationDate}"
                />
              </div>
              <div class="form-group col-md-2">
                <label for="update_date">F. ultima modificación</label>
                <input  ?disabled="${this.disabledDate}"
                  type="date"
                  id="update_date"
                  name="update_date"
                  class="form-control inputForm"
                 
                  placeholder="yyyy-mm-dd"
                  .value="${this.updateDate}"
                />
              </div>
        
              </div>
          </form> 






              </form>
            </div>
            <div class="card-footer">
              <div class="card-body text-right">
               
                  <button
                    ?disabled="${this.isActiveSave}"
                    type="button"
                    class="btn btn-bbva btn-lg"
                    @click=${this._updateUser}
                  >
                    Guardar
                    <svg class="bi" width="24" height="24" fill="currentColor">
                      <use
                        xlink:href="./resource/bootstrap-icons.svg#box-arrow-in-down"
                      />
                    </svg>
                  </button>
                  <!--<button
                    type="button"
                    class="btn btn-dark"
                    @click=${this._cleanUser}
                  >
                    Limpiar
                    <svg class="bi" width="24" height="24" fill="currentColor">
                      <use
                        xlink:href="./resource/bootstrap-icons.svg#trash-fill"
                      />
                    </svg>
                  </button>-->
                  <button
                    type="button"
                    class="btn btn-bbva btn-lg"
                    data-dismiss="modal"
                    name="btnExitNewUser"
                    @click=${this._exitUser}
                  >
                    Salir
                    <svg class="bi" width="24" height="24" fill="currentColor">
                      <use
                        xlink:href="./resource/bootstrap-icons.svg#door-closed-fill"
                      />
                    </svg>
                  </button>
                
              </div>
            </div>
          </div>
        </div>
      </div>
    `}});customElements.define("cmp-list",class extends te{createRenderRoot(){return this}static get properties(){return{idField:{type:String},title:{type:String},headers:{type:Array},fields:{type:Array},actions:{type:Array},data:{type:Array},urlIcon:{type:String},dataSize:{type:Number},pagesSize:{type:Number},page:{type:Number},numberOfRowsForPage:{type:Number},listPages:{type:Array},selectedPage:{type:Number},firstPage:{type:Number},lastPage:{type:Number},dataSizeDisable:{type:Boolean}}}constructor(){super(),this.idField="",this.title="",this.headers=[],this.fields=[],this.data=[],this.dataSize=0,this.numberOfRowsForPage=5,this.pagesSize=1,this.listPages=[1],this.selectedPage=1,this.firstPage=1,this.lastPage=1,this.dataSizeDisable=!0,this.addEventListener("reload-paginator",({detail:e})=>this._generatePaginator(e))}_generatePaginator(e){this.dataSize=e.totalRows,this.numberOfRowsForPage=e.rowsForPage,this.pagesSize=Math.floor(this.dataSize/this.numberOfRowsForPage)>=1?this.dataSize%this.numberOfRowsForPage==0?Math.floor(this.dataSize/this.numberOfRowsForPage):Math.floor(this.dataSize/this.numberOfRowsForPage)+1:1,this.lastPage=this.pagesSize,this.listPages=[];for(let e=1;e<=this.pagesSize;e++)this.listPages.push(e);console.log("_generatePaginator","this.dataSize: ",this.dataSize,"this.numberOfRowsForPage: ",this.numberOfRowsForPage,"this.pagesSize: ",this.pagesSize,"this.listPages: ",this.listPages)}_getNumberOfRowsPerPage(e){let t=e.target.selectedOptions[0].value;this.numOfRows=t,this.selectedPage=1,console.log("_getNumberOfRowsPerPage","this.selectedPage: ",this.selectedPage,"this.numOfRows: ",this.numOfRows),this._sentEventToPaginator(this.selectedPage,this.numOfRows)}_getNumberPage(e){let t=parseInt(this.querySelector("#idListPaginator").selectedOptions[0].value),s=parseInt(e.target.text);this.numOfRows=t,this.selectedPage=s,console.log("_getNumberPage","this.selectedPage: ",this.selectedPage,"this.numOfRows: ",this.numOfRows),this._sentEventToPaginator(this.selectedPage,this.numOfRows)}_sentEventToPaginator(e,t){console.log("_getDataPaginator","numberPage: ",e,"rowsPerPage: ",t);let s=new CustomEvent("send-event-paginator",{detail:{numRows:t,page:e},bubbles:!0,composed:!0});this.dispatchEvent(s)}_sendEventObjectActionSelected(e,t){console.log("_sendEventObjectActionSelected","Object: ",e,"Action: ",t);let s=new CustomEvent("send-evemt-object-action-selected",{detail:{item:e,action:t}});this.dispatchEvent(s)}getValueFielf(e,t){let s,i=t.split(".");if(i.length>1){s=e[i[0]];for(let e=1;e<i.length;e++)s=s[i[e]]}else s=e[t];return s}render(){return I`
      <style>
        .my-custom-scrollbar {
          position: relative;
          height: 300px;
          overflow: auto;
        }
        .table-wrapper-scroll-y {
          display: block;
        }
      </style>
      <div class="table-wrapper-scroll-y my-custom-scrollbar">
        <table class="table table-striped table-hover table-sm">
          <thead class="thead-dark">
            <tr>
              <th
                colspan="${this.fields&&null!=this.actions&&this.actions.length>0?this.fields.length+1:this.fields.length}"
              >
                ${this.title}
              </th>
            </tr>
            <tr>
              ${this.headers&&this.headers.map(e=>I`<th scope="col">${e}</th>`)}
              ${null!=this.actions?I`<th scope="col">Acciones</th>`:I``}
            </tr>
          </thead>
          <tbody>
            ${this.data&&this.data.map(e=>I` <tr>
                ${this.fields&&this.fields.map(t=>I`
                    <td>
                      ${"Acciones"!=t?this.getValueFielf(e,t):this.actions&&this.actions.map(t=>I`
                              ${"pencil"==t.icon?I` <svg
                                    @click="${()=>this._sendEventObjectActionSelected(e,t)}"
                                    class="bi"
                                    width="24"
                                    height="24"
                                    fill="currentColor"
                                  >
                                    <use
                                      xlink:href="./resource/bootstrap-icons.svg#pencil-fill"
                                    />
                                  </svg>`:"trash"==t.icon?I` <svg
                                    @click="${()=>this._sendEventObjectActionSelected(e,t)}"
                                    class="bi"
                                    width="24"
                                    height="24"
                                    fill="currentColor"
                                  >
                                    <use
                                      xlink:href="./resource/bootstrap-icons.svg#trash-fill"
                                    />
                                  </svg>`:"search"==t.icon?I` <svg
                                    @click="${()=>this._sendEventObjectActionSelected(e,t)}"
                                    class="bi"
                                    width="24"
                                    height="24"
                                    fill="currentColor"
                                  >
                                    <use
                                      xlink:href="./resource/bootstrap-icons.svg#search"
                                    />
                                  </svg>`:I``}
                            `)}
                    </td>
                  `)}
              </tr>`)}
          </tbody>
        </table>
      </div>
      <br />
      <nav aria-label="Page navigation example">
        <form class="form-inline">
          <div class="form-row">
            <div class="col">
              <ul class="pagination">
                <li class="page-item"><a class="page-link" href="#"><<</a></li>
                ${this.listPages.map(e=>I`<li class="page-item">
                      <a class="page-link" @click=${this._getNumberPage}
                        >${e}</a
                      >
                    </li>`)}
                <li class="page-item"><a class="page-link" href="#">>></a></li>
              </ul>
            </div>
            <div class="col">
              <select
                class="custom-select mr-sm-2"
                id="idListPaginator"
                @input="${this._getNumberOfRowsPerPage}"
              >
                <option value="5">5</option>
                <option value="10">10</option>
                <option value="15">15</option>
                <option value="20">20</option>
                <option value="25">25</option>
                <option value="30">30</option>
              </select>
            </div>
            <div class="col">
              <input
                type="text"
                class="form-control -sm"
                name="dataSize"
                id="idDataSize"
                .value="Total de ${this.dataSize} registro(s)."
                ?disabled="${this.dataSizeDisable}"
              />
            </div>
          </div>
        </form>
      </nav>
    `}});customElements.define("cmp-confirm-modal",class extends te{createRenderRoot(){return this}static get properties(){return{idObject:{type:Number},message:{type:String},messageTitle:{type:String}}}constructor(){super(),this.idObject=null,this.mesage="",this.messageTitle="",this.addEventListener("load-id-confirm",({detail:e})=>this._load(e))}_load(e){this.idObject=e.id,this.message=e.mesage,this.messageTitle=e.messageTitle}_acept(){console.log("id seleccionado",this.idObject);let e=new CustomEvent("acept-confirm",{detail:this.idObject});this.dispatchEvent(e),this.renderRoot.querySelector("#myModal").style.display="none"}_cancel(){this.idObject=null,this.renderRoot.querySelector("#myModal").style.display="none"}render(){return I`
      <link rel="stylesheet" href="./src/components/style-confirm.css" />
      
      <!-- Modal HTML -->
     
        <div id="myModal"  class="popup">
          <div class="modal-dialog modal-sm">
          <div class="card">
            <div class="card-header">
            <svg class="bi text-danger" width="22" height="22" fill="currentColor">
                <use
                  xlink:href="./resource/bootstrap-icons.svg#x-circle-fill"
                />
              </svg>
              
              &nbsp;${this.messageTitle}
             
            </div>
            <div class="modal-body">
              <p>${this.message}</p>
            </div>
            <div class="modal-footer justify-content-center">
              <button
                type="button"
                class="btn btn-secondary"
                data-dismiss="modal"
                @click="${this._cancel}"
              >
                Cancelar
                <svg class="bi" width="24" height="24" fill="currentColor">
                      <use
                        xlink:href="./resource/bootstrap-icons.svg#x"
                      />
                    </svg>
              </button>
              <button
                type="button"
                class="btn btn-danger"
                @click="${this._acept}"
              >
                Aceptar
                <svg class="bi" width="24" height="24" fill="currentColor">
                      <use
                        xlink:href="./resource/bootstrap-icons.svg#check"
                      />
                    </svg>
              </button>
            </div>
          </div>
        </div>
        </div>
    `}});customElements.define("cmp-list-user",class extends te{createRenderRoot(){return this}static get properties(){return{headers:{type:Array},fields:{type:Array},actions:{type:Array},items:{type:Array},idField:{type:String},title:{type:String},messages:{type:Array},listCountrySearch:{type:Array},listCitySearch:{type:Array},listProfileSearch:{type:Array},listDocumentTypeSearch:{type:Array},startDateSearch:{type:String},endDateSearch:{type:String},firstNameSearch:{type:String},lastNameSearch:{type:String},documentNumberSearch:{type:String},emailSearch:{type:String},idClienteSearch:{type:String},creationDateInit:{type:String},creationDateEnd:{type:String}}}get userDm(){return ie()}get numberUtil(){return re()}constructor(){super(),this.firstNameSearch="",this.lastNameSearch="",this.documentNumberSearch="",this.emailSearch="",this.idClienteSearch="",this.creationDateInit=this.numberUtil._getPreMonthDate(),this.creationDateEnd=this.numberUtil._getTodayDate(),this.headers=["Id","Cod. Cliente","Tipo documento","Número documento","Nombres","Apellidos","Email","Perfil","Cod. Empleado","Pais","Ciudad","Fecha de creación"],this.fields=["id","id_client","document_type","document_number","first_name","last_name","email","profile.code","employee.code","country","city","creation_date","Acciones"],this.actions=[{id:"update",name:"Actualizar",icon:"pencil",function:"_updateRow",style:"btn btn-primary"},{id:"delete",name:"Eliminar",icon:"trash",function:"_deleteRow",style:"btn btn-danger"}],this.idField="id",this.items=[],this.title="Usuarios",this.listCountrySearch=[],this.listCitySearch=[],this.listProfileSearch=[],this.listDocumentTypeSearch=[],this._searchInit(),this.addEventListener("load-parameters",({detail:e})=>this._loadParameters(e))}_loadParameters(e){this.listCountrySearch=JSON.parse(sessionStorage.getItem("parametersCountry")),this.listProfileSearch=JSON.parse(sessionStorage.getItem("parametersProfiles")),this.listDocumentTypeSearch=JSON.parse(sessionStorage.getItem("parametersDocumentType"));let t=this.querySelector("cmp-user");const s=new CustomEvent("load-parameters",{detail:{},bubbles:!0,composed:!0});t.dispatchEvent(s)}_getLoadCity(e){this.listCitySearch=[];let t=e.target.selectedOptions[0].value,s=JSON.parse(sessionStorage.getItem("parametersCity"));for(let e=0;e<s.length;e++)s[e].parameter_parent_code==t&&this.listCitySearch.push(s[e])}async _searchInit(){await this.updateComplete,this._search()}async _getUserAction({item:e,action:t}){console.log("item",e),console.log("action",t),"update"==t.id?this._getUserById(e):"delete"==t.id?this._showDialogDelete(e,t):t.id}_showDialogDelete(e,t){let s=this.querySelector("cmp-confirm-modal");s.querySelector("#myModal").style.display="block";const i=new CustomEvent("load-id-confirm",{detail:{id:e.id,messageTitle:"¿Esta seguro?",mesage:"¿Esta seguro de ejecutar la accion?, el cambio no se puede revertir"},bubbles:!0,composed:!0});s.dispatchEvent(i)}async _getUserById(e){this._initLoading();let t=await this.userDm.getUserById(e.id);if(1==t.objects.length){this.messages=[],console.log("user: ",t.objects[0]);let e=this.querySelector("cmp-user");const s=new CustomEvent("load-user",{detail:t.objects[0],bubbles:!0,composed:!0});e.dispatchEvent(s),this.querySelector("cmp-user").querySelector("#idDialogUser").style.display="block"}else this.errors=[],this.errors.push(this._addError("No se pudo obtener el usuario.","Danger! ","alert alert-danger"));this._endLoading()}_addError(e,t,s){return{message:e,typeMessage:t,typeClass:s}}_initLoading(){const e=this.parentElement.parentElement.parentElement.querySelector("cmp-loading");if(null!=e){const t=new CustomEvent("init-loading",{detail:{},bubbles:!0,composed:!0});e.dispatchEvent(t)}}_endLoading(){const e=this.parentElement.parentElement.parentElement.querySelector("cmp-loading");if(null!=e){const t=new CustomEvent("end-loading",{detail:{},bubbles:!0,composed:!0});e.dispatchEvent(t)}}async _search(){this._initLoading();let e={};e.numRows=this.querySelector("cmp-list").querySelector("#idListPaginator").selectedOptions[0].value,e.page=1,this._searchPaginator(e)}_getParamsSearch(){let e={document_number:this.querySelector("#document_number_search").value,document_type:this.querySelector("#document_type_search").selectedOptions[0].value,first_name:this.querySelector("#first_name_search").value,last_name:this.querySelector("#last_name_search").value,creation_date_init:this.querySelector("#creation_date_init").value,creation_date_end:this.querySelector("#creation_date_end").value,country:this.querySelector("#country_search").selectedOptions[0].value,city:this.querySelector("#city_search").selectedOptions[0].value,id_client:this.querySelector("#id_client_search").value,email:this.querySelector("#email_search").value};return console.log("paramsSearch",JSON.stringify(e)),e}async _searchPaginator(e){let t=this._getParamsSearch();this._getParamsSearch();let s=await this.userDm.getUsers(t);t.rowsPerPage=parseInt(e.numRows),t.skippingRows=parseInt((e.page-1)*e.numRows),console.log("parametros",t);let i=await this.userDm.getUsers(t);console.log("data: ",i),this.items=i.objects,this._reloadCmpList(t.rowsPerPage,s.objects.length),this._endLoading()}_reloadCmpList(e,t){const s=this.querySelector("cmp-list"),i=new CustomEvent("reload-paginator",{detail:{rowsForPage:e,totalRows:t},bubbles:!0,composed:!0});s.dispatchEvent(i)}_openNewUser(){console.log(this),this.parentElement.querySelector("cmp-user").querySelector("#idDialogUser").style.display="block"}async _deleteUser(e){let t,s;this._initLoading(),console.log("_deleteUser: id",e),s=await this.userDm.deleteUser(e),t="Registro eliminado correctamente.",console.log("response: ",s),"204"==s.status?(this.messages=[],this.messages.push({message:"Registro eliminado correctamente.",typeMessage:"¡Success! ",typeClass:"alert alert-success"}),this._search()):(this.messages=[],this.messages.push({message:"No se pudo eliminar el registro.",typeMessage:"Danger! ",typeClass:"alert alert-danger"}));let i=this.parentElement.querySelector("cmp-alert-modal");const a=new CustomEvent("load-alert-component",{detail:{messageStatus:this.messages[0].typeMessage,message:this.messages[0].message,messageStyle:this.messages[0].typeClass},bubbles:!0,composed:!0});i.dispatchEvent(a),this._endLoading()}_clean(){this.querySelector("#document_number_search").value="",this.querySelector("#first_name_search").value="",this.querySelector("#last_name_search").value="",this.querySelector("#creation_date_init").value="",this.querySelector("#creation_date_end").value="",this.querySelector("#id_client_search").value="",this.querySelector("#email_search").value="",this.listCitySearch=[],this.querySelector("#document_type_search").value="",this.querySelector("#country_search").value="",this.querySelector("#city_search").value="",this.querySelector("#profile_search").value=""}render(){return I`
      <cmp-confirm-modal
        @acept-confirm="${({detail:e})=>this._deleteUser(e)}"
      ></cmp-confirm-modal>

      <cmp-user @reload-table-user="${this._search}"></cmp-user>
      <div class="card">
      <div class="card-header text-dark bg-light text-uppercase font-weight-light">
          <h5 class="card-title">
            <svg class="bi" width="24" height="24" fill="currentColor">
              <use
                xlink:href="./resource/bootstrap-icons.svg#person-circle"
              />
            </svg>

            usuarios
          </h5>
        </div>
        </div>
        <br>
  </div>
      <div class="card">
      <div class="card-header text-dark bg-light text-uppercase font-weight-light">
          <h5 class="card-title">
            <svg class="bi" width="24" height="24" fill="currentColor">
              <use
                xlink:href="./resource/bootstrap-icons.svg#funnel-fill"
              />
            </svg>

            Filtros de busqueda
          </h5>
        </div>
        
        <div class="card-body ">
          <form>
            <div class="form-row">
              <div class="form-group col-md-2">
                <label for="document_type_search">Tipo de documento</label>

                <select
                  name="document_type"
                  class="custom-select mr-sm-2"
                  id="document_type_search"
                  name="document_type_search"
                >
                  <option value="">--Seleccione uno--</option>
                  ${this.listDocumentTypeSearch&&this.listDocumentTypeSearch.map(e=>I`<option .value="${e.parameter_value}">
                        ${e.parameter_name}
                      </option>`)}
                </select>
              </div>
              <div class="form-group col-md-2">
                <label for="document_number_search">Nº de documento</label>
                <input
                  type="text"
                  class="form-control inputForm"
                  id="document_number_search"
                  name="document_number_search"
                  .value="${this.documentNumberSearch}"
                />
              </div>
              <div class="form-group col-md-4">
                <label for="first_name_search">Nombres</label>
                <input
                  type="text"
                  class="form-control inputForm"
                  id="first_name_search"
                  name="first_name_search"
                  .value="${this.firstNameSearch}"
                />
              </div>

              <div class="form-group col-md-4">
                <label for="last_name_search">Apellidos</label>
                <input
                  type="text"
                  class="form-control inputForm"
                  id="last_name_search"
                  name="last_name_search"
                  .value="${this.lastNameSearch}"
                />
              </div>
            </div>
            <div class="form-row">
              <div class="form-group col-md-2">
                <label for="country_search">Pais</label>
                <select
                  name="country_search"
                  class="custom-select mr-sm-2"
                  id="country_search"
                  @input=${this._getLoadCity}
                >
                  <option value="">--Seleccione uno--</option>
                  ${this.listCountrySearch&&this.listCountrySearch.map(e=>I`<option .value="${e.parameter_value}">
                        ${e.parameter_name}
                      </option>`)}
                </select>
              </div>

              <div class="form-group col-md-2">
                <label for="city_search">Ciudad</label>
                <select
                  name="city_search"
                  class="custom-select mr-sm-2"
                  id="city_search"
                >
                  <option value="">--Seleccione uno--</option>
                  ${this.listCitySearch&&this.listCitySearch.map(e=>I`<option .value="${e.parameter_value}">
                        ${e.parameter_name}
                      </option>`)}
                </select>
              </div>
              <div class="form-group col-md-4">
                <label for="email_search">Email</label>
                <input
                  type="text"
                  class="form-control inputForm"
                  id="email_search"
                  name="email_search"
                  .value="${this.emailSearch}"
                />
              </div>
              <div class="form-group col-md-2">
                <label for="profile_search">Perfil</label>
                <select
                  name="profile_search"
                  class="custom-select mr-sm-2"
                  id="profile_search"
                >
                  <option value="">--Seleccione uno--</option>
                  ${this.listProfileSearch&&this.listProfileSearch.map(e=>I`<option .value="${e.parameter_value}">
                        ${e.parameter_name}
                      </option>`)}
                </select>
              </div>
              <div class="form-group col-md-2">
                <label for="id_client_search">Cod. Cliente</label>
                <input
                  type="text"
                  class="form-control inputForm"
                  id="id_client_search"
                  name="id_client_search"
                  .value="${this.idClienteSearch}"
                />
              </div>
            </div>
            <div class="form-row">
              <div class="form-group col-md-2">
                <label for="creation_date_init">Fecha creación inicial</label>
                <input
                  type="date"
                  id="creation_date_init"
                  name="creation_date_init"
                  class="form-control inputForm"
                  min="2015-01-01"
                  max="2022-01-01"
                  .value="${this.creationDateInit}"
                />
              </div>
              <div class="form-group col-md-2">
                <label for="creation_date_end">Fecha creación final</label>
                <input
                  type="date"
                  id="creation_date_end"
                  name="creation_date_end"
                  class="form-control inputForm"
                  min="2015-01-01"
                  max="2022-01-01"
                  placeholder="yyyy-mm-dd"
                  .value="${this.creationDateEnd}"
                />
              </div>
            </div>
          </form>
        </div>
        <div class="card-footer text-right">
       
            <button type="button" class="btn btn-bbva btn-lg" @click="${this._search}">
              <svg class="bi" width="24" height="24" fill="currentColor">
                <use
                  xlink:href="./resource/bootstrap-icons.svg#search"
                />
              </svg>

              Buscar
            </button>
            <button type="button" class="btn btn-bbva btn-lg" @click="${this._clean}">
              <svg class="bi" width="24" height="24" fill="currentColor">
                <use
                  xlink:href="./resource/bootstrap-icons.svg#trash-fill"
                />
              </svg>
              Limpiar
            </button>
            <!-- <button
              type="button"
              class="btn btn-dark"
              @click=${this._openNewUser}
            >
              <svg class="bi" width="24" height="24" fill="currentColor">
                <use
                  xlink:href="./resource/bootstrap-icons.svg#person-plus-fill"
                />
              </svg>
              Nuevo
            </button>-->
         
        </div>
      </div>
      <br>
      <div class="card">
      <div class="card-header text-dark bg-light text-uppercase font-weight-light">
          <h5 class="card-title">
            <svg class="bi" width="24" height="24" fill="currentColor">
              <use
                xlink:href="./resource/bootstrap-icons.svg#list"
              />
            </svg>

            Lista de resultados
          </h5>
        </div>
        
        <div class="card-body">
          <cmp-list
            title="${this.title}"
            idField="${this.idField}"
            .headers="${this.headers}"
            .fields="${this.fields}"
            .data="${this.items}"
            .actions="${this.actions}"
            @send-event-paginator="${({detail:e})=>this._searchPaginator(e)}"
            @send-evemt-object-action-selected="${({detail:e})=>this._getUserAction(e)}"
          >
          </cmp-list>
        </div>
      </div>
    `}});customElements.define("cmp-app-user",class extends te{createRenderRoot(){return this}static get properties(){return{userName:{type:String},userEmail:{type:String},userId:{type:Number},userLogged:{type:Object},profile:{type:Object}}}constructor(){super(),this.userName="",this.userEmail="",this.userId=null,this.profile={},this.userLogged={profile:this.profile},this.addEventListener("send-user",({detail:e})=>this._getAccountsUser(e))}_getAccountsUser(e){this.userLogged=e.user,console.log(JSON.stringify(this.userLogged))}render(){return I`
      <div class="card">
        <div class="card-body text-light bg-bbva">
          <div class="form-row">
            <div class="form-group col-md-3">
              <h1 class="card-title">
                <b>BBVA TechU</b>
              </h1>
            </div>
            <div class="form-group col-md-3"></div>
            <div class="form-group col-md-3"></div>
            <div class="form-group col-md-3 text-right">
              <h5>
                <ul style="list-style-type: none" class="text-uppercase font-weight-light">
                <li>
                ${this.userLogged.first_name+" "+this.userLogged.last_name+" - "+this.userLogged.id_client}
                </li>
                <li class="text-bbva-subtitle text-uppercase font-weight-light">
             
              ${this.userLogged.email} 
              </li>
              </ul>
              </h5>
            </div>
          </div>
        </div>
      </div>
    `}});customElements.define("cmp-alert-modal",class extends te{createRenderRoot(){return this}static get properties(){return{message:{type:String},messageTitle:{type:String},messageStyle:{type:String},messageCode:{type:String},messageStatus:{type:String}}}constructor(){super(),this.message="",this.messageTitle="",this.messageStyle="oculto",this.messageCode="",this.messageStatus="",this.addEventListener("load-alert-component",({detail:e})=>this._load(e))}_load(e){this.messageStatus=e.messageStatus,this.message=e.message,this.messageStyle=e.messageStyle,this.querySelector("#myModal").style.display="block"}_acept(){}_cancel(){this.message="",this.messageTitle="",this.messageStyle="",this.messageCode="",this.messageStatus="",this.renderRoot.querySelector("#myModal").style.display="none"}render(){return I`
      <style>
        .parpadea {
          animation-name: parpadeo;
          animation-duration: 3s;
          animation-timing-function: linear;
          animation-iteration-count: infinite;

          -webkit-animation-name: parpadeo;
          -webkit-animation-duration: 3s;
          -webkit-animation-timing-function: linear;
          -webkit-animation-iteration-count: infinite;
        }

        @-moz-keyframes parpadeo {
          0% {
            opacity: 1;
          }
          50% {
            opacity: 0;
          }
          100% {
            opacity: 1;
          }
        }

        @-webkit-keyframes parpadeo {
          0% {
            opacity: 1;
          }
          50% {
            opacity: 0;
          }
          100% {
            opacity: 1;
          }
        }

        @keyframes parpadeo {
          0% {
            opacity: 1;
          }
          50% {
            opacity: 0;
          }
          100% {
            opacity: 1;
          }
        }
      </style>
       <link rel="stylesheet" href="./src/components/style-alert.css" />
       <div id="myModal"  class="popup">
          <div class="modal-dialog modal-sm">
      <div id="cmpAlert" class="${this.messageStyle} ">
        <strong>${this.messageStatus}: </strong> ${this.message}

        <button
          id="idBtnCerrarAlert"
          type="button"
          class="close"
          @click=${this._cancel}
        >
          &times;
        </button>
      </div>
      </div>
      </div>
    `}});customElements.define("cmp-loading",class extends te{createRenderRoot(){return this}static get properties(){return{}}constructor(){super(),this.addEventListener("init-loading",this._init),this.addEventListener("end-loading",this._end)}_init(){this.querySelectorAll(".progress-bar-striped").forEach(e=>{e.style.display="block"})}_end(){this.querySelectorAll(".progress-bar-striped").forEach(e=>{e.style.display="none"})}render(){return I`
      <div class="progress" id="idLoading" >
      <div class="progress-bar-striped bg-info progress-bar-animated" role="progressbar" style="width: 5%" aria-valuenow="5" aria-valuemin="0" aria-valuemax="200"></div>
      <div class="progress-bar-striped bg-success progress-bar-animated" role="progressbar" style="width: 15%" aria-valuenow="15" aria-valuemin="0" aria-valuemax="100"></div>
      <div class="progress-bar-striped bg-warning progress-bar-animated" role="progressbar" style="width: 30%" aria-valuenow="30" aria-valuemin="0" aria-valuemax="100"></div>
      <div class="progress-bar-striped bg-danger progress-bar-animated" role="progressbar" style="width: 50%" aria-valuenow="50" aria-valuemin="0" aria-valuemax="100"></div>
</div>



      <div id="idLoading2" class="d-flex justify-content-center">
       
        <div style="display:none"
          class="spinner-border -sm m-2 text-primary"
          role="status"
          aria-hidden="true"
        ></div>
        <div style="display:none"
          class="spinner-border -sm m-2 text-secondary"
          role="status"
          aria-hidden="true"
        ></div>
		<div style="display:none"
          class="spinner-border -sm m-2 text-success"
          role="status"
          aria-hidden="true"
        ></div>
		<div style="display:none"
          class="spinner-border -sm m-2 text-danger"
          role="status"
          aria-hidden="true"
        ></div>
		<div style="display:none"
          class="spinner-border -sm m-2 text-warning"
          role="status"
          aria-hidden="true"
        ></div>
      </div>
    `}});customElements.define("cmp-user-info",class extends te{createRenderRoot(){return this}get userDm(){return ie()}get numberUtil(){return re()}static get properties(){return{id:{type:String},idClient:{type:String},firstName:{type:String},lastName:{type:String},email:{type:String},password:{type:String},documentType:{type:String},documentNumber:{type:String},mobilePhone:{type:String},address:{type:String},phone:{type:String},city:{type:String},country:{type:String},creationDate:{type:String},updateDate:{type:String},visibleAmounts:{type:Boolean},maximumAmountOperation:{type:String},type:{type:String},profileCode:{type:String},profileName:{type:String},employeeCode:{type:String},employeePosition:{type:String},disabledDate:{type:Boolean},message:{type:String},typeClass:{type:String},errors:{type:Array},isActiveSave:{type:Boolean},listCountry:{type:Array},listCity:{type:Array},listProfile:{type:Array},listDocumentType:{type:Array},listType:{type:Array},userObject:{type:Object}}}constructor(){super(),this.userObject={},this.id="",this.idClient="",this.firstName="",this.lastName="",this.email="",this.password="",this.documentType="",this.documentNumber="",this.mobilePhone="",this.address="",this.phone="",this.city="",this.country="",this.creationDate="",this.updateDate="",this.visibleAmounts=!1,this.maximumAmountOperation="0.00",this.type="",this.profileCode="",this.profileName="",this.employeeCode="",this.employeePosition="",this.disabledDate=!0,this.typeMessage="",this.message="",this.typeClass="",this.errors=[],this.isActiveSave=!1,this.addEventListener("send-user",({detail:e})=>this._loadUser(e)),this.listCountry=[],this.listCity=[],this.listProfile=[],this.listDocumentType=[],this.listType=[],this.addEventListener("load-parameters",({detail:e})=>this._loadParameters(e))}_loadParameters(e){this.listCountry=JSON.parse(sessionStorage.getItem("parametersCountry")),this.listProfile=JSON.parse(sessionStorage.getItem("parametersProfiles")),this.listDocumentType=JSON.parse(sessionStorage.getItem("parametersDocumentType")),this.listDocumentType=JSON.parse(sessionStorage.getItem("parametersDocumentType")),this.listType=JSON.parse(sessionStorage.getItem("parametersType")),console.log("this.listCountry",JSON.stringify(this.listCountry)),console.log("this.listProfile",JSON.stringify(this.listProfile)),console.log("this.listDocumentType ",JSON.stringify(this.listDocumentType)),console.log("this.listType ",JSON.stringify(this.listType))}async _loadUser(e){let t=await this.userDm.getUserById(e.user.id);1==t.objects.length&&(this.userObject=t.objects[0],this.id=this.userObject.id,this.idClient=this.userObject.id_client,this.firstName=this.userObject.first_name,this.lastName=this.userObject.last_name,this.email=this.userObject.email,this.password=this.userObject.password,this.documentType=this.userObject.document_type,this.documentNumber=this.userObject.document_number,this.mobilePhone=this.userObject.mobile_phone,this.address=this.userObject.address,this.phone=this.userObject.phone,this.city=this.userObject.city,this.country=this.userObject.country,this.creationDate=this.userObject.creation_date,this.updateDate=this.userObject.update_date,this.visibleAmounts=this.userObject.visible_amounts,this.maximumAmountOperation=this.userObject.maximum_amount_operations,this.type=this.userObject.type,this.profileCode=this.userObject.profile.code,this.profileName=this.userObject.profile.name,this.employeeCode=this.userObject.employee.code,this.employeePosition=this.userObject.employee.position,this.isActiveSave=!1,this._getLoadCityInit(this.country),this.querySelector("#document_type").value=this.documentType,this.querySelector("#country").value=this.country,this.querySelector("#profile_code").value=this.profileCode,this.querySelector("#type").value=this.type,this.querySelector("#city").value=this.city,this._setCity())}async _setCity(){await this.updateComplete,this.querySelector("#city").value=this.city}_getLoadCityInit(e){this.listCity=[];let t=e,s=JSON.parse(sessionStorage.getItem("parametersCity"));for(let e=0;e<s.length;e++)s[e].parameter_parent_code==t&&this.listCity.push(s[e])}_getLoadCity(e){let t=e.target.value;this.listCity=[];let s=t,i=JSON.parse(sessionStorage.getItem("parametersCity"));for(let e=0;e<i.length;e++)i[e].parameter_parent_code==s&&this.listCity.push(i[e])}_initLoading(){const e=this.parentElement.parentElement.parentElement.querySelector("cmp-loading");if(null!=e){const t=new CustomEvent("init-loading",{detail:{},bubbles:!0,composed:!0});e.dispatchEvent(t)}}_endLoading(){const e=this.parentElement.parentElement.parentElement.querySelector("cmp-loading");if(null!=e){const t=new CustomEvent("end-loading",{detail:{},bubbles:!0,composed:!0});e.dispatchEvent(t)}}_mapperUser(){let e={id:this.id,id_client:this.querySelector("#id_client").value,document_type:this.querySelector("#document_type").value,document_number:this.querySelector("#document_number").value,first_name:this.querySelector("#first_name").value,last_name:this.querySelector("#last_name").value,country:this.querySelector("#country").value,city:this.querySelector("#city").value,address:this.querySelector("#address").value,mobile_phone:this.querySelector("#mobile_phone").value,phone:this.querySelector("#phone").value,email:this.querySelector("#email").value,type:this.querySelector("#type").value,maximum_amount_operations:this.querySelector("#maximum_amount_operations").value,employee:{code:this.querySelector("#employee_code").value,position:this.querySelector("#employee_position").value},profile:{code:this.querySelector("#profile_code").value},visible_amounts:"on"==this.querySelector("#visible_amounts").value,creation_date:this.querySelector("#creation_date").value,update_date:this.numberUtil._getTodayDate()};return console.log("userObject",e),e}_errorOnUpdateUser(){let e=[],t=/^\d+$/,s=this._mapperUser();return""==s.id_client&&e.push({message:"El id de Cliente es requerido.",typeMessage:"¡Danger! ",typeClass:"alert alert-danger"}),""==s.document_type&&e.push({message:"El tipo de documento es requerido.",typeMessage:"¡Danger! ",typeClass:"alert alert-danger"}),""==s.document_number&&e.push({message:"El número de documento es requerido.",typeMessage:"¡Danger! ",typeClass:"alert alert-danger"}),""==s.first_name&&e.push({message:"Los nombres son requeridos",typeMessage:"¡Danger! ",typeClass:"alert alert-danger"}),""==s.last_name&&e.push({message:"Los apellidos son requeridos",typeMessage:"¡Danger! ",typeClass:"alert alert-danger"}),""==s.country&&e.push({message:"Es pais es requerido",typeMessage:"¡Danger! ",typeClass:"alert alert-danger"}),""==s.city&&e.push({message:"La ciudad es requerida",typeMessage:"¡Danger! ",typeClass:"alert alert-danger"}),""==s.address&&e.push({message:"La direccion es requerida",typeMessage:"¡Danger! ",typeClass:"alert alert-danger"}),""==s.mobile_phone&&e.push({message:"El númer de celular es requerido",typeMessage:"¡Danger! ",typeClass:"alert alert-danger"}),""==s.email&&e.push({message:"El email es requerido",typeMessage:"¡Danger! ",typeClass:"alert alert-danger"}),""==s.phone&&e.push({message:"El telefono fijo es requerido",typeMessage:"¡Danger! ",typeClass:"alert alert-danger"}),""==s.profile.code&&e.push({message:"El código de perfil es requerido",typeMessage:"¡Danger! ",typeClass:"alert alert-danger"}),""==s.type&&e.push({message:"El tipo de cliente es requerido.",typeMessage:"¡Danger! ",typeClass:"alert alert-danger"}),""==s.maximum_amount_operations&&e.push({message:"El monto máximo de operaciones es requerido.",typeMessage:"¡Danger! ",typeClass:"alert alert-danger"}),/^(([^<>()[\]\.,;:\s@\"]+(\.[^<>()[\]\.,;:\s@\"]+)*)|(\".+\"))@(([^<>()[\]\.,;:\s@\"]+\.)+[^<>()[\]\.,;:\s@\"]{2,})$/i.test(s.email)||e.push({message:"Ingrese un email valido.",typeMessage:"¡Danger! ",typeClass:"alert alert-danger"}),/^([0-9]+\.?[0-9]{0,2})$/.test(s.maximum_amount_operations)||e.push({message:"Ingrese un monto limite de operaciones valido.",typeMessage:"¡Danger! ",typeClass:"alert alert-danger"}),t.test(s.mobile_phone)||e.push({message:"Ingrese un numero de celular valido.",typeMessage:"¡Danger! ",typeClass:"alert alert-danger"}),t.test(s.phone)||e.push({message:"Ingrese un numero de telèfono fijo valido.",typeMessage:"¡Danger! ",typeClass:"alert alert-danger"}),e}async _update(){this._initLoading();let e=this._errorOnUpdateUser();if(0===e.length){let e=this._mapperUser();200==(await this.userDm.updateUser(e,e.id)).status?(this.errors=[],this.errors.push({message:"El registro se actualizó correctamente.",typeMessage:"¡Success! ",typeClass:"alert alert-success"}),this._setEventReloadTableUser()):(this.errors=[],this.errors.push({message:"No se pudo actualizar el registro.",typeMessage:"Danger! ",typeClass:"alert alert-danger"}))}else this.errors=e;let t=this.parentElement.querySelector("cmp-alert-modal");const s=new CustomEvent("load-alert-component",{detail:{messageStatus:this.errors[0].typeMessage,message:this.errors[0].message,messageStyle:this.errors[0].typeClass},bubbles:!0,composed:!0});t.dispatchEvent(s),this._endLoading()}_setEventReloadTableUser(){let e=new CustomEvent("reload-table-user",{detail:{status:"200"}});this.dispatchEvent(e)}_cleanUser(){this.id="",this.idClient="",this.firstName="",this.lastName="",this.email="",this.password="",this.documentType="",this.documentNumber="",this.mobilePhone="",this.address="",this.phone="",this.city="",this.country="",this.creationDate="",this.updateDate="",this.visibleAmounts=!1,this.maximumAmountOperation=0,this.type="",this.profileCode="",this.profileName="",this.employeeCode="",this.employeePosition="",this.errors=[],this.isActiveSave=!1,this.listCity=[],this.querySelector("#document_type").value="",this.querySelector("#country").value="",this.querySelector("#profile_code").value="",this.querySelector("#type").value="",this.querySelector("#city").value=""}_exitUser(){this._cleanUser(),this.renderRoot.querySelector("#idDialogUser").style.display="none"}render(){return I`
     <div class="card">
      <div class="card-header text-dark bg-light text-uppercase font-weight-light">
          <h5 class="card-title">
            <svg class="bi" width="24" height="24" fill="currentColor">
              <use
                xlink:href="./resource/bootstrap-icons.svg#person-lines-fill"
              />
            </svg>

            MIS DATOS
          </h5>
        </div>
        </div>
        <br>
    <div class="form-row">
      <div class="form-group col-md-6">
        <div class="card">
        <div class="card-header text-dark bg-light text-uppercase font-weight-light">
          <h5 class="card-title">
            <svg class="bi" width="24" height="24" fill="currentColor">
              <use
                xlink:href="./resource/bootstrap-icons.svg#file-person-fill"
              />
            </svg>

            Información personal
          </h5>
        </div>
          
          <div class="card-body ">
            <div class="form-row">
              <div class="form-group col-md-6">
                <label for="document_type">Tipo de documento</label>
                <select ?disabled="${!0}"
                    name="document_type"
                    class="custom-select mr-sm-3"
                    id="document_type"
                    name="document_type"
                  >
                    <option value="">--Seleccione uno--</option>
                    ${this.listDocumentType&&this.listDocumentType.map(e=>I`<option .value="${e.parameter_value}">
                          ${e.parameter_name}
                        </option>`)}
                </select>
              </div>
              <div class="form-group col-md-6">
                <label for="document_number">Nº de documento</label>
                <input
                    type="text" ?disabled="${!0}"
                    class="form-control inputForm"
                    id="document_number"
                    name="document_number"
                    .value="${this.documentNumber}"
                />
              </div>
            </div>
            <div class="form-row">
              <div class="form-group col-md-12">
                <label for="first_name">Nombres</label>
                <input
                  type="text" ?disabled="${!0}"
                  class="form-control inputForm"
                  id="first_name"
                  name="first_name"
                  .value="${this.firstName}"
                />
              </div>
            </div>
            <div class="form-row">
              <div class="form-group col-md-12">
                <label for="last_name">Apellidos</label>
                <input
                      type="text" ?disabled="${!0}"
                      class="form-control inputForm"
                      id="last_name"
                      name="last_name"
                      .value="${this.lastName}"
                />
              </div>
            </div>
            <div class="form-row" >
              <div class="form-group col-md-6">
                <label for="id_client">Cod. Cliente</label>
                <input ?disabled="${!0}"
                  type="text"
                  class="form-control inputForm"
                  id="id_client"
                  name="id_client"
                  .value="${this.idClient}"
                />
              </div>   
              <div class="form-group col-md-6">
                <label for="type">Tipo cliente</label>
                <select ?disabled=${!0}
                  name="type"
                  class="custom-select mr-sm-2"
                  id="type"
                >
                  <option value="">--Seleccione uno--</option>
                  ${this.listType&&this.listType.map(e=>I`<option .value="${e.parameter_value}">
                        ${e.parameter_name}
                      </option>`)}
                </select>
              </div>    
            </div>
          </div>
        </div>
      </div>
      <div class="form-group col-md-6">
        <div class="card">
        <div class="card-header text-dark bg-light text-uppercase font-weight-light">
          <h5 class="card-title">
            <svg class="bi" width="24" height="24" fill="currentColor">
              <use
                xlink:href="./resource/bootstrap-icons.svg#telephone-fill"
              />
            </svg>

            Contactabilidad
          </h5>
        </div>
          
          <div class="card-body ">
            <div class="form-row" >
              <div class="form-group col-md-6">
                <label for="country">Pais</label>
                  <div class="input-group">
                  <select
                      name="country"
                      class="custom-select"
                      id="country"
                      @input=${this._getLoadCity}
                  >
                    <option value="">--Seleccione uno--</option>
                    ${this.listCountry&&this.listCountry.map(e=>I`<option .value="${e.parameter_value}">
                          ${e.parameter_name}
                        </option>`)}
                  </select>
                  <div class="input-group-append">
                    <button class="btn btn-bbva" type="button" @click="${this._update}">Actualizar</button>
                  </div>
                </div>
              </div>
              <div class="form-group col-md-6">
                <label for="city">Ciudad</label>
                <div class="input-group">
                <select
                  name="city"
                  class="custom-select"
                  id="city"
                >
                  <option value="">--Seleccione uno--</option>
                  ${this.listCity&&this.listCity.map(e=>I`<option .value="${e.parameter_value}">
                        ${e.parameter_name}
                      </option>`)}
                </select>
                
                <div class="input-group-append">
                  <button class="btn btn-bbva" type="button" @click="${this._update}">Actualizar</button>
                </div>
                </div>
              </div>
            </div>  
            <div class="form-row" >
              <div class="form-group col-md-12">
                <label for="address">Dirección</label>
                  <div class="input-group">
                    <input
                      type="text"
                      class="form-control inputForm"
                      id="address"
                      name="address"
                      .value="${this.address}"
                    />
                    <div class="input-group-append">
                      <button class="btn btn-bbva" type="button" @click="${this._update}">Actualizar</button>
                    </div>
                  </div>
              </div>
            </div>   
            <div class="form-row" >
              <div class="form-group col-md-6">
               
                  <label for="mobile_phone">N° Celular</label>
                  <div class="input-group">
                    <input
                      type="text"
                      class="form-control inputForm"
                      id="mobile_phone"
                      name="mobile_phone"
                      .value="${this.mobilePhone}"
                    />
                    <div class="input-group-append">
                        <button class="btn btn-bbva" type="button" @click="${this._update}">Actualizar</button>
                      </div>
                </div>
              </div>
              <div class="form-group col-md-6">
                <label for="phone">Teléfono fijo</label>
                <div class="input-group">
                  <input
                    type="text"
                    class="form-control inputForm"
                    id="phone"
                    name="phone"
                    .value="${this.phone}"
                  />
                  <div class="input-group-append">
                        <button class="btn btn-bbva" type="button" @click="${this._update}">Actualizar</button>
                      </div>
                </div> 
              </div>         
            </div>   
            <div class="form-row" >
            <div class="form-group col-md-12">
                <label for="email">Email</label>
                <div class="input-group">
                  <div class="input-group-prepend">
                    <span class="input-group-text" id="validationTooltipUsernamePrepend">@</span>
                  </div>
                  <input type="text" class="form-control" id="email" .value="${this.email}" name="email" aria-describedby="validationTooltipUsernamePrepend" >
                  <div class="input-group-append">
                        <button class="btn btn-bbva" type="button" @click="${this._update}">Actualizar</button>
                      </div>
                </div>
            </div>
            </div>  
          </div>
        </div>
      </div>
    </div>
  </div>
    </div>
      <div class="form-row">
        <div class="form-group col-md-4">
          <div class="card">
          <div class="card-header text-dark bg-light text-uppercase font-weight-light">
          <h5 class="card-title">
            <svg class="bi" width="24" height="24" fill="currentColor">
              <use
                xlink:href="./resource/bootstrap-icons.svg#cash"
              />
            </svg>

            Información financiera
          </h5>
        </div>
           
            <div class="card-body ">
              <div class="form-row">
                <div class="form-group col-md-6">
                  <label for="maximum_amount_operations">Monto Max. Ope.</label>
                    <div class="input-group">
                      <input 
                        type="text" 
                        class="form-control inputForm"
                        id="maximum_amount_operations"
                        name="maximum_amount_operations"
                        data-inputmask="'alias': 'currency'"
                        .value="${this.maximumAmountOperation}"
                      />
                      <div class="input-group-append">
                          <button class="btn btn-bbva" type="button" @click="${this._update}">Actualizar</button>
                        </div>
                  </div>
                </div>
                <div class="form-group col-md-6">
                  <label for="visible_amounts">Cuentas</label>
                  <div class="custom-control custom-checkbox">
                    <input class="form-check-input" type="checkbox" id="visible_amounts" ?checked="${this.visibleAmounts}" />
                    <label class="form-check-label" for="visible_amounts">
                      Activar visualizar montos.
                    </label>
                  </div>
                </div>  
              </div>
            </div>
          </div>
        </div>
        <div class="form-group col-md-8">
          <div class="card">
          <div class="card-header text-dark bg-light text-uppercase font-weight-light">
          <h5 class="card-title">
            <svg class="bi" width="24" height="24" fill="currentColor">
              <use
                xlink:href="./resource/bootstrap-icons.svg#briefcase-fill"
              />
            </svg>

            Información laboral
          </h5>
        </div>

            
            <div class="card-body ">
                <div class="form-row">
                  <div class="form-group col-md-4">
                    <label for="employee_code">Cod. Empleado</label>
                    <input
                          type="text" ?disabled="${!0}"
                          class="form-control inputForm"
                          id="employee_code"
                          name="employee_code"
                          .value="${this.employeeCode}"
                    />
                  </div>
                  <div class="form-group col-md-4">
                    <label for="employee_position">Cargo</label>
                    <input
                          type="text" ?disabled="${!0}"
                          class="form-control inputForm"
                          id="employee_position"
                          name="employee_position"
                          .value="${this.employeePosition}"
                     />
                  </div>
                  <div class="form-group col-md-4">
                <label for="profile_code">Perfil</label>
                <select
                  name="profile_code"
                  class="custom-select mr-sm-2"
                  id="profile_code" ?disabled="${!0}"
                >
                  <option value="">--Seleccione uno--</option>
                  ${this.listProfile&&this.listProfile.map(e=>I`<option .value="${e.parameter_value}">
                        ${e.parameter_name}
                      </option>`)}
                </select>
              </div>
                </div>
              </div>
            </div>
          </div>
        </div>
    <div class="form-row">
      <div class="form-group col-md-6">
        <div class="card">
        <div class="card-header text-dark bg-light text-uppercase font-weight-light">
          <h5 class="card-title">
            <svg class="bi" width="24" height="24" fill="currentColor">
              <use
                xlink:href="./resource/bootstrap-icons.svg#calendar2-date-fill"
              />
            </svg>

            Información del sistema
          </h5>
        </div>

           
          <div class="card-body">
            <div class="form-row">
              
              <div class="form-group col-md-6">
                  <label for="creation_date">Fecha de creación</label>
                  <input ?disabled="${this.disabledDate}"
                    type="date"
                    id="creation_date"
                    name="creation_date"
                    class="form-control inputForm"
                    
                    .value="${this.creationDate}"
                  />
                </div>
                <div class="form-group col-md-6">
                  <label for="update_date">Fecha de última modificación</label>
                  <input  ?disabled="${this.disabledDate}"
                    type="date"
                    id="update_date"
                    name="update_date"
                    class="form-control inputForm"
                  
                    placeholder="yyyy-mm-dd"
                    .value="${this.updateDate}"
                  />
                </div>
          
              </div>
          </div>
    </div>
    </div>
    </div>

</div>

          
      
     
    `}});class oe{async getAccountByUserId(e){let t="https://fierce-reaches-83730.herokuapp.com/techU/v1/users/"+e+"/accounts",s={},i=await fetch(t);if(i.ok)if(200==i.status){let e=await i.json();s.objects=e,s.status=i.status}else s.objects=[],s.status=204;else s.objects=[],s.status=500;return s}async insertAccount(e,t){let s={},i="https://fierce-reaches-83730.herokuapp.com/techU/v1/users/"+t+"/accounts",a=await fetch(i,{method:"POST",body:JSON.stringify(e),headers:{"Content-Type":"application/json"}});if(a.ok)if(201==a.status){let e=await a.json();s.json=e,s.status=a.status}else s.json="{}",s.status=a.status;else s.json="{}",s.status=a.status;return s}async updateAccount(e,t,s){let i={},a="https://fierce-reaches-83730.herokuapp.com/techU/v1/users/"+t+"/accounts/"+s,r=await fetch(a,{method:"PUT",body:JSON.stringify(e),headers:{"Content-Type":"application/json"}});if(r.ok)if(200==r.status){let e=await r.json();i.json=e,i.status=r.status}else i.json="{}",i.status=r.status;else i.json="{}",i.status=r.status;return i}async insertTransaction(e,t){let s={},i="https://fierce-reaches-83730.herokuapp.com/techU/v1/accounts/"+t+"/transactions",a=await fetch(i,{method:"POST",body:JSON.stringify(e),headers:{"Content-Type":"application/json"}});if(a.ok)if(201==a.status){let e=await a.json();s.json=e,s.status=a.status}else s.json="{}",s.status=a.status;else s.json="{}",s.status=a.status;return s}}let ne=()=>new oe;customElements.define("cmp-user-account",class extends te{createRenderRoot(){return this}get accountDm(){return ne()}get numberUtil(){return re()}static get properties(){return{user:{type:Object},accounts:{type:Array},visibleAmounts:{type:Boolean},acumulateAvailableBalance:{type:Number},acumulateCountableleBalance:{type:Number},listAccountCurrency:{type:Array},listAccuntType:{type:Array},accountName:{type:String},accountCurrency:{type:String},accountType:{type:String},accountAlias:{type:String},negativeStyle:{type:String},positiveStyle:{type:String},errors:{type:Array}}}constructor(){super(),this.user={},this.accounts=[],this.visibleAmounts=!1,this.acumulateAvailableBalance=0,this.acumulateCountableleBalance=0,this.listAccountCurrency=[],this.listAccuntType=[],this.accountName="",this.accountCurrency="",this.accountType="",this.accountAlias="",this.negativeStyle="list-group-item text-danger",this.positiveStyle="list-group-item text-success",this.addEventListener("send-user",({detail:e})=>this._getAccountsUser(e)),this.errors=[],this.addEventListener("load-parameters",({detail:e})=>this._loadParameters(e))}_loadParameters(){this.listAccountCurrency=JSON.parse(sessionStorage.getItem("parametersAccountCurrency")),this.listAccuntType=JSON.parse(sessionStorage.getItem("parametersAccountType"))}async _getAccountsUser(e){this._initLoading(),await this.updateComplete,this.user=e.user;let t=await this.accountDm.getAccountByUserId(this.user.id);this.accounts=t.objects,console.log("this.accounts",this.accounts),this.visibleAmounts=e.user.visible_amounts,this.acumulateAvailableBalance=0,this.acumulateCountableleBalance=0;for(let e=0;e<this.accounts.length;e++){try{this.acumulateAvailableBalance+=this.accounts[e].available_balance,this.acumulateCountableleBalance+=this.accounts[e].countable_balance;let t="[id='"+this.myTrim(this.accounts[e].account_number.replace(/\s/g,"X"))+"']";this.myTrim(this.accounts[e].account_number.replace(/\s/g,"X")),this.querySelector(t).style.display="none"}catch(e){}this._endLoading()}this.acumulateAvailableBalance=(Math.round(100*this.acumulateAvailableBalance)/100).toFixed(2),this.acumulateCountableleBalance=(Math.round(100*this.acumulateCountableleBalance)/100).toFixed(2)}_openNewAccount(){this._cleanNewAccount(),this.querySelector("#new_account_form").style.display="block"}_refreshAccount(){let e={user:this.user};this._getAccountsUser(e)}_cleanNewAccount(){this.accountName="",this.accountCurrency="",this.accountType="",this.accountAlias="",this.querySelector("#account_alias").value="",this.querySelector("#account_name").value="",this.querySelector("#account_currency").value="",this.querySelector("#account_type").value=""}_cancelCreateAccount(){this._cleanNewAccount(),this.querySelector("#new_account_form").style.display="none"}_initLoading(){const e=this.parentElement.parentElement.parentElement.querySelector("cmp-loading");if(null!=e){const t=new CustomEvent("init-loading",{detail:{},bubbles:!0,composed:!0});e.dispatchEvent(t)}}_endLoading(){const e=this.parentElement.parentElement.parentElement.querySelector("cmp-loading");if(null!=e){const t=new CustomEvent("end-loading",{detail:{},bubbles:!0,composed:!0});e.dispatchEvent(t)}}async _createAccount(){this._initLoading(),this.accountName=this.querySelector("#account_name").value,this.accountCurrency=this.querySelector("#account_currency").value,this.accountType=this.querySelector("#account_type").value,this.accountAlias=this.querySelector("#account_alias").value;let e=this._mapAccount(this.user);201==(await this.accountDm.insertAccount(e,this.user.id)).status?(this._refreshAccount(),this.querySelector("#new_account_form").style.display="none",this.errors=[],this.errors.push({message:"El registro se insertó correctamente.",typeMessage:"¡Success! ",typeClass:"alert alert-success"})):(console.log("error"),this.errors=[],this.errors.push({message:"No se pudo insertar el registro.",typeMessage:"Danger! ",typeClass:"alert alert-danger"}));let t=this.parentElement.querySelector("cmp-alert-modal");const s=new CustomEvent("load-alert-component",{detail:{messageStatus:this.errors[0].typeMessage,message:this.errors[0].message,messageStyle:this.errors[0].typeClass},bubbles:!0,composed:!0});this._endLoading(),t.dispatchEvent(s)}_generateCC(){let e=this.numberUtil._zfill(Math.floor(9999*Math.random()+1),4),t=this.numberUtil._zfill(Math.floor(9999*Math.random()+1),4),s=this.numberUtil._zfill(Math.floor(9999999999*Math.random()+1),10);return console.log("CC",e+"-"+t+"-"+s),e+"-"+t+"-"+s}_generateCCI(e){let t=e.split("-"),s=t[0].substr(1,3),i=t[1].substr(1,3),a="00"+t[2];return console.log("CC",s+"-"+i+"-"+a+"-88"),s+"-"+i+"-"+a+"-88"}_mapAccount(e){let t=this._generateCC(),s=this._generateCCI(t),i=this.numberUtil._getStringDate(),a=this.numberUtil._getStringDate();console.log("fc",i),console.log("fu",a);let r={account_name:this.accountName,account_alias:this.accountAlias,account_number:t,account_number_cci:s,available_balance:0,countable_balance:0,id_currency:this.accountCurrency,type:this.accountType,creation_date:i,update_date:a};return console.log("account",r),r}_visualizeAmounts(){}_reloadAccount(){}_openAccountDetail(e){console.log(e.id);let t="[id='"+this.myTrim(e.account_number.replace(/\s/g,"X"))+"']",s=this.querySelector(t);"block"==s.style.display?s.style.display="none":s.style.display="block"}myTrim(e){return e.replace(/^\s+|\s+$/gm,"").replace(/-/g,"")}render(){return I`
     <div class="card">
      <div class="card-header text-dark bg-light text-uppercase font-weight-light">
          <h5 class="card-title">
            <svg class="bi" width="24" height="24" fill="currentColor">
              <use
                xlink:href="./resource/bootstrap-icons.svg#credit-card-fill"
              />
            </svg>

            MIS CUENTAS
          </h5>
        </div>
        </div>
        <br>
      <div class="card" style="">
        <div
          class="card-header text-dark bg-light text-uppercase font-weight-light"
        >
          <h5 class="card-title">
            <svg class="bi" width="20" height="28" fill="currentColor">
              <use
                xlink:href="./resource/bootstrap-icons.svg#grid"
              />
            </svg>

            Resumen de cuentas
          </h5>
        </div>
        <ul class="list-group list-group-flush">
          <li class="list-group-item">
            <h5>
              Saldo disponible cumulado (S/.):
              <b class="text-success">${this.acumulateAvailableBalance}</b>
            </h5>
          </li>
          <li class="list-group-item">
            <h5>
              Saldo contable acumulado(S/.):
              <b class="text-info">${this.acumulateCountableleBalance}</b>
            </h5>
          </li>
          
        </ul>
        <div class="card-footer text-right">
          <button
            type="button"
            class="btn btn-bbva btn-lg"
            @click="${this._openNewAccount}"
          >
            <svg class="bi" width="24" height="24" fill="currentColor">
              <use
                xlink:href="./resource/bootstrap-icons.svg#plus-square"
              />
            </svg>

            Crear cuenta
          </button>
          <button
            type="button"
            class="btn btn-bbva btn-lg"
            @click="${this._refreshAccount}"
          >
            <svg class="bi" width="24" height="24" fill="currentColor">
              <use
                xlink:href="./resource/bootstrap-icons.svg#arrow-repeat"
              />
            </svg>
            Refrescar
          </button>
          <!-- <button
              type="button"
              class="btn btn-dark"
              @click=${this._openNewUser}
            >
              <svg class="bi" width="24" height="24" fill="currentColor">
                <use
                  xlink:href="./resource/bootstrap-icons.svg#person-plus-fill"
                />
              </svg>
              Nuevo
            </button>-->
        </div>
      </div>
      <br />
      <div class="card" style="display:none" id="new_account_form">
        <div
          class="card-header text-dark bg-light text-uppercase font-weight-light"
        >
          <h4 class="card-title">
            <svg class="bi" width="28" height="28" fill="currentColor">
              <use
                xlink:href="./resource/bootstrap-icons.svg#plus-square"
              />
            </svg>

            Nueva cuenta
          </h4>
        </div>

        <ul class="list-group list-group-flush">
          <li class="list-group-item">
            <div class="form-row">
              <div class="form-group col-md-3">
                <h5><label for="account_name">Nombre de la cuenta:</label></h5>
                <input
                  type="text"
                  id="account_name"
                  name="account_name"
                  class="form-control inputForm"
                  .value="${this.accountName}"
                />
              </div>
              <div class="form-group col-md-3">
                <h5><label for="account_alias">Alias de la cuenta:</label></h5>
                <input
                  type="text"
                  id="account_alias"
                  name="account_alias"
                  class="form-control inputForm"
                  .value="${this.accountAlias}"
                />
              </div>
              <div class="form-group col-md-3">
                <h5><label for="account_type">Tipo de cuenta</label></h5>
                <select
                  name="account_type"
                  class="custom-select mr-sm-2"
                  id="account_type"
                >
                  <option value="">--Seleccione uno--</option>
                  ${this.listAccuntType&&this.listAccuntType.map(e=>I`<option .value="${e.parameter_value}">
                        ${e.parameter_name}
                      </option>`)}
                </select>
              </div>
              <div class="form-group col-md-3">
                <h5><label for="account_currency">Divisa</label></h5>
                <select
                  name="account_currency"
                  class="custom-select mr-sm-2"
                  id="account_currency"
                >
                  <option value="">--Seleccione uno--</option>
                  ${this.listAccountCurrency&&this.listAccountCurrency.map(e=>I`<option .value="${e.parameter_value}">
                        ${e.parameter_name}
                      </option>`)}
                </select>
              </div>
            </div>
          </li>
        </ul>

        <div class="card-footer text-right">
          <button
            type="button"
            class="btn btn-bbva btn-lg"
            @click="${this._createAccount}"
          >
            <svg class="bi" width="24" height="24" fill="currentColor">
              <use
                xlink:href="./resource/bootstrap-icons.svg#box-arrow-in-down"
              />
            </svg>

            Guardar
          </button>
          <button
            type="button"
            class="btn btn-bbva btn-lg"
            @click="${this._cleanNewAccount}"
          >
            <svg class="bi" width="24" height="24" fill="currentColor">
              <use
                xlink:href="./resource/bootstrap-icons.svg#trash"
              />
            </svg>
            Limpiar
          </button>
          <button
            type="button"
            class="btn btn-bbva btn-lg"
            @click="${this._cancelCreateAccount}"
          >
            <svg class="bi" width="24" height="24" fill="currentColor">
              <use
                xlink:href="./resource/bootstrap-icons.svg#x-circle"
              />
            </svg>
            Cancelar
          </button>
          <!-- <button
         type="button"
         class="btn btn-dark"
         @click=${this._openNewUser}
       >
         <svg class="bi" width="24" height="24" fill="currentColor">
           <use
             xlink:href="./resource/bootstrap-icons.svg#person-plus-fill"
           />
         </svg>
         Nuevo
       </button>-->
        </div>
      </div>
      <br />
      <div id="accordion">
        ${this.accounts&&this.accounts.map(e=>I` <div class="card ">
                <div
                  class="card-header text-dark bg-light text-uppercase font-weight-light"
                  id="headingOne"
                >
                  <div class="form-row ">
                    <div class="form-group col-sm-6">
                      <div class="input-group">
                        <a href="#"
                          class=""
                          @click=${()=>this._openAccountDetail(e)}
                          data-toggle="collapse"
                          data-target="#collapseOne"
                          aria-expanded="true"
                          aria-controls="collapseOne"
                        >
                          <h5>
                            <b
                              >${e.account_name+" ("+e.account_alias+")"}
                            </b>
                          </h5>
                        </a>
                      </div>
                    </div>
                    <div class="form-group col-sm-6">
                      <label
                        ><h5>
                          ${"Divisa: "}<b> ${e.id_currency}</b>
                        </h5></label
                      >
                    </div>
                  </div>
                  <div class="form-row">
                    <div class="form-group col-sm-6">
                      <label>
                        <h5>${"C.C: "}<b> ${e.account_number}</b></h5></label
                      >
                    </div>
                    <div class="form-group col-sm-6">
                      <label
                        ><h5>
                          ${"Saldo disponible: "}<b class="text-success">
                            ${e.available_balance}</b
                          >
                        </h5></label
                      >
                    </div>
                  </div>
                  <div class="form-row">
                    <div class="form-group col-sm-6">
                      <label
                        ><h5>
                          ${"C.C.I: "}<b> ${e.account_number_cci}</b>
                        </h5></label
                      >
                    </div>
                    <div class="form-group col-sm-6">
                      <label
                        ><h5>
                          ${"Saldo contable: "}<b class="text-info">
                            ${e.countable_balance}</b
                          >
                        </h5></label
                      >
                    </div>
                  </div>
                </div>

                <div
                  id="collapseOne"
                  class="collapseDetail"
                  aria-labelledby="headingOne"
                  data-parent="#accordion"
                  style="display:none"
                  id="${this.myTrim(e.account_number.replace(/\s/g,"X"))}"
                >
                  <div class="card-body">
                    ${e.transactions&&e.transactions.map(e=>I`
                        <div class="card">
                          <div class="card-body text-light bg-secondary">
                           <h5 class="card-title"><p>
                              <svg style="${e.amount<0?"display:none":"display:inline"}"
                                class="bi"
                                width="24"
                                height="24"
                                fill="currentColor"
                              >
                                <use
                                  xlink:href="./resource/bootstrap-icons.svg#box-arrow-in-down"
                                />
                              </svg>
                              <svg style="${e.amount>0?"display:none":"display:inline"}"
                                class="bi"
                                width="24"
                                height="24"
                                fill="currentColor"
                              >
                                <use 
                                  xlink:href="./resource/bootstrap-icons.svg#box-arrow-up"
                                />
                              </svg>
                              ${e.reason}</p>
                            </h5>
                            <p class="card-text">S${e.description}</p>
                          </div>
                          <ul class="list-group list-group-flush">
                            <li class="${e.amount<0?this.negativeStyle:this.positiveStyle}">
                              Monto: &nbsp; ${e.amount}
                            </li>
                            <li class="list-group-item">
                              Número de operación: &nbsp; ${e.number}
                            </li>
                            <li class="list-group-item">
                              Fecha de operación: &nbsp; ${e.date}
                            </li>
                          </ul>
                        </div>
                      `)}
                  </div>
                </div>
              </div>

              <span> </span>`)}
      </div>
    `}});customElements.define("cmp-user-transaction",class extends te{createRenderRoot(){return this}get accountDm(){return ne()}get numberUtil(){return re()}static get properties(){return{user:{type:Object},listAccountGet:{type:Array},listAccountSet:{type:Array},accountSelectedGet:{type:Object},accountSelectedSet:{type:Object},operatioResult:{type:Object},isAccountCheckGet:{type:Boolean},isAccountCheckSet:{type:Boolean},amountTransaction:{type:Number},reasonTransaction:{type:String},errors:{type:Array}}}constructor(){super(),this.user={},this.listAccountGet=[],this.listAccountSet=[],this.accountSelectedGet={},this.accountSelectedSet={},this.isAccountCheckGet=!1,this.isAccountCheckSet=!1,this.amountTransaction=0,this.operatioResult={},this.reasonTransaction="",this.addEventListener("send-user",({detail:e})=>this._loadUser(e)),this._initPage(),this.errors=[]}async _initPage(){await this.updateComplete,this.querySelector("#paso_uno").style.display="block",this.querySelector("#paso_dos").style.display="none",this.querySelector("#paso_tres").style.display="none"}_setStepOne(){if(this.amountTransaction=this.querySelector("#amountTransaction").value,this.reasonTransaction=this.querySelector("#reasonTransaction").value,this._errorOnStepOne()){let e=this.parentElement.querySelector("cmp-alert-modal");const t=new CustomEvent("load-alert-component",{detail:{messageStatus:this.errors[0].typeMessage,message:this.errors[0].message,messageStyle:this.errors[0].typeClass},bubbles:!0,composed:!0});e.dispatchEvent(t)}else this.querySelector("#paso_uno").style.display="none",this.querySelector("#paso_dos").style.display="block",this.querySelector("#paso_tres").style.display="none"}_errorOnStepOne(){return this.errors=[],null==this.accountSelectedGet.id?(this.errors.push({message:"Debe seleccionar cuenta cargo.",typeMessage:"Danger! ",typeClass:"alert alert-danger"}),!0):null==this.accountSelectedSet.id?(this.errors.push({message:"Debe seleccionar cuenta abono.",typeMessage:"Danger! ",typeClass:"alert alert-danger"}),!0):0==this.amountTransaction?(this.errors.push({message:"Debe ingresar monto.",typeMessage:"Danger! ",typeClass:"alert alert-danger"}),!0):""==this.reasonTransaction?(this.errors.push({message:"Debe ingresar una razon.",typeMessage:"Danger! ",typeClass:"alert alert-danger"}),!0):/^([0-9]+\.?[0-9]{0,2})$/.test(this.amountTransaction)?this.accountSelectedGet.available_balance<this.amountTransaction?(this.errors.push({message:"No cuenta con saldo suficiente",typeMessage:"Danger! ",typeClass:"alert alert-danger"}),!0):void 0:(this.errors.push({message:"ingrese un monto de transaccion valido.",typeMessage:"Danger! ",typeClass:"alert alert-danger"}),!0)}_initLoading(){const e=this.parentElement.parentElement.parentElement.querySelector("cmp-loading");if(null!=e){const t=new CustomEvent("init-loading",{detail:{},bubbles:!0,composed:!0});e.dispatchEvent(t)}}_endLoading(){const e=this.parentElement.parentElement.parentElement.querySelector("cmp-loading");if(null!=e){const t=new CustomEvent("end-loading",{detail:{},bubbles:!0,composed:!0});e.dispatchEvent(t)}}async _setStepTwo(){this._initLoading(),this.accountSelectedGet.available_balance=parseFloat(this.accountSelectedGet.available_balance)-parseFloat(this.amountTransaction),this.accountSelectedGet.countable_balance=parseFloat(this.accountSelectedGet.countable_balance)-parseFloat(this.amountTransaction),this.accountSelectedSet.available_balance=parseFloat(this.accountSelectedSet.available_balance)+parseFloat(this.amountTransaction),this.accountSelectedSet.countable_balance=parseFloat(this.accountSelectedSet.countable_balance)+parseFloat(this.amountTransaction);let e=this._getTransactionBody(this.accountSelectedGet,"retiro"),t=this._getTransactionBody(this.accountSelectedGet,"abono");e.id_account=this.accountSelectedGet.id,t.id_account=this.accountSelectedSet.id;let s=await this.accountDm.insertTransaction(e,this.accountSelectedGet.id),i=await this.accountDm.insertTransaction(t,this.accountSelectedSet.id);if(this.errors=[],201==s.status&&201==i.status){await this.accountDm.updateAccount(this.accountSelectedGet,this.accountSelectedGet.id_user,this.accountSelectedGet.id),await this.accountDm.updateAccount(this.accountSelectedSet,this.accountSelectedSet.id_user,this.accountSelectedSet.id);this.operatioResult={numberOpe:e.number,ccOutOpe:this.accountSelectedGet.account_number,ccInOpe:this.accountSelectedSet.account_number,amountOpe:t.amount,dateOpe:e.date,descriptionOpe:e.reason},this.querySelector("#paso_uno").style.display="none",this.querySelector("#paso_dos").style.display="none",this.querySelector("#paso_tres").style.display="block",this.errors.push({message:"La transferencia se ejecutó correctamente.",typeMessage:"¡Success! ",typeClass:"alert alert-success"})}else this.errors.push({message:"La transferencia no se ejecutó correctamente.",typeMessage:"¡Success! ",typeClass:"alert alert-success"});this._endLoading();let a=this.parentElement.querySelector("cmp-alert-modal");const r=new CustomEvent("load-alert-component",{detail:{messageStatus:this.errors[0].typeMessage,message:this.errors[0].message,messageStyle:this.errors[0].typeClass},bubbles:!0,composed:!0});a.dispatchEvent(r)}_getTransactionBody(e,t){let s={number:this.numberUtil._zfill(Math.floor(9999*Math.random()+1),4),date:this.numberUtil._getStringDate(),description:this.reasonTransaction,reason:"Tranferencia a cuentas propias",amount:"retiro"==t?-Math.abs(this.amountTransaction):parseFloat(this.amountTransaction)};return console.log("transaction",s),s}_setStepThree(){this.listAccountGet=[],this.listAccountSet=[],this.accountSelectedGet={},this.accountSelectedSet={},this.isAccountCheckGet=!1,this.isAccountCheckSet=!1,this.amountTransaction=0,this.reasonTransaction="",this.operatioResult={};let e={user:this.user};this._loadUser(e),this.querySelector("#paso_uno").style.display="block",this.querySelector("#paso_dos").style.display="none",this.querySelector("#paso_tres").style.display="none"}_backStepOne(){this.querySelector("#paso_uno").style.display="block",this.querySelector("#paso_dos").style.display="none",this.querySelector("#paso_tres").style.display="none"}async _loadUser(e){this.user=e.user;let t=await this.accountDm.getAccountByUserId(this.user.id);this.listAccountGet=t.objects,console.log("this.accounts",this.accounts)}_getAccountGet(e){this.accountSelectedSet={},this.accountSelectedGet=e,console.log("this",e),this.listAccountSet=[];for(let t=0;t<this.listAccountGet.length;t++)this.listAccountGet[t].account_number!=e.account_number&&this.listAccountSet.push(this.listAccountGet[t])}_getAccountSet(e){this.accountSelectedSet=e,console.log("_getAccountSet",e)}_limpiar(){this.accountSelectedSet={},this.accountSelectedGet={},this.listAccountSet=[],this.amountTransaction=0,this.querySelector("#amountTransaction").value=0,this.querySelector("#reasonTransaction").value="",this.reasonTransaction="",console.log("_getAccountSet",account)}render(){return I`
    
      <div class="card" >
        <div
          class="card-header text-dark bg-light text-uppercase font-weight-light"
        >
          <h5 class="card-title">
            <svg class="bi" width="24" height="24" fill="currentColor">
              <use
                xlink:href="./resource/bootstrap-icons.svg#box-arrow-up-right"
              />
            </svg>

            TRANSFERENCIAS
          </h5>
        </div>
      </div>
      <br />

      <div class="card" id="paso_uno">
        <div
          class="card-header text-dark bg-light text-uppercase font-weight-light"
        >
          <h5 class="card-title">
            <svg class="bi" width="24" height="24" fill="currentColor">
              <use
                xlink:href="./resource/bootstrap-icons.svg#arrow-down-up"
              />
            </svg>

            PASO 01 - TRANSFERENCIA A CUANTAS PROPIAS
          </h5>
        </div>

        <div class="card-body">
          <div class="form-row">
            <div class="form-group col-md-6">
              <ul class="list-group" id="list_account_get_get">
                <li
                  class="list-group-item disabled list-group-item list-group-item-action bg-bbva text-light"
                >
                  CUENTA CARGO
                </li>

                ${this.listAccountGet.map(e=>I`
                    
                     
                       <button type="button" href="#" class="list-group-item list-group-item-action"  @click="${()=>this._getAccountGet(e)}">
                        ${e.account_name+" ("+e.account_alias+") - Divisa: "+e.id_currency+") - C.C.: "+e.account_number+" Saldo disponible: "+e.available_balance}   </button>
                   
                       
                    
                  `)}
                
              </ul>

              <br />
            </div>

            <div class="form-group col-md-6 ">
              <ul class="list-group">
                <li
                  class="list-group-item disabled list-group-item list-group-item-action bg-bbva text-light"
                >
                  CUENTA DE ABONO
                </li>
                ${this.listAccountSet.map(e=>I`

                      <button type="button" href="#" class="list-group-item list-group-item-action"  @click="${()=>this._getAccountSet(e)}">
                        ${e.account_name+" ("+e.account_alias+") - Divisa: "+e.id_currency+") - C.C.: "+e.account_number+" Saldo disponible: "+e.available_balance}    </button>
                   
                  
                  `)}
              </ul>
              <br />
            </div>
          </div>


          





<div class="form-row">
            <div class="form-group col-md-6">
              <ul class="list-group" id="list_account_get_get">
                <li
                  class="list-group-item disabled list-group-item list-group-item-action bg-bbva text-light"
                >
                  CUENTA CARGO SELECCIONADA
                </li>

                <li class="list-group-item"> ${null!=this.accountSelectedGet.account_number?this.accountSelectedGet.account_name+" ("+this.accountSelectedGet.account_alias+") - Divisa: "+this.accountSelectedGet.id_currency+") - C.C.: "+this.accountSelectedGet.account_number:""} 

                </li>
                <li class="list-group-item">
                ${null!=this.accountSelectedGet.account_number?"Saldo disponible: "+this.accountSelectedGet.available_balance:""} 
                </li> 
                       
                    
                  
                
                
              </ul>

              <br />
            </div>

            <div class="form-group col-md-6 ">
              <ul class="list-group">
                <li
                  class="list-group-item disabled list-group-item list-group-item-action bg-bbva text-light"
                >
                  CUENTA DE ABONO SELECCIONADA
                </li>
                <li class="list-group-item"> ${null!=this.accountSelectedSet.account_number?this.accountSelectedSet.account_name+" ("+this.accountSelectedSet.account_alias+") - Divisa: "+this.accountSelectedSet.id_currency+") - C.C.: "+this.accountSelectedSet.account_number:""} 

                </li>
                <li class="list-group-item">
                ${null!=this.accountSelectedSet.account_number?"Saldo disponible: "+this.accountSelectedSet.available_balance:""} 
                </li>    
                     
                     
                       
                    
               
              </ul>
              <br />
            </div>
          </div>


          <div class="form-row">
            <div class="form-group col-md-12">
              <div class="card">
                <div class="card-body">
                  <ul class="list-group list-group-flush">
                    <li class="list-group-item">
                      Monto:
                      <input
                        type="number"
                        id="amountTransaction"
                        name="amountTransaction"
                        class="form-control inputForm"
                        .value="${this.amountTransaction}"
                     
                      />
                    </li>
                    <li class="list-group-item">
                      Referencia:
                      <input
                        type="text"
                        id="reasonTransaction"
                        name="reasonTransaction"
                        class="form-control inputForm"
                        .value="${this.reasonTransaction}"
                      />
                    </li>
                    <li class="list-group-item"></li>
                  </ul>
                </div>
                <div class="card-foother">
                  <div class="form-group col-md-12 text-right">
                  <button
                      type="button"
                      class="btn btn-bbva btn-lg"
                      @click="${this._limpiar}"
                    >
                      LIMPIAR
                      <svg
                        class="bi"
                        width="24"
                        height="24"
                        fill="currentColor"
                      >
                        <use
                          xlink:href="./resource/bootstrap-icons.svg#trash-fill"
                        />
                      </svg>
                    </button>
                    <button
                      type="button"
                      class="btn btn-bbva btn-lg"
                      @click="${this._setStepOne}"
                    >
                      TRANSFERIR
                      <svg
                        class="bi"
                        width="24"
                        height="24"
                        fill="currentColor"
                      >
                        <use
                          xlink:href="./resource/bootstrap-icons.svg#box-arrow-right"
                        />
                      </svg>
                    </button>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>




 

      <div class="card" id="paso_dos">
        <div
          class="card-header text-dark bg-light text-uppercase font-weight-light"
        >
          <h5 class="card-title">
            <svg class="bi" width="24" height="24" fill="currentColor">
              <use
                xlink:href="./resource/bootstrap-icons.svg#arrow-down-up"
              />
            </svg>

            PASO 02 - TRANSFERENCIAS A CUENTAS PROPIAS
          </h5>
        </div>

        <div class="card-body">
          <div class="form-row">
            <div class="form-group col-md-5">
              <ul class="list-group">
                <li
                  class="list-group-item disabled list-group-item list-group-item-action bg-bbva text-light"
                >
                  CUANTA A CARGO
                </li>
                <li class="list-group-item">
               
                  <p class="font-weight-lighter">${this.accountSelectedGet.account_name+" ("+this.accountSelectedGet.account_alias+") "}</p>
                
                  <p class="font-weight-lighter">${"C.C: "+this.accountSelectedGet.account_number}</p>
                
                  <p class="font-weight-lighter">${"Divisa: "+this.accountSelectedGet.id_currency+" (-)"+this.amountTransaction}</p>
                
                  </li>
                  </ul>
            </div>

            <div class="form-group col-md-2 text-center">
                    <p>
                
                <svg
                  class="bi"
                  width="24"
                  height="24"
                  fill="currentColor"
                >
                  <use
                    xlink:href="./resource/bootstrap-icons.svg#arrow-right-square"
                  />
                </svg>
                 
                  </p>
                 <p>
                  <svg
                  class="bi"
                  width="24"
                  height="24"
                  fill="currentColor"
                >
                  <use
                    xlink:href="./resource/bootstrap-icons.svg#arrow-right-square"
                  />
                </svg>
                </p>
                 <p>
                  <svg
                  class="bi"
                  width="24"
                  height="24"
                  fill="currentColor"
                >
                  <use
                    xlink:href="./resource/bootstrap-icons.svg#arrow-right-square"
                  />
                </svg>
                </p>
                 <p>
                  <svg
                  class="bi"
                  width="24"
                  height="24"
                  fill="currentColor"
                >
                  <use
                    xlink:href="./resource/bootstrap-icons.svg#arrow-right-square"
                  />
                </svg>
                </p>
                <p>
                  <svg
                  class="bi"
                  width="24"
                  height="24"
                  fill="currentColor"
                >
                  <use
                    xlink:href="./resource/bootstrap-icons.svg#arrow-right-square"
                  />
                </svg>
                </p>

            </div>
            <div class="form-group col-md-5">
              <ul class="list-group">
                <li
                  class="list-group-item disabled list-group-item list-group-item-action bg-bbva text-light"
                >
                  CUENTA DE ABONO
                </li>
                <li class="list-group-item">
               
                  <p class="font-weight-lighter">${this.accountSelectedSet.account_name+" ("+this.accountSelectedSet.account_alias+") "}</p>
                
                  <p class="font-weight-lighter">${"C.C: "+this.accountSelectedSet.account_number}</p>
                
                  <p class="font-weight-lighter">${"Divisa: "+this.accountSelectedSet.id_currency+" (+)"+this.amountTransaction}</p>
                
                  </li>
              </ul>
              <br />
            </div>
            
          </div>
          <div class="card-foother">
                  <div class="form-group col-md-12 text-right">
                  <button
                type="button"
                class="btn btn-bbva btn-lg"
                @click="${this._backStepOne}"
              >
                ATRAS
                <svg
                  class="bi"
                  width="24"
                  height="24"
                  fill="currentColor"
                >
                  <use
                    xlink:href="./resource/bootstrap-icons.svg#arrow-return-left"
                  />
                </svg>
              </button>
                    <button
                      type="button"
                      class="btn btn-bbva btn-lg"
                      @click="${this._setStepTwo}"
                    >
                      CONFIRMAR
                      <svg
                        class="bi"
                        width="24"
                        height="24"
                        fill="currentColor"
                      >
                        <use
                          xlink:href="./resource/bootstrap-icons.svg#check"
                        />
                      </svg>
                    </button>
                  </div>
                </div>
                <br>
        </div>
      </div>
      

<div class="card" id="paso_tres"> 
  <div
    class="card-header text-dark bg-light text-uppercase font-weight-light"
  >
    <h5 class="card-title">
      <svg class="bi" width="24" height="24" fill="currentColor">
        <use
          xlink:href="./resource/bootstrap-icons.svg#arrow-down-up"
        />
      </svg>

      PASO 03 - TRANSFERENCIAS A CUENTAS PROPIAS
    </h5>
  </div>

  <div class="card-body">
    <div class="form-row">
      <div class="form-group col-md-12">
        
        <ul class="list-group">
          <li
            class="list-group-item disabled list-group-item list-group-item-action bg-bbva text-light"
          >
            CONSTANCIA DE OPERACIÓN
          </li>
                  <li class="list-group-item"><p>N° de operacion: ${this.operatioResult.numberOpe}</p></li>
          <li class="list-group-item">C.C. A carg: ${this.operatioResult.ccOutOpe}</li>
          <li class="list-group-item">C.C. Abono: ${this.operatioResult.ccInOpe}</li>
          <li class="list-group-item">Monto: ${this.operatioResult.amountOpe}</li>
          <li class="list-group-item">Fecha: ${this.operatioResult.dateOpe}</li>
          <li class="list-group-item">Motivo: ${this.operatioResult.descriptionOpe}</li>


          
        </ul>
        <br />
      </div>
      
    </div>
    <div class="card-foother">
            <div class="form-group col-md-12 text-right">
            
              <button
                type="button"
                class="btn btn-bbva btn-lg"
                @click="${this._setStepThree}"
              >
                SALIR
                <svg
                  class="bi"
                  width="24"
                  height="24"
                  fill="currentColor"
                >
                  <use
                    xlink:href="./resource/bootstrap-icons.svg#door-closed"
                  />
                </svg>
              </button>
            </div>
          </div>
          <br>
  </div>
</div>
</br>
     
    `}});customElements.define("cmp-singin",class extends te{createRenderRoot(){return this}get userDm(){return ie()}static get properties(){return{documentType:{type:String},documentNumber:{type:String},password:{type:String},documentTypeList:{type:Array}}}constructor(){super(),this.documentType="",this.documentNumber="",this.password="",this.documentTypeList=[],this.addEventListener("send-list-type-document",({detail:e})=>this._loadParameters(e))}async _loadParameters(e){this.documentTypeList=e.list}async _singIn(){let e={document_type:this.querySelector("#document_type").value,document_number:this.querySelector("#document_number").value,password:this.querySelector("#password").value},t=await this.userDm.singIn(e),s=t.json,i=t.status;this._sendSingInResult(i,s)}_sendSingInResult(e,t){let s=new CustomEvent("send-event-singin-result",{detail:{status:e,user:t},bubbles:!0,composed:!0});this.dispatchEvent(s)}render(){return I`
      <link rel="stylesheet" href="./src/components/style-singin.css" />
      <section class="login-block">
        <div class="container">
          <div class="card">
            <div
              class="card-header text-light bg-bbva text-uppercase font-weight-light text-center"
            >
              <h5 class="card-title">INICIO DE SESIÓN</h5>
            </div>
          </div>
          <div class="card">
            <div class="card-body ">
              <div class="form-row">
                <div class="form-group col-md-12">
                  <label for="document_type">Tipo de documento</label>
                  <select
                   
                    name="document_type"
                    class="custom-select mr-sm-3"
                    id="document_type"
                    name="document_type"
                  >
                    <option value="">--Seleccione uno--</option>
                    ${this.documentTypeList&&this.documentTypeList.map(e=>I`<option .value="${e.parameter_value}">
                          ${e.parameter_name}
                        </option>`)}
                  </select>
                </div>
              </div>

              <div class="form-row">
                <div class="form-group col-md-12">
                  <label for="document_number">Nº de documento</label>
                  <input
                    type="text"
                    
                    class="form-control inputForm"
                    id="document_number"
                    name="document_number"
                    .value="${this.documentNumber}"
                  />
                </div>
              </div>

              <div class="form-row">
                <div class="form-group col-md-12"></div>
                <label for="password">Password</label>
                <input
                  type="password"
                  
                  class="form-control inputForm"
                  id="password"
                  name="password"
                  .value="${this.password}"
                />
              </div>
            </div>
          </div>
          <div class="card-footer text-center">
            <button
              type="button"
              class="btn btn-bbva btn-lg"
              @click="${this._singIn}"
            >
              ACEPTAR
            </button>
          </div>
        </div>
      </section>
    `}});class le{async _getParameter(){let e={},t=await fetch("https://fierce-reaches-83730.herokuapp.com/techU/v1/parameters");if(t.ok)if(200==t.status){let s=await t.json();e.objects=s,e.status=t.status}else e.objects=[],e.status=204;else e.objects=[],e.status=500;return e}}customElements.define("cmp-main",class extends te{createRenderRoot(){return this}static get properties(){return{isAdmin:{type:Boolean}}}get parameterDm(){return new le}constructor(){super(),this._getParameter(),this.isAdmin=!1}async _getParameter(){await this.updateComplete;let e=await this.parameterDm._getParameter();if(200==e.status){let t=[],s=[],i=[],a=[],r=[],o=[],n=[];for(let l=0;l<e.objects.length;l++)"TIPO_DOCUMENTO"==e.objects[l].parameter_code?t.push(e.objects[l]):"PERFILES"==e.objects[l].parameter_code?s.push(e.objects[l]):"DEPARTAMENTOS"==e.objects[l].parameter_code?i.push(e.objects[l]):"PAIS"==e.objects[l].parameter_code?a.push(e.objects[l]):"TIPO"==e.objects[l].parameter_code?r.push(e.objects[l]):"TIPO_CUENTA"==e.objects[l].parameter_code?o.push(e.objects[l]):"DIVISA"==e.objects[l].parameter_code&&n.push(e.objects[l]);sessionStorage.setItem("parametersDocumentType",JSON.stringify(t)),sessionStorage.setItem("parametersProfiles",JSON.stringify(s)),sessionStorage.setItem("parametersCity",JSON.stringify(i)),sessionStorage.setItem("parametersCountry",JSON.stringify(a)),sessionStorage.setItem("parametersType",JSON.stringify(r)),sessionStorage.setItem("parametersAccountType",JSON.stringify(o)),sessionStorage.setItem("parametersAccountCurrency",JSON.stringify(n))}let t=JSON.parse(sessionStorage.getItem("appUser"));null!=t&&1==t.logged?(console.log(t),"ADM"==t.profile.code?this.isAdmin=!0:this.isAdmin=!1,this._sendUserToComponent("cmp-app-user"),this.querySelector("#cmpSingin").style.display="none",this.querySelector("#allIndex").style.display="block"):(this._sendTypeDocument("cmp-singin"),this.querySelector("#cmpSingin").style.display="block"),this._endLoading()}_initLoading(){const e=this.querySelector("cmp-loading"),t=new CustomEvent("init-loading",{detail:{},bubbles:!0,composed:!0});e.dispatchEvent(t)}_endLoading(){const e=this.querySelector("cmp-loading"),t=new CustomEvent("end-loading",{detail:{},bubbles:!0,composed:!0});e.dispatchEvent(t)}_sendUserToComponent(e){let t=JSON.parse(sessionStorage.getItem("appUser"));const s=this.querySelector(e),i=new CustomEvent("send-user",{detail:{user:t},bubbles:!0,composed:!0});s.dispatchEvent(i)}_sendParameterToComponent(e){const t=this.querySelector(e),s=new CustomEvent("load-parameters",{detail:{},bubbles:!0,composed:!0});t.dispatchEvent(s)}_sendTypeDocument(e){let t=JSON.parse(sessionStorage.getItem("parametersDocumentType"));const s=this.querySelector(e),i=new CustomEvent("send-list-type-document",{detail:{list:t},bubbles:!0,composed:!0});s.dispatchEvent(i)}_activeContentUser(e){this._inactiveOtherTapContent(e),this.querySelector("#idClientAccount").style.display="none",this.querySelector("#idClientInfo").style.display="none",this.querySelector("#idClientTransaction").style.display="none",this.querySelector("#idUserContent").style.display="block",this._sendUserToComponent("cmp-app-user"),this._sendParameterToComponent("cmp-list-user")}_activeContenClientInfo(e){this._inactiveOtherTapContent(e),this.querySelector("#idClientAccount").style.display="none",this.querySelector("#idUserContent").style.display="none",this.querySelector("#idClientTransaction").style.display="none",this.querySelector("#idClientInfo").style.display="block",this._sendUserToComponent("cmp-user-info"),this._sendParameterToComponent("cmp-user-info")}_activeContenClientAccount(e){this._inactiveOtherTapContent(e),this.querySelector("#idUserContent").style.display="none",this.querySelector("#idClientInfo").style.display="none",this.querySelector("#idClientTransaction").style.display="none",this.querySelector("#idClientAccount").style.display="block",this._sendUserToComponent("cmp-user-account"),this._sendParameterToComponent("cmp-user-account")}_activeContenClientTransaction(e){this._inactiveOtherTapContent(e),this.querySelector("#idUserContent").style.display="none",this.querySelector("#idClientInfo").style.display="none",this.querySelector("#idClientAccount").style.display="none",this.querySelector("#idClientTransaction").style.display="block",this._sendUserToComponent("cmp-user-transaction")}_inactiveOtherTapContent(e){this.querySelectorAll(".tablinks").forEach(e=>{try{e.className="tablinks"}catch(e){}}),e.target.parentElement.className+=" active"}_getSinginUser(e){if(200==e.status){let t=e.user;sessionStorage.setItem("appUser",JSON.stringify(t)),this._sendUserToComponent("cmp-app-user"),this.querySelector("#allIndex").style.display="block",this.querySelector("#cmpSingin").style.display="none","ADM"==t.profile.code?this.isAdmin=!0:this.isAdmin=!1}else{let e=this.querySelector("#appAlert");const t=new CustomEvent("load-alert-component",{detail:{messageStatus:"Error ",message:"Usuario o password incorrectos",messageStyle:"alert alert-danger"},bubbles:!0,composed:!0});e.dispatchEvent(t)}}render(){return I`
      <style>
        /* Style the tab */
        .tab {
          float: left;
          border: 1px solid #ccc;
          background-color: #f1f1f1;
          width: 20%;
          height: 1050px;
        }

        /* Style the buttons inside the tab */
        .tab button {
          display: block;
          background-color: inherit;
          color: black;
          padding: 22px 16px;
          width: 100%;
          border: none;
          outline: none;
          text-align: left;
          cursor: pointer;
          transition: 0.3s;
          font-size: 17px;
        }

        /* Change background color of buttons on hover */
        .tab button:hover {
          background-color: #ddd;
        }

        /* Create an active/current "tab button" class */
        .tab button.active {
          background-color: #ccc;
        }

        /* Style the tab content */
        .tabcontent {
          float: left;
          padding: 0px 12px;
          border: 1px solid #ccc;
          width: 80%;
          border-left: none;
          height: 1050px;
          display: none;
        }
      </style>
       <cmp-singin id="cmpSingin" style="display:none" @send-event-singin-result="${({detail:e})=>this._getSinginUser(e)}" ></cmp-singin>
         <cmp-alert-modal id="appAlert"></cmp-alert-modal>
      <div id="allIndex" style="display:none">
     
      <cmp-app-user id="appUser"></cmp-app-user>
      <cmp-loading></cmp-loading>
     
      
      <div class="tab">
        <button class="tablinks">
          <form class="form-inline">
            <div class="form-group mb-2">
              <svg class="bi " width="36" height="36" fill="currentColor">
                <use
                  xlink:href="./resource/bootstrap-icons.svg#house-fill"
                />
              </svg>
              <span><h1>&nbsp;TechU!</h1></span>
            </div>
          </form>
        </button>
        <button
          class="tablinks" ?disabled="${!this.isAdmin}" ?hidden="${!this.isAdmin}"
          @click="${this._activeContentUser}"
          id="defaultOpen"
        >
          <form class="form-inline">
            <div class="form-group mb-2 text-uppercase font-weight-light">
              &nbsp;&nbsp;&nbsp;&nbsp;
              <svg class="bi " width="24" height="24" fill="currentColor">
                <use
                  xlink:href="./resource/bootstrap-icons.svg#person-circle"
                />
              </svg>
              <span><h5>&nbsp;&nbsp;Usuarios</h5></span>
            </div>
          </form>
        </button>
        <button class="tablinks " @click="${this._activeContenClientInfo}">
          <form class="form-inline"> 
            <div class="form-group mb-2 text-uppercase font-weight-light">
              &nbsp;&nbsp;&nbsp;&nbsp;
              <svg class="bi " width="24" height="24" fill="currentColor">
                <use
                  xlink:href="./resource/bootstrap-icons.svg#person-lines-fill"
                />
              </svg>
              <p ><h5 >&nbsp;&nbsp;Mis Datos</h5></p>
            </div>
          </form>
        </button>
        <button class="tablinks" @click="${this._activeContenClientAccount}">
          <form class="form-inline">
            <div class="form-group mb-2 text-uppercase font-weight-light">
              &nbsp;&nbsp;&nbsp;&nbsp;
              <svg class="bi " width="24" height="24" fill="currentColor">
                <use
                  xlink:href="./resource/bootstrap-icons.svg#credit-card-fill"
                />
              </svg>
              <span><h5>&nbsp;&nbsp;Mis Cuentas</h5></span>
            </div>
          </form>
        </button>
        <button class="tablinks" @click="${this._activeContenClientTransaction}">
          <form class="form-inline">
            <div class="form-group mb-2 text-uppercase font-weight-light">
              &nbsp;&nbsp;&nbsp;&nbsp;
              <svg class="bi " width="24" height="24" fill="currentColor">
                <use
                  xlink:href="./resource/bootstrap-icons.svg#box-arrow-up-right"
                />
              </svg>
              <span><h5>&nbsp;&nbsp;Transferencias</h5></span>
            </div>
          </form>
        </button>
      </div>

      <div id="idUserContent" class="tabcontent overflow-auto">
        <cmp-alert-modal></cmp-alert-modal>
        
        
        <br />
        <cmp-list-user></cmp-list-user>
      </div>

      <div id="idClientInfo" class="tabcontent overflow-auto">
        <cmp-alert-modal></cmp-alert-modal>
        
       
        <br />
        <cmp-user-info></cmp-user-info>
      </div>

      <div id="idClientAccount" class="tabcontent overflow-auto">
        <cmp-alert-modal></cmp-alert-modal>
        
       
        <br />
        <cmp-user-account></cmp-user-account>
      </div>

       <div id="idClientTransaction" class="tabcontent overflow-auto">
        <cmp-alert-modal></cmp-alert-modal>
        
       
        <br />
        <cmp-user-transaction></cmp-user-transaction>
      </div>
      </div>
    `}})}]);