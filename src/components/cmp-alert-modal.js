import { LitElement, css, html } from "lit-element";

class CmpAlertModal extends LitElement {
  createRenderRoot() {
    return this;
  }

  static get properties() {
    return {
      message: { type: String },
      messageTitle: { type: String },
      messageStyle: { type: String },
      messageCode: { type: String },
      messageStatus: { type: String },
    };
  }

  constructor() {
    super();
    this.message = "";
    this.messageTitle = "";
    this.messageStyle = "oculto";
    this.messageCode = "";
    this.messageStatus = "";
    this.addEventListener("load-alert-component", ({ detail }) =>
      this._load(detail)
    );
  }

  _load(detail) {
    this.messageStatus = detail.messageStatus;
    this.message = detail.message;
    this.messageStyle = detail.messageStyle;
    this.querySelector("#myModal").style.display = "block";
  }

  _acept() {}

  _cancel() {
    this.message = "";
    this.messageTitle = "";
    this.messageStyle = "";
    this.messageCode = "";
    this.messageStatus = "";
    this.renderRoot.querySelector("#myModal").style.display = "none";
  }

  render() {
    return html`
      <style>
        .parpadea {
          animation-name: parpadeo;
          animation-duration: 3s;
          animation-timing-function: linear;
          animation-iteration-count: infinite;

          -webkit-animation-name: parpadeo;
          -webkit-animation-duration: 3s;
          -webkit-animation-timing-function: linear;
          -webkit-animation-iteration-count: infinite;
        }

        @-moz-keyframes parpadeo {
          0% {
            opacity: 1;
          }
          50% {
            opacity: 0;
          }
          100% {
            opacity: 1;
          }
        }

        @-webkit-keyframes parpadeo {
          0% {
            opacity: 1;
          }
          50% {
            opacity: 0;
          }
          100% {
            opacity: 1;
          }
        }

        @keyframes parpadeo {
          0% {
            opacity: 1;
          }
          50% {
            opacity: 0;
          }
          100% {
            opacity: 1;
          }
        }
      </style>
       <link rel="stylesheet" href="./src/components/style-alert.css" />
       <div id="myModal"  class="popup">
          <div class="modal-dialog modal-sm">
      <div id="cmpAlert" class="${this.messageStyle} ">
        <strong>${this.messageStatus}: </strong> ${this.message}

        <button
          id="idBtnCerrarAlert"
          type="button"
          class="close"
          @click=${this._cancel}
        >
          &times;
        </button>
      </div>
      </div>
      </div>
    `;
  }
}

customElements.define("cmp-alert-modal", CmpAlertModal);
