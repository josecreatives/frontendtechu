import { LitElement, css, html } from "lit-element";

class CmpAppUser extends LitElement {
  createRenderRoot() {
    return this;
  }

  static get properties() {
    return {
      userName: { type: String },
      userEmail: { type: String },
      userId: { type: Number },
      userLogged: { type: Object },
      profile: { type: Object },
    };
  }

  constructor() {
    super();
    this.userName = "";
    this.userEmail = "";
    this.userId = null;
    this.profile = {};
    this.userLogged = { profile: this.profile };
    this.addEventListener("send-user", ({ detail }) =>
      this._getAccountsUser(detail)
    );
  }

  _getAccountsUser(detail) {
    this.userLogged = detail.user;
    console.log(JSON.stringify(this.userLogged));
  }

  render() {
    return html`
      <div class="card">
        <div class="card-body text-light bg-bbva">
          <div class="form-row">
            <div class="form-group col-md-3">
              <h1 class="card-title">
                <b>BBVA TechU</b>
              </h1>
            </div>
            <div class="form-group col-md-3"></div>
            <div class="form-group col-md-3"></div>
            <div class="form-group col-md-3 text-right">
              <h5>
                <ul style="list-style-type: none" class="text-uppercase font-weight-light">
                <li>
                ${
                  this.userLogged.first_name +
                  " " +
                  this.userLogged.last_name+
                  " - " +
                  
                  this.userLogged.id_client }
                </li>
                <li class="text-bbva-subtitle text-uppercase font-weight-light">
             
              ${this.userLogged.email} 
              </li>
              </ul>
              </h5>
            </div>
          </div>
        </div>
      </div>
    `;
  }
}

customElements.define("cmp-app-user", CmpAppUser);
