import { LitElement, css, html } from "lit-element";

class CmpConfirmModal extends LitElement {
  createRenderRoot() {
    return this;
  }

  static get properties() {
    return {
      idObject: { type: Number },
      message: { type: String },
      messageTitle: { type: String },
    };
  }

  constructor() {
    super();
    this.idObject = null;
    this.mesage = "";
    this.messageTitle = "";
    this.addEventListener("load-id-confirm", ({ detail }) =>
      this._load(detail)
    );
  }

  _load(detail) {
    this.idObject = detail.id;
    this.message = detail.mesage;
    this.messageTitle = detail.messageTitle;
  }

  _acept() {
    console.log("id seleccionado", this.idObject);
    let event = new CustomEvent("acept-confirm", {
      detail: this.idObject,
    });
    this.dispatchEvent(event);
    this.renderRoot.querySelector("#myModal").style.display = "none";
  }

  _cancel() {
    this.idObject = null;
    this.renderRoot.querySelector("#myModal").style.display = "none";
  }

  render() {
    return html`
      <link rel="stylesheet" href="./src/components/style-confirm.css" />
      
      <!-- Modal HTML -->
     
        <div id="myModal"  class="popup">
          <div class="modal-dialog modal-sm">
          <div class="card">
            <div class="card-header">
            <svg class="bi text-danger" width="22" height="22" fill="currentColor">
                <use
                  xlink:href="./resource/bootstrap-icons.svg#x-circle-fill"
                />
              </svg>
              
              &nbsp;${this.messageTitle}
             
            </div>
            <div class="modal-body">
              <p>${this.message}</p>
            </div>
            <div class="modal-footer justify-content-center">
              <button
                type="button"
                class="btn btn-secondary"
                data-dismiss="modal"
                @click="${this._cancel}"
              >
                Cancelar
                <svg class="bi" width="24" height="24" fill="currentColor">
                      <use
                        xlink:href="./resource/bootstrap-icons.svg#x"
                      />
                    </svg>
              </button>
              <button
                type="button"
                class="btn btn-danger"
                @click="${this._acept}"
              >
                Aceptar
                <svg class="bi" width="24" height="24" fill="currentColor">
                      <use
                        xlink:href="./resource/bootstrap-icons.svg#check"
                      />
                    </svg>
              </button>
            </div>
          </div>
        </div>
        </div>
    `;
  }
}

customElements.define("cmp-confirm-modal", CmpConfirmModal);
