import { LitElement, css, html } from "lit-element";
import "./cmp-user.js";
import "./cmp-list.js";
import "./cmp-confirm-modal.js";

import { userDm } from "../dm/user-dm.js";
import { numberUtil } from "../utils/number-util.js";

class CmpListUser extends LitElement {
  createRenderRoot() {
    return this;
  }

  static get properties() {
    return {
      headers: { type: Array },
      fields: { type: Array },
      actions: { type: Array },
      items: { type: Array },
      idField: { type: String },
      title: { type: String },
      messages: { type: Array },
      listCountrySearch: { type: Array },
      listCitySearch: { type: Array },
      listProfileSearch: { type: Array },
      listDocumentTypeSearch: { type: Array },
      startDateSearch: { type: String },
      endDateSearch: { type: String },

      firstNameSearch: { type: String },
      lastNameSearch: { type: String },
      documentNumberSearch: { type: String },
      emailSearch: { type: String },
      idClienteSearch: { type: String },
      creationDateInit: { type: String },
      creationDateEnd: { type: String },
    };
  }

  get userDm() {
    return userDm();
  }

  get numberUtil() {
    return numberUtil();
  }

  

  constructor() {
    super();
    this.firstNameSearch = "";
    this.lastNameSearch = "";
    this.documentNumberSearch = "";
    this.emailSearch = "";
    this.idClienteSearch = "";
    this.creationDateInit = this.numberUtil._getPreMonthDate();
    this.creationDateEnd = this.numberUtil._getTodayDate();

    this.headers = [
      "Id",
      "Cod. Cliente",
      "Tipo documento",
      "Número documento",
      "Nombres",
      "Apellidos",
      "Email",
      "Perfil",
      "Cod. Empleado",
      "Pais",
      "Ciudad",
      "Fecha de creación",
    ];
    this.fields = [
      "id",
      "id_client",
      "document_type",
      "document_number",
      "first_name",
      "last_name",
      "email",
      "profile.code",
      "employee.code",
      "country",
      "city",
      "creation_date",
      "Acciones",
    ];
    this.actions = [
      {
        id: "update",
        name: "Actualizar",
        icon: "pencil",
        function: "_updateRow",
        style: "btn btn-primary",
      },
      {
        id: "delete",
        name: "Eliminar",
        icon: "trash",
        function: "_deleteRow",
        style: "btn btn-danger",
      },
    ];
    this.idField = "id";
    this.items = [];
    this.title = "Usuarios";
    this.listCountrySearch = [];
    this.listCitySearch = [];
    this.listProfileSearch = [];
    this.listDocumentTypeSearch = [];
    this._searchInit();
    
    this.addEventListener("load-parameters", ({ detail }) => this._loadParameters(detail));
    
    //this._loadParameters();
  }

 

  _loadParameters(detail) {
    
    this.listCountrySearch = JSON.parse(
      sessionStorage.getItem("parametersCountry")
    );
    // this.listCitySearch = JSON.parse(sessionStorage.getItem('parametersCity'));
    this.listProfileSearch = JSON.parse(
      sessionStorage.getItem("parametersProfiles")
    );
    this.listDocumentTypeSearch = JSON.parse(
      sessionStorage.getItem("parametersDocumentType")
    );
   

    let cmpUser = this.querySelector("cmp-user");
      const eventLoadUserParameters = new CustomEvent("load-parameters", {
        detail: {},
        bubbles: true,
        composed: true,
      });
      cmpUser.dispatchEvent(eventLoadUserParameters);
  }

  _getLoadCity(e) {
    this.listCitySearch = [];
    let idCountry = e.target.selectedOptions[0].value;
    let listCity = JSON.parse(sessionStorage.getItem("parametersCity"));
    for (let i = 0; i < listCity.length; i++) {
      if (listCity[i].parameter_parent_code == idCountry) {
        this.listCitySearch.push(listCity[i]);
      }
    }

    
    
  }

  async _searchInit() {
    await this.updateComplete;
    this._search();
    /* let listUser = document.querySelector("cmp-list-user");
    let params = {};
    let response = await this.userDm.getUsers(params);
    let responseCount = response.objects.length;
    params["rowsPerPage"] = 5;
    params["skippingRows"] = 0;
    console.log("parametros: ", params);
    let responseParams = await this.userDm.getUsers(params);
    const loadDataUser = new CustomEvent("load-data-user", {
      detail: responseParams.objects,
      bubbles: true,
      composed: true,
    });
    listUser.dispatchEvent(loadDataUser);
    const cmpList = document.querySelector("cmp-list");
    const eventReloadPaginator = new CustomEvent("reload-paginator", {
      detail: { rowsForPage: 5, totalRows: responseCount },
      bubbles: true,
      composed: true,
    });
    cmpList.dispatchEvent(eventReloadPaginator);
    const cmpLoading = this.querySelector("cmp-loading");
    const eventLoadingEnd = new CustomEvent("end-loading", {
      detail: {},
      bubbles: true,
      composed: true,
    });
    cmpLoading.dispatchEvent(eventLoadingEnd);*/
  }

  async _getUserAction({ item, action }) {
    console.log("item", item);
    console.log("action", action);
    if (action.id == "update") {
      this._getUserById(item);
    } else if (action.id == "delete") {
      this._showDialogDelete(item, action);
    } else if (action.id == "detail") {
    }
  }

  _showDialogDelete(item, action) {
    let cmpCompModal = this.querySelector("cmp-confirm-modal");
    cmpCompModal.querySelector("#myModal").style.display = "block";
    const eventLoadIdObject = new CustomEvent("load-id-confirm", {
      detail: {
        id: item.id,
        messageTitle: "¿Esta seguro?",
        mesage:
          "¿Esta seguro de ejecutar la accion?, el cambio no se puede revertir",
      },
      bubbles: true,
      composed: true,
    });
    cmpCompModal.dispatchEvent(eventLoadIdObject);
  }

  async _getUserById(item) {
    this._initLoading();

    let response = await this.userDm.getUserById(item.id);

    if (response.objects.length == 1) {
      this.messages = [];
      console.log("user: ", response.objects[0]);
      let cmpUser = this.querySelector("cmp-user");
      const eventLoadUser = new CustomEvent("load-user", {
        detail: response.objects[0],
        bubbles: true,
        composed: true,
      });
      cmpUser.dispatchEvent(eventLoadUser);
      this.querySelector("cmp-user").querySelector("#idDialogUser").style.display = 'block';
    } else {
      this.errors = [];
      this.errors.push(
        this._addError(
          "No se pudo obtener el usuario.",
          "Danger! ",
          "alert alert-danger"
        )
      );
    }
    this._endLoading();
  }

  _addError(message, type, styleClass) {
    return {
      message: message,
      typeMessage: type,
      typeClass: styleClass,
    };
  }

  _initLoading() {
    const cmpLoading = this.parentElement.parentElement.parentElement.querySelector("cmp-loading");
    if(cmpLoading!=null){
    const eventLoadingInit = new CustomEvent("init-loading", {
      detail: {},
      bubbles: true,
      composed: true,
    });
    cmpLoading.dispatchEvent(eventLoadingInit);
  }
  }

  _endLoading() {
    
    const cmpLoading = this.parentElement.parentElement.parentElement.querySelector("cmp-loading");
    if(cmpLoading!=null){
    const eventLoadingEnd = new CustomEvent("end-loading", {
      detail: {},
      bubbles: true,
      composed: true,
    });
    cmpLoading.dispatchEvent(eventLoadingEnd);
  }
  }

  async _search() {
    this._initLoading();
    let detail = {};
    detail["numRows"] = this.querySelector("cmp-list").querySelector(
      "#idListPaginator"
    ).selectedOptions[0].value;
    detail["page"] = 1;

    this._searchPaginator(detail);
  }

  _getParamsSearch() {
    let paramsSearch = {
      document_number: this.querySelector("#document_number_search").value,
      document_type: this.querySelector("#document_type_search")
        .selectedOptions[0].value,
      first_name: this.querySelector("#first_name_search").value,
      last_name: this.querySelector("#last_name_search").value,
      creation_date_init: this.querySelector("#creation_date_init").value,
      creation_date_end: this.querySelector("#creation_date_end").value,
      country: this.querySelector("#country_search").selectedOptions[0].value,
      city: this.querySelector("#city_search").selectedOptions[0].value,
      id_client: this.querySelector("#id_client_search").value,
      email: this.querySelector("#email_search").value,
    };
    console.log("paramsSearch", JSON.stringify(paramsSearch));
    return paramsSearch;
  }
  async _searchPaginator(detail) {
    let params = this._getParamsSearch();
    this._getParamsSearch();
    let dataSize = await this.userDm.getUsers(params);

    params.rowsPerPage = parseInt(detail.numRows);
    params.skippingRows = parseInt((detail.page - 1) * detail.numRows);

    console.log("parametros", params);

    let data = await this.userDm.getUsers(params);
    console.log("data: ", data);
    this.items = data.objects;

    this._reloadCmpList(params.rowsPerPage, dataSize.objects.length);
    this._endLoading();
  }

  _reloadCmpList(rowsForPage, dataSize) {
    const cmpList = this.querySelector("cmp-list");
    const eventReloadPaginator = new CustomEvent("reload-paginator", {
      detail: { rowsForPage: rowsForPage, totalRows: dataSize },
      bubbles: true,
      composed: true,
    });
    cmpList.dispatchEvent(eventReloadPaginator);
  }

  _openNewUser() {
    console.log(this);
    this.parentElement
      .querySelector("cmp-user")
      .querySelector("#idDialogUser").style.display = "block";
    //this.parentElement.querySelector('cmp-user').querySelector('#idDialogUser').classList.remove("fade");
  }

  async _deleteUser(id) {
   this._initLoading();
    console.log("_deleteUser: id", id);
    let mesage;
    let response;
    response = await this.userDm.deleteUser(id);
    mesage = "Registro eliminado correctamente.";
    console.log("response: ", response);
    if (response.status == "204") {
      this.messages = [];

      this.messages.push({
        message: mesage,
        typeMessage: "¡Success! ",
        typeClass: "alert alert-success",
      });

      this._search();
    } else {
      this.messages = [];
      this.messages.push({
        message: "No se pudo eliminar el registro.",
        typeMessage: "Danger! ",
        typeClass: "alert alert-danger",
      });
    }

    let cmpAlert = this.parentElement.querySelector("cmp-alert-modal");
    const eventAlert = new CustomEvent("load-alert-component", {
      detail: {
        messageStatus: this.messages[0].typeMessage,
        message: this.messages[0].message,
        messageStyle: this.messages[0].typeClass,
      },
      bubbles: true,
      composed: true,
    });
    cmpAlert.dispatchEvent(eventAlert);

    this._endLoading();
  }

  _clean() {
    this.querySelector("#document_number_search").value = "";
    this.querySelector("#first_name_search").value = "";
    this.querySelector("#last_name_search").value = "";
    this.querySelector("#creation_date_init").value = "";
    this.querySelector("#creation_date_end").value = "";
    this.querySelector("#id_client_search").value = "";
    this.querySelector("#email_search").value = "";
    this.listCitySearch = [];
    this.querySelector("#document_type_search").value = "";
    this.querySelector("#country_search").value = "";
    this.querySelector("#city_search").value = "";
    this.querySelector("#profile_search").value = "";
  }

  render() {
    return html`
      <cmp-confirm-modal
        @acept-confirm="${({ detail }) => this._deleteUser(detail)}"
      ></cmp-confirm-modal>

      <cmp-user @reload-table-user="${this._search}"></cmp-user>
      <div class="card">
      <div class="card-header text-dark bg-light text-uppercase font-weight-light">
          <h5 class="card-title">
            <svg class="bi" width="24" height="24" fill="currentColor">
              <use
                xlink:href="./resource/bootstrap-icons.svg#person-circle"
              />
            </svg>

            usuarios
          </h5>
        </div>
        </div>
        <br>
  </div>
      <div class="card">
      <div class="card-header text-dark bg-light text-uppercase font-weight-light">
          <h5 class="card-title">
            <svg class="bi" width="24" height="24" fill="currentColor">
              <use
                xlink:href="./resource/bootstrap-icons.svg#funnel-fill"
              />
            </svg>

            Filtros de busqueda
          </h5>
        </div>
        
        <div class="card-body ">
          <form>
            <div class="form-row">
              <div class="form-group col-md-2">
                <label for="document_type_search">Tipo de documento</label>

                <select
                  name="document_type"
                  class="custom-select mr-sm-2"
                  id="document_type_search"
                  name="document_type_search"
                >
                  <option value="">--Seleccione uno--</option>
                  ${this.listDocumentTypeSearch && this.listDocumentTypeSearch.map(
                    (item) =>
                      html`<option .value="${item.parameter_value}">
                        ${item.parameter_name}
                      </option>`
                  )}
                </select>
              </div>
              <div class="form-group col-md-2">
                <label for="document_number_search">Nº de documento</label>
                <input
                  type="text"
                  class="form-control inputForm"
                  id="document_number_search"
                  name="document_number_search"
                  .value="${this.documentNumberSearch}"
                />
              </div>
              <div class="form-group col-md-4">
                <label for="first_name_search">Nombres</label>
                <input
                  type="text"
                  class="form-control inputForm"
                  id="first_name_search"
                  name="first_name_search"
                  .value="${this.firstNameSearch}"
                />
              </div>

              <div class="form-group col-md-4">
                <label for="last_name_search">Apellidos</label>
                <input
                  type="text"
                  class="form-control inputForm"
                  id="last_name_search"
                  name="last_name_search"
                  .value="${this.lastNameSearch}"
                />
              </div>
            </div>
            <div class="form-row">
              <div class="form-group col-md-2">
                <label for="country_search">Pais</label>
                <select
                  name="country_search"
                  class="custom-select mr-sm-2"
                  id="country_search"
                  @input=${this._getLoadCity}
                >
                  <option value="">--Seleccione uno--</option>
                  ${ this.listCountrySearch && this.listCountrySearch.map(
                    (item) =>
                      html`<option .value="${item.parameter_value}">
                        ${item.parameter_name}
                      </option>`
                  )}
                </select>
              </div>

              <div class="form-group col-md-2">
                <label for="city_search">Ciudad</label>
                <select
                  name="city_search"
                  class="custom-select mr-sm-2"
                  id="city_search"
                >
                  <option value="">--Seleccione uno--</option>
                  ${this.listCitySearch && this.listCitySearch.map(
                    (item) =>
                      html`<option .value="${item.parameter_value}">
                        ${item.parameter_name}
                      </option>`
                  )}
                </select>
              </div>
              <div class="form-group col-md-4">
                <label for="email_search">Email</label>
                <input
                  type="text"
                  class="form-control inputForm"
                  id="email_search"
                  name="email_search"
                  .value="${this.emailSearch}"
                />
              </div>
              <div class="form-group col-md-2">
                <label for="profile_search">Perfil</label>
                <select
                  name="profile_search"
                  class="custom-select mr-sm-2"
                  id="profile_search"
                >
                  <option value="">--Seleccione uno--</option>
                  ${this.listProfileSearch && this.listProfileSearch.map(
                    (item) =>
                      html`<option .value="${item.parameter_value}">
                        ${item.parameter_name}
                      </option>`
                  )}
                </select>
              </div>
              <div class="form-group col-md-2">
                <label for="id_client_search">Cod. Cliente</label>
                <input
                  type="text"
                  class="form-control inputForm"
                  id="id_client_search"
                  name="id_client_search"
                  .value="${this.idClienteSearch}"
                />
              </div>
            </div>
            <div class="form-row">
              <div class="form-group col-md-2">
                <label for="creation_date_init">Fecha creación inicial</label>
                <input
                  type="date"
                  id="creation_date_init"
                  name="creation_date_init"
                  class="form-control inputForm"
                  min="2015-01-01"
                  max="2022-01-01"
                  .value="${this.creationDateInit}"
                />
              </div>
              <div class="form-group col-md-2">
                <label for="creation_date_end">Fecha creación final</label>
                <input
                  type="date"
                  id="creation_date_end"
                  name="creation_date_end"
                  class="form-control inputForm"
                  min="2015-01-01"
                  max="2022-01-01"
                  placeholder="yyyy-mm-dd"
                  .value="${this.creationDateEnd}"
                />
              </div>
            </div>
          </form>
        </div>
        <div class="card-footer text-right">
       
            <button type="button" class="btn btn-bbva btn-lg" @click="${this._search}">
              <svg class="bi" width="24" height="24" fill="currentColor">
                <use
                  xlink:href="./resource/bootstrap-icons.svg#search"
                />
              </svg>

              Buscar
            </button>
            <button type="button" class="btn btn-bbva btn-lg" @click="${this._clean}">
              <svg class="bi" width="24" height="24" fill="currentColor">
                <use
                  xlink:href="./resource/bootstrap-icons.svg#trash-fill"
                />
              </svg>
              Limpiar
            </button>
            <!-- <button
              type="button"
              class="btn btn-dark"
              @click=${this._openNewUser}
            >
              <svg class="bi" width="24" height="24" fill="currentColor">
                <use
                  xlink:href="./resource/bootstrap-icons.svg#person-plus-fill"
                />
              </svg>
              Nuevo
            </button>-->
         
        </div>
      </div>
      <br>
      <div class="card">
      <div class="card-header text-dark bg-light text-uppercase font-weight-light">
          <h5 class="card-title">
            <svg class="bi" width="24" height="24" fill="currentColor">
              <use
                xlink:href="./resource/bootstrap-icons.svg#list"
              />
            </svg>

            Lista de resultados
          </h5>
        </div>
        
        <div class="card-body">
          <cmp-list
            title="${this.title}"
            idField="${this.idField}"
            .headers="${this.headers}"
            .fields="${this.fields}"
            .data="${this.items}"
            .actions="${this.actions}"
            @send-event-paginator="${({ detail }) =>
              this._searchPaginator(detail)}"
            @send-evemt-object-action-selected="${({ detail }) =>
              this._getUserAction(detail)}"
          >
          </cmp-list>
        </div>
      </div>
    `;
  }
}

customElements.define("cmp-list-user", CmpListUser);
