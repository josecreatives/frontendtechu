import { LitElement, css, html } from "lit-element";

class CmpList extends LitElement {
  createRenderRoot() {
    return this;
  }

  static get properties() {
    return {
      idField: { type: String },
      title: { type: String },
      headers: { type: Array },
      fields: { type: Array },
      actions: { type: Array },
      data: { type: Array },
      urlIcon: { type: String },
      dataSize: { type: Number },
      pagesSize: { type: Number },
      page: { type: Number },
      numberOfRowsForPage: { type: Number },
      listPages: { type: Array },
      selectedPage: { type: Number },
      firstPage: { type: Number },
      lastPage: { type: Number },
      dataSizeDisable: { type: Boolean },
    };
  }

  constructor() {
    super();

    /*Init properties*/
    this.idField = "";
    this.title = "";
    this.headers = [];
    this.fields = [];
    this.data = [];
    this.dataSize = 0;
    this.numberOfRowsForPage = 5;
    this.pagesSize = 1;
    this.listPages = [1];
    this.selectedPage = 1;
    this.firstPage = 1;
    this.lastPage = 1;
    this.dataSizeDisable = true;

    /*Listener*/
    this.addEventListener("reload-paginator", ({ detail }) =>
      this._generatePaginator(detail)
    );
  }

  _generatePaginator(detail) {
    this.dataSize = detail.totalRows;
    this.numberOfRowsForPage = detail.rowsForPage;
    this.pagesSize =
      Math.floor(this.dataSize / this.numberOfRowsForPage) >= 1
        ? this.dataSize % this.numberOfRowsForPage == 0
          ? Math.floor(this.dataSize / this.numberOfRowsForPage)
          : Math.floor(this.dataSize / this.numberOfRowsForPage) + 1
        : 1;
    this.lastPage = this.pagesSize;
    this.listPages = [];
    for (let i = 1; i <= this.pagesSize; i++) {
      this.listPages.push(i);
    }
    console.log(
      "_generatePaginator",
      "this.dataSize: ",
      this.dataSize,
      "this.numberOfRowsForPage: ",
      this.numberOfRowsForPage,
      "this.pagesSize: ",
      this.pagesSize,
      "this.listPages: ",
      this.listPages
    );
  }

  _getNumberOfRowsPerPage(e) {
    let numOfRows = e.target.selectedOptions[0].value;
    let page = 1;
    this.numOfRows = numOfRows;
    this.selectedPage = page;
    console.log(
      "_getNumberOfRowsPerPage",
      "this.selectedPage: ",
      this.selectedPage,
      "this.numOfRows: ",
      this.numOfRows
    );
    this._sentEventToPaginator(this.selectedPage, this.numOfRows);
  }

  _getNumberPage(e) {
    let numOfRows = parseInt(
      this.querySelector("#idListPaginator").selectedOptions[0].value
    );
    let page = parseInt(e.target.text);
    this.numOfRows = numOfRows;
    this.selectedPage = page;
    console.log(
      "_getNumberPage",
      "this.selectedPage: ",
      this.selectedPage,
      "this.numOfRows: ",
      this.numOfRows
    );
    this._sentEventToPaginator(this.selectedPage, this.numOfRows);
  }

  _sentEventToPaginator(numberPage, rowsPerPage) {
    console.log(
      "_getDataPaginator",
      "numberPage: ",
      numberPage,
      "rowsPerPage: ",
      rowsPerPage
    );

    let eventPaginator = new CustomEvent("send-event-paginator", {
      detail: { numRows: rowsPerPage, page: numberPage },
      bubbles: true,
      composed: true,
    });
    this.dispatchEvent(eventPaginator);
  }

  _sendEventObjectActionSelected(item, action) {
    console.log(
      "_sendEventObjectActionSelected",
      "Object: ",
      item,
      "Action: ",
      action
    );
    let evenObjectActionSelected = new CustomEvent(
      "send-evemt-object-action-selected",
      {
        detail: { item, action },
      }
    );
    this.dispatchEvent(evenObjectActionSelected);
  }

  getValueFielf(item, subitem) {
    let nivel = subitem.split(".");
    let value;

    if (nivel.length > 1) {
      value = item[nivel[0]];
      for (let i = 1; i < nivel.length; i++) {
        value = value[nivel[i]];
      }
    } else {
      value = item[subitem];
    }

    return value;
  }

  render() {
    return html`
      <style>
        .my-custom-scrollbar {
          position: relative;
          height: 300px;
          overflow: auto;
        }
        .table-wrapper-scroll-y {
          display: block;
        }
      </style>
      <div class="table-wrapper-scroll-y my-custom-scrollbar">
        <table class="table table-striped table-hover table-sm">
          <thead class="thead-dark">
            <tr>
              <th
                colspan="${this.fields &&
                this.actions != undefined &&
                this.actions.length > 0
                  ? this.fields.length + 1
                  : this.fields.length}"
              >
                ${this.title}
              </th>
            </tr>
            <tr>
              ${this.headers &&
              this.headers.map((item) => html`<th scope="col">${item}</th>`)}
              ${this.actions != undefined
                ? html`<th scope="col">Acciones</th>`
                : html``}
            </tr>
          </thead>
          <tbody>
            ${this.data &&
            this.data.map(
              (item) => html` <tr>
                ${this.fields &&
                this.fields.map(
                  (subitem) => html`
                    <td>
                      ${subitem != "Acciones"
                        ? this.getValueFielf(item, subitem)
                        : this.actions &&
                          this.actions.map(
                            (itemAccion) => html`
                              ${itemAccion.icon == "pencil"
                                ? html` <svg
                                    @click="${() =>
                                      this._sendEventObjectActionSelected(
                                        item,
                                        itemAccion
                                      )}"
                                    class="bi"
                                    width="24"
                                    height="24"
                                    fill="currentColor"
                                  >
                                    <use
                                      xlink:href="./resource/bootstrap-icons.svg#pencil-fill"
                                    />
                                  </svg>`
                                : itemAccion.icon == "trash"
                                ? html` <svg
                                    @click="${() =>
                                      this._sendEventObjectActionSelected(
                                        item,
                                        itemAccion
                                      )}"
                                    class="bi"
                                    width="24"
                                    height="24"
                                    fill="currentColor"
                                  >
                                    <use
                                      xlink:href="./resource/bootstrap-icons.svg#trash-fill"
                                    />
                                  </svg>`
                                : itemAccion.icon == "search"
                                ? html` <svg
                                    @click="${() =>
                                      this._sendEventObjectActionSelected(
                                        item,
                                        itemAccion
                                      )}"
                                    class="bi"
                                    width="24"
                                    height="24"
                                    fill="currentColor"
                                  >
                                    <use
                                      xlink:href="./resource/bootstrap-icons.svg#search"
                                    />
                                  </svg>`
                                : html``}
                            `
                          )}
                    </td>
                  `
                )}
              </tr>`
            )}
          </tbody>
        </table>
      </div>
      <br />
      <nav aria-label="Page navigation example">
        <form class="form-inline">
          <div class="form-row">
            <div class="col">
              <ul class="pagination">
                <li class="page-item"><a class="page-link" href="#"><<</a></li>
                ${this.listPages.map(
                  (item) =>
                    html`<li class="page-item">
                      <a class="page-link" @click=${this._getNumberPage}
                        >${item}</a
                      >
                    </li>`
                )}
                <li class="page-item"><a class="page-link" href="#">>></a></li>
              </ul>
            </div>
            <div class="col">
              <select
                class="custom-select mr-sm-2"
                id="idListPaginator"
                @input="${this._getNumberOfRowsPerPage}"
              >
                <option value="5">5</option>
                <option value="10">10</option>
                <option value="15">15</option>
                <option value="20">20</option>
                <option value="25">25</option>
                <option value="30">30</option>
              </select>
            </div>
            <div class="col">
              <input
                type="text"
                class="form-control -sm"
                name="dataSize"
                id="idDataSize"
                .value="Total de ${this.dataSize} registro(s)."
                ?disabled="${this.dataSizeDisable}"
              />
            </div>
          </div>
        </form>
      </nav>
    `;
  }
}

customElements.define("cmp-list", CmpList);
