import { LitElement, css, html } from "lit-element";

class CmpLoading extends LitElement {
  createRenderRoot() {
    return this;
  }

  static get properties() {
    return {};
  }

 
  
 

  constructor() {
    super();
     /*Listener*/
     
     this.addEventListener("init-loading", ( this._init ));
     this.addEventListener("end-loading", ( this._end ));
  }

  

  _init() {
    //this.querySelector(".spinner-border").style.display='block';
    
      
      this.querySelectorAll(".progress-bar-striped").forEach((element) => {
         element.style.display = 'block';
     });
      
    }

  _end() {
    this.querySelectorAll(".progress-bar-striped").forEach((element) => {
      element.style.display = 'none';
      });
     
    
      
  }

  render() {
    return html`
      <div class="progress" id="idLoading" >
      <div class="progress-bar-striped bg-info progress-bar-animated" role="progressbar" style="width: 5%" aria-valuenow="5" aria-valuemin="0" aria-valuemax="200"></div>
      <div class="progress-bar-striped bg-success progress-bar-animated" role="progressbar" style="width: 15%" aria-valuenow="15" aria-valuemin="0" aria-valuemax="100"></div>
      <div class="progress-bar-striped bg-warning progress-bar-animated" role="progressbar" style="width: 30%" aria-valuenow="30" aria-valuemin="0" aria-valuemax="100"></div>
      <div class="progress-bar-striped bg-danger progress-bar-animated" role="progressbar" style="width: 50%" aria-valuenow="50" aria-valuemin="0" aria-valuemax="100"></div>
</div>



      <div id="idLoading2" class="d-flex justify-content-center">
       
        <div style="display:none"
          class="spinner-border -sm m-2 text-primary"
          role="status"
          aria-hidden="true"
        ></div>
        <div style="display:none"
          class="spinner-border -sm m-2 text-secondary"
          role="status"
          aria-hidden="true"
        ></div>
		<div style="display:none"
          class="spinner-border -sm m-2 text-success"
          role="status"
          aria-hidden="true"
        ></div>
		<div style="display:none"
          class="spinner-border -sm m-2 text-danger"
          role="status"
          aria-hidden="true"
        ></div>
		<div style="display:none"
          class="spinner-border -sm m-2 text-warning"
          role="status"
          aria-hidden="true"
        ></div>
      </div>
    `;
  }
}

customElements.define("cmp-loading", CmpLoading);
