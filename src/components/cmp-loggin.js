import {LitElement, css, html} from 'lit-element';
import { until } from 'lit-html/directives/until.js';
import './my-header.js';
const content = fetch('./content.txt').then(r => r.text());
function headerTemplate(title) {
  return html`<header>${title}</header>`;
}
function articleTemplate(text) {
  return html`<article>${text}</article>`;
}
function footerTemplate() {
  return html`<footer>Your footer here.</footer>`;
}

class CmpLoggin extends LitElement {

  static get properties() {
    return {
      message: {type: String},
      myArray: { type: Array },
      myBool: { type: Boolean },
      prop1: {type: String},
      prop2: {type: String},
      prop3: {type: Boolean},
      prop4: {type: String},
      article: {
        attribute: false,
      },
    }
  }

  static get styles() {
    return css`
      div { color: red; }
    `;
  }

  constructor() {
    super();
    this.message = 'Loading';
    this.addEventListener('stuff-loaded', (e) => { this.message = e.detail } );
    this.loadStuff();
    this.myString = 'Hello World';
    this.myArray = ['an','array','of','test','data'];
    this.myBool = true;
    this.prop1 = 'text binding';
    this.prop2 = 'mydiv';
    this.prop3 = true;
    this.prop4 = 'pie';
    

    this.article = {
      title: 'My Nifty Article',
      text: 'Some witty text.',
    };
    
  }

  loadStuff() {
    setInterval(() => {
      let loaded = new CustomEvent('stuff-loaded', {
        detail: 'Loading complete.'
      });
      this.dispatchEvent(loaded);
    }, 3000);
  }

  clickHandler(e) {
    console.log(e.target);
  }

  get _closeButton() {
    return this.shadowRoot.querySelector('#close-button');
  }

  render() {
    return html`
     <div>I'm styled!</div> 
        ${until(content, html`<span>Loading...</span>`)}
        <my-header></my-header>
        ${headerTemplate(this.article.title)}
        ${articleTemplate(this.article.text)}
        ${footerTemplate()}
        <slot name="one">slot 1</slot>
        <slot name="two">slot 2</slot>
      <p>${this.message}</p>
      <p>${this.myString}</p>
      <ul>
        ${this.myArray.map(i => html`<li>${i}</li>`)}
      </ul>
      ${this.myBool?
        html`<p>Render some HTML if myBool is true</p>`:
        html`<p>Render some other HTML if myBool is false</p>`}

         <!-- text binding -->
      <div>${this.prop1}</div>

      <!-- attribute binding -->
      <div id="${this.prop2}">attribute binding</div>

      <!-- boolean attribute binding -->
      <div>
        boolean attribute binding
        <input type="text" ?disabled="${this.prop3}"/>
      </div>

      <!-- property binding -->
      <div>
        property binding
        <input type="text" .value="${this.prop4}"/>
      </div>

      <!-- event handler binding -->
      <div>event handler binding
        <button @click="${this.clickHandler}">click</button>
      </div>
    `;
  }
}

customElements.define('cmp-loggin', CmpLoggin);