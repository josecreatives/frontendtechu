import { LitElement, css, html } from "lit-element";
import "./cmp-list-user.js";
import "./cmp-app-user.js";
import "./cmp-alert-modal.js";
import "./cmp-loading.js";
import "./cmp-user-info.js";
import "./cmp-user-account.js";
import "./cmp-user-transaction.js";
import "./cmp-singin.js";

import { parameterDm } from "../dm/parameter-dm.js";

class CmpMain extends LitElement {
  createRenderRoot() {
    return this;
  }

  static get properties() {
    return {isAdmin: { type: Boolean }};
  }

  get parameterDm() {
    return parameterDm();
  }

  constructor() {
    super();
    this._getParameter();
    this.isAdmin=false;
   
  }

  /**
   *@method:_getPrameter
   *@description:Este metodo sirve para obtener lista de parametrias.
   *@author: Jose E. Rodriguez Paredes.
   *@version: 17.09.2020
   */
  async _getParameter() {
    await this.updateComplete;
    let response = await this.parameterDm._getParameter();
    if (response.status == 200) {
      let parametersDocumentType = [];
      let parametersProfiles = [];
      let parametersCity = [];
      let parametersCountry = [];
      let parametersType = [];
      let parametersAccountType = [];
      let parametersAccountCurrency = [];
      for (let i = 0; i < response.objects.length; i++) {
        if (response.objects[i].parameter_code == "TIPO_DOCUMENTO") {
          parametersDocumentType.push(response.objects[i]);
        } else if (response.objects[i].parameter_code == "PERFILES") {
          parametersProfiles.push(response.objects[i]);
        } else if (response.objects[i].parameter_code == "DEPARTAMENTOS") {
          parametersCity.push(response.objects[i]);
        } else if (response.objects[i].parameter_code == "PAIS") {
          parametersCountry.push(response.objects[i]);
        } else if (response.objects[i].parameter_code == "TIPO") {
          parametersType.push(response.objects[i]);
        } else if (response.objects[i].parameter_code == "TIPO_CUENTA") {
          parametersAccountType.push(response.objects[i]);
        } else if (response.objects[i].parameter_code == "DIVISA") {
          parametersAccountCurrency.push(response.objects[i]);
        }
      }
      sessionStorage.setItem(
        "parametersDocumentType",
        JSON.stringify(parametersDocumentType)
      );
      sessionStorage.setItem(
        "parametersProfiles",
        JSON.stringify(parametersProfiles)
      );
      sessionStorage.setItem("parametersCity", JSON.stringify(parametersCity));
      sessionStorage.setItem(
        "parametersCountry",
        JSON.stringify(parametersCountry)
      );
      sessionStorage.setItem("parametersType", JSON.stringify(parametersType));
      sessionStorage.setItem(
        "parametersAccountType",
        JSON.stringify(parametersAccountType)
      );
      sessionStorage.setItem(
        "parametersAccountCurrency",
        JSON.stringify(parametersAccountCurrency)
      );
    }
    let user = JSON.parse(sessionStorage.getItem("appUser"));
    if(user!=null && user.logged == true){
      console.log(user);
      if(user.profile.code == "ADM"){
        this.isAdmin =true;
      }else{
        this.isAdmin =false;
      }
      this._sendUserToComponent("cmp-app-user");
      this.querySelector("#cmpSingin").style.display = 'none';
      this.querySelector("#allIndex").style.display = 'block';
    }else{
      this._sendTypeDocument("cmp-singin");
      this.querySelector("#cmpSingin").style.display = 'block';
    
    }

    this._endLoading();
   
  }

  _initLoading() {
    const cmpLoading = this.querySelector("cmp-loading");
    const eventLoadingInit = new CustomEvent("init-loading", {
      detail: {},
      bubbles: true,
      composed: true,
    });
    cmpLoading.dispatchEvent(eventLoadingInit);
  }

  _endLoading() {
    const cmpLoading = this.querySelector("cmp-loading");
    const eventLoadingEnd = new CustomEvent("end-loading", {
      detail: {},
      bubbles: true,
      composed: true,
    });
    cmpLoading.dispatchEvent(eventLoadingEnd);
  }

  _sendUserToComponent(component) {
    let user = JSON.parse(sessionStorage.getItem("appUser"));
    const cmp = this.querySelector(component);
    const eventSendUser = new CustomEvent("send-user", {
      detail: { user },
      bubbles: true,
      composed: true,
    });
    cmp.dispatchEvent(eventSendUser);
  }

  _sendParameterToComponent(component) {
    const cmp = this.querySelector(component);
    const eventSendUser = new CustomEvent("load-parameters", {
      detail: {},
      bubbles: true,
      composed: true,
    });
    cmp.dispatchEvent(eventSendUser);
  }

  _sendTypeDocument(component) {
    let listDocumentType = JSON.parse(
      sessionStorage.getItem('parametersDocumentType'));
    const cmp = this.querySelector(component);
    const eventSendUser = new CustomEvent("send-list-type-document", {
      detail: { list:listDocumentType },
      bubbles: true,
      composed: true,
    });
    cmp.dispatchEvent(eventSendUser);
  }

  _activeContentUser(e) {
    //e.target.parentElement.classList.add("active");
    this._inactiveOtherTapContent(e);
    this.querySelector("#idClientAccount").style.display = "none";
    this.querySelector("#idClientInfo").style.display = "none";
    this.querySelector("#idClientTransaction").style.display = "none";
    this.querySelector("#idUserContent").style.display = "block";
    this._sendUserToComponent("cmp-app-user");
    this._sendParameterToComponent("cmp-list-user");
  }
  _activeContenClientInfo(e) {
    this._inactiveOtherTapContent(e);
    this.querySelector("#idClientAccount").style.display = "none";
    this.querySelector("#idUserContent").style.display = "none";
    this.querySelector("#idClientTransaction").style.display = "none";
    this.querySelector("#idClientInfo").style.display = "block";
    this._sendUserToComponent("cmp-user-info");
    this._sendParameterToComponent("cmp-user-info");
  }

  _activeContenClientAccount(e) {
    this._inactiveOtherTapContent(e);
    this.querySelector("#idUserContent").style.display = "none";
    this.querySelector("#idClientInfo").style.display = "none";
    this.querySelector("#idClientTransaction").style.display = "none";
    this.querySelector("#idClientAccount").style.display = "block";
    this._sendUserToComponent("cmp-user-account");
    this._sendParameterToComponent("cmp-user-account");
  }
  _activeContenClientTransaction(e) {
    this._inactiveOtherTapContent(e);
    this.querySelector("#idUserContent").style.display = "none";
    this.querySelector("#idClientInfo").style.display = "none";
    this.querySelector("#idClientAccount").style.display = "none";
    this.querySelector("#idClientTransaction").style.display = "block";
    this._sendUserToComponent("cmp-user-transaction");
  }

  _inactiveOtherTapContent(e) {
    this.querySelectorAll(".tablinks").forEach((element) => {
      try {
        element.className = "tablinks";
      } catch (error) {}
    });
    e.target.parentElement.className += " active";
  }

  _getSinginUser(detail) {
    if ((detail.status == 200)) {
      let appUser = detail.user;
      sessionStorage.setItem("appUser", JSON.stringify(appUser));
      this._sendUserToComponent("cmp-app-user");
      this.querySelector("#allIndex").style.display = 'block';
      this.querySelector("#cmpSingin").style.display = 'none';
      if(appUser.profile.code == "ADM"){
        this.isAdmin =true;
      }else{
        this.isAdmin =false;
      }
    } else {
      let cmpAlert = this.querySelector("#appAlert");
    const eventAlert = new CustomEvent("load-alert-component", {
      detail: {
        messageStatus: "Error ",
        message: "Usuario o password incorrectos",
        messageStyle: 'alert alert-danger',
      },
      bubbles: true,
      composed: true,
    });
    cmpAlert.dispatchEvent(eventAlert);
    }
  }

  render() {
    return html`
      <style>
        /* Style the tab */
        .tab {
          float: left;
          border: 1px solid #ccc;
          background-color: #f1f1f1;
          width: 20%;
          height: 1050px;
        }

        /* Style the buttons inside the tab */
        .tab button {
          display: block;
          background-color: inherit;
          color: black;
          padding: 22px 16px;
          width: 100%;
          border: none;
          outline: none;
          text-align: left;
          cursor: pointer;
          transition: 0.3s;
          font-size: 17px;
        }

        /* Change background color of buttons on hover */
        .tab button:hover {
          background-color: #ddd;
        }

        /* Create an active/current "tab button" class */
        .tab button.active {
          background-color: #ccc;
        }

        /* Style the tab content */
        .tabcontent {
          float: left;
          padding: 0px 12px;
          border: 1px solid #ccc;
          width: 80%;
          border-left: none;
          height: 1050px;
          display: none;
        }
      </style>
       <cmp-singin id="cmpSingin" style="display:none" @send-event-singin-result="${({ detail }) =>
        this._getSinginUser(detail)}" ></cmp-singin>
         <cmp-alert-modal id="appAlert"></cmp-alert-modal>
      <div id="allIndex" style="display:none">
     
      <cmp-app-user id="appUser"></cmp-app-user>
      <cmp-loading></cmp-loading>
     
      
      <div class="tab">
        <button class="tablinks">
          <form class="form-inline">
            <div class="form-group mb-2">
              <svg class="bi " width="36" height="36" fill="currentColor">
                <use
                  xlink:href="./resource/bootstrap-icons.svg#house-fill"
                />
              </svg>
              <span><h1>&nbsp;TechU!</h1></span>
            </div>
          </form>
        </button>
        <button
          class="tablinks" ?disabled="${!this.isAdmin}" ?hidden="${!this.isAdmin}"
          @click="${this._activeContentUser}"
          id="defaultOpen"
        >
          <form class="form-inline">
            <div class="form-group mb-2 text-uppercase font-weight-light">
              &nbsp;&nbsp;&nbsp;&nbsp;
              <svg class="bi " width="24" height="24" fill="currentColor">
                <use
                  xlink:href="./resource/bootstrap-icons.svg#person-circle"
                />
              </svg>
              <span><h5>&nbsp;&nbsp;Usuarios</h5></span>
            </div>
          </form>
        </button>
        <button class="tablinks " @click="${this._activeContenClientInfo}">
          <form class="form-inline"> 
            <div class="form-group mb-2 text-uppercase font-weight-light">
              &nbsp;&nbsp;&nbsp;&nbsp;
              <svg class="bi " width="24" height="24" fill="currentColor">
                <use
                  xlink:href="./resource/bootstrap-icons.svg#person-lines-fill"
                />
              </svg>
              <p ><h5 >&nbsp;&nbsp;Mis Datos</h5></p>
            </div>
          </form>
        </button>
        <button class="tablinks" @click="${this._activeContenClientAccount}">
          <form class="form-inline">
            <div class="form-group mb-2 text-uppercase font-weight-light">
              &nbsp;&nbsp;&nbsp;&nbsp;
              <svg class="bi " width="24" height="24" fill="currentColor">
                <use
                  xlink:href="./resource/bootstrap-icons.svg#credit-card-fill"
                />
              </svg>
              <span><h5>&nbsp;&nbsp;Mis Cuentas</h5></span>
            </div>
          </form>
        </button>
        <button class="tablinks" @click="${
          this._activeContenClientTransaction
        }">
          <form class="form-inline">
            <div class="form-group mb-2 text-uppercase font-weight-light">
              &nbsp;&nbsp;&nbsp;&nbsp;
              <svg class="bi " width="24" height="24" fill="currentColor">
                <use
                  xlink:href="./resource/bootstrap-icons.svg#box-arrow-up-right"
                />
              </svg>
              <span><h5>&nbsp;&nbsp;Transferencias</h5></span>
            </div>
          </form>
        </button>
      </div>

      <div id="idUserContent" class="tabcontent overflow-auto">
        <cmp-alert-modal></cmp-alert-modal>
        
        
        <br />
        <cmp-list-user></cmp-list-user>
      </div>

      <div id="idClientInfo" class="tabcontent overflow-auto">
        <cmp-alert-modal></cmp-alert-modal>
        
       
        <br />
        <cmp-user-info></cmp-user-info>
      </div>

      <div id="idClientAccount" class="tabcontent overflow-auto">
        <cmp-alert-modal></cmp-alert-modal>
        
       
        <br />
        <cmp-user-account></cmp-user-account>
      </div>

       <div id="idClientTransaction" class="tabcontent overflow-auto">
        <cmp-alert-modal></cmp-alert-modal>
        
       
        <br />
        <cmp-user-transaction></cmp-user-transaction>
      </div>
      </div>
    `;
  }
}

customElements.define("cmp-main", CmpMain);
