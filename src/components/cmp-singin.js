import { LitElement, css, html } from "lit-element";
import { userDm } from "../dm/user-dm.js";

class CmpSingin extends LitElement {
  createRenderRoot() {
    return this;
  }
  get userDm() {
    return userDm();
  }

  static get properties() {
    return {
      documentType: { type: String },
      documentNumber: { type: String },
      password: { type: String },
      documentTypeList: { type: Array },
    };
  }

  constructor() {
    super();
    
    this.documentType = "";
    this.documentNumber = "";
    this.password = "";
    this.documentTypeList = [];
    this.addEventListener("send-list-type-document", ({ detail }) =>
      this._loadParameters(detail)
    );
    //this._loadParameters();
  }

 async _loadParameters(detail) {
  
    this.documentTypeList = detail.list;
  
  }

  async _singIn(){
      let document_type = this.querySelector("#document_type").value;
      let document_number = this.querySelector("#document_number").value;
      let password = this.querySelector("#password").value;
      let detail = {
        document_type:document_type,
        document_number:document_number,
        password:password

      }
      let response = await this.userDm.singIn(detail);
      let userLogging = response.json;
      let status = response.status;
      this._sendSingInResult(status,userLogging);
     

  }

  _sendSingInResult(status,user){
    let eventSingInResult = new CustomEvent("send-event-singin-result", {
      detail: { status: status, user: user },
      bubbles: true,
      composed: true,
    });
    this.dispatchEvent(eventSingInResult);
  }
  render() {
    return html`
      <link rel="stylesheet" href="./src/components/style-singin.css" />
      <section class="login-block">
        <div class="container">
          <div class="card">
            <div
              class="card-header text-light bg-bbva text-uppercase font-weight-light text-center"
            >
              <h5 class="card-title">INICIO DE SESIÓN</h5>
            </div>
          </div>
          <div class="card">
            <div class="card-body ">
              <div class="form-row">
                <div class="form-group col-md-12">
                  <label for="document_type">Tipo de documento</label>
                  <select
                   
                    name="document_type"
                    class="custom-select mr-sm-3"
                    id="document_type"
                    name="document_type"
                  >
                    <option value="">--Seleccione uno--</option>
                    ${this.documentTypeList && this.documentTypeList.map(
                      (item) =>
                        html`<option .value="${item.parameter_value}">
                          ${item.parameter_name}
                        </option>`
                    )}
                  </select>
                </div>
              </div>

              <div class="form-row">
                <div class="form-group col-md-12">
                  <label for="document_number">Nº de documento</label>
                  <input
                    type="text"
                    
                    class="form-control inputForm"
                    id="document_number"
                    name="document_number"
                    .value="${this.documentNumber}"
                  />
                </div>
              </div>

              <div class="form-row">
                <div class="form-group col-md-12"></div>
                <label for="password">Password</label>
                <input
                  type="password"
                  
                  class="form-control inputForm"
                  id="password"
                  name="password"
                  .value="${this.password}"
                />
              </div>
            </div>
          </div>
          <div class="card-footer text-center">
            <button
              type="button"
              class="btn btn-bbva btn-lg"
              @click="${this._singIn}"
            >
              ACEPTAR
            </button>
          </div>
        </div>
      </section>
    `;
  }
}

customElements.define("cmp-singin", CmpSingin);
