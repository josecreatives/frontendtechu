import { LitElement, css, html } from "lit-element";
import { userDm } from "../dm/user-dm.js";
import { accountDm } from "../dm/account-dm.js";
import { numberUtil } from "../utils/number-util.js";

class CmpUserAccount extends LitElement {
  createRenderRoot() {
    return this;
  }

  get accountDm() {
    return accountDm();
  }

  get numberUtil() {
    return numberUtil();
  }
  

  static get properties() {
    return {
      user: { type: Object },
      accounts: { type: Array },
      visibleAmounts: { type: Boolean },
      acumulateAvailableBalance: { type: Number },
      acumulateCountableleBalance: { type: Number },
      listAccountCurrency: { type: Array },
      listAccuntType: { type: Array },
      accountName: { type: String },
      accountCurrency: { type: String },
      accountType: { type: String },
      accountAlias: { type: String },
      negativeStyle: { type: String },
      positiveStyle: { type: String },
      errors: { type: Array }
    };
  }

  constructor() {
    super();
    this.user = {};
    this.accounts = [];
    this.visibleAmounts = false;
    this.acumulateAvailableBalance = 0.0;
    this.acumulateCountableleBalance = 0.0;
    this.listAccountCurrency = [];
    this.listAccuntType = [];
    this.accountName = "";
    this.accountCurrency = "";
    this.accountType = "";
    this.accountAlias = "";
    this.negativeStyle = "list-group-item text-danger";
    this.positiveStyle = "list-group-item text-success";
    this.addEventListener("send-user", ({ detail }) =>
      this._getAccountsUser(detail)
    );
    this.errors = [];
    //this._loadParameters();
    this.addEventListener("load-parameters", ({ detail }) => this._loadParameters(detail));
  }

  _loadParameters() {
    this.listAccountCurrency = JSON.parse(
      sessionStorage.getItem("parametersAccountCurrency")
    );
    this.listAccuntType = JSON.parse(
      sessionStorage.getItem("parametersAccountType")
    );
  }

  async _getAccountsUser(detail) {
    this._initLoading();
    await this.updateComplete;
    this.user = detail.user;
    let response = await this.accountDm.getAccountByUserId(this.user.id);
    this.accounts = response.objects;
    console.log("this.accounts", this.accounts);
    this.visibleAmounts = detail.user.visible_amounts;

    this.acumulateAvailableBalance = 0.0;
    this.acumulateCountableleBalance = 0.0;
    for (let i = 0; i < this.accounts.length; i++) {
      try {
        
     
        this.acumulateAvailableBalance += this.accounts[i].available_balance;
        this.acumulateCountableleBalance += this.accounts[i].countable_balance;
        let idElement = "[id='"+this.myTrim(this.accounts[i].account_number.replace(/\s/g, "X"))+"']";
         
          this.myTrim(this.accounts[i].account_number.replace(/\s/g, "X"));
        let element = this.querySelector(idElement);
        element.style.display = "none";
      } catch (error) {
        
      }
      this._endLoading();
    }
    this.acumulateAvailableBalance = (
      Math.round(this.acumulateAvailableBalance * 100) / 100
    ).toFixed(2);
    this.acumulateCountableleBalance = (
      Math.round(this.acumulateCountableleBalance * 100) / 100
    ).toFixed(2);
  }

  _openNewAccount() {
    this._cleanNewAccount();
    this.querySelector("#new_account_form").style.display = "block";
  }
  _refreshAccount() {
    
    let detail = { user: this.user };
    this._getAccountsUser(detail);
  }
  _cleanNewAccount() {
    this.accountName = "";
    this.accountCurrency = "";
    this.accountType = "";
    this.accountAlias = "";

    this.querySelector("#account_alias").value = "";
    this.querySelector("#account_name").value = "";
    this.querySelector("#account_currency").value = "";
    this.querySelector("#account_type").value = "";
  }

  _cancelCreateAccount() {
    this._cleanNewAccount();
    this.querySelector("#new_account_form").style.display = "none";
  }
  _initLoading() {
    const cmpLoading = this.parentElement.parentElement.parentElement.querySelector("cmp-loading");
    if(cmpLoading!=null){
    const eventLoadingInit = new CustomEvent("init-loading", {
      detail: {},
      bubbles: true,
      composed: true,
    });
    cmpLoading.dispatchEvent(eventLoadingInit);
  }
  }

  _endLoading() {
    
    const cmpLoading = this.parentElement.parentElement.parentElement.querySelector("cmp-loading");
    if(cmpLoading!=null){
    const eventLoadingEnd = new CustomEvent("end-loading", {
      detail: {},
      bubbles: true,
      composed: true,
    });
    cmpLoading.dispatchEvent(eventLoadingEnd);
  }
  }
  async _createAccount() {
    this._initLoading();
    this.accountName = this.querySelector("#account_name").value;
    this.accountCurrency = this.querySelector("#account_currency").value;
    this.accountType = this.querySelector("#account_type").value;
    this.accountAlias = this.querySelector("#account_alias").value;
    let accountObject = this._mapAccount(this.user);

    let response = await this.accountDm.insertAccount(accountObject, this.user.id);
    if (response.status == 201) {
      this._refreshAccount();
      this.querySelector("#new_account_form").style.display = "none";
      this.errors = [];
        this.errors.push({
          message: "El registro se insertó correctamente.",
          typeMessage: "¡Success! ",
          typeClass: "alert alert-success",
        });
    } else {
      console.log("error");
      this.errors = [];
        this.errors.push({
          message: "No se pudo insertar el registro.",
          typeMessage: "Danger! ",
          typeClass: "alert alert-danger",
        });
    }
    let cmpAlert = this.parentElement.querySelector("cmp-alert-modal");
    const eventAlert = new CustomEvent("load-alert-component", {
      detail: {
        messageStatus: this.errors[0].typeMessage,
        message: this.errors[0].message,
        messageStyle: this.errors[0].typeClass,
      },
      bubbles: true,
      composed: true,
    });
    this._endLoading();
    cmpAlert.dispatchEvent(eventAlert);
   
  }

  _generateCC() {
    let cc = "";
    let part1 = this.numberUtil._zfill(
      Math.floor(Math.random() * (10000 - 1) + 1),
      4
    );
    let part2 = this.numberUtil._zfill(
      Math.floor(Math.random() * (10000 - 1) + 1),
      4
    );
    let part3 = this.numberUtil._zfill(
      Math.floor(Math.random() * (10000000000 - 1) + 1),
      10
    );
    console.log("CC", part1 + "-" + part2 + "-" + part3);
    return part1 + "-" + part2 + "-" + part3;
  }
  _generateCCI(cc) {
    let cci = cc.split("-");

    let part1 = cci[0].substr(1, 3);
    let part2 = cci[1].substr(1, 3);
    let part3 = "00" + cci[2];
    let part4 = "88";
    console.log("CC", part1 + "-" + part2 + "-" + part3 + "-" + part4);
    return part1 + "-" + part2 + "-" + part3 + "-" + part4;
  }

  _mapAccount(user) {
    let cc = this._generateCC();
    let cci = this._generateCCI(cc);
    let fc = this.numberUtil._getStringDate();
    let fu = this.numberUtil._getStringDate();
    console.log("fc", fc);
    console.log("fu", fu);
    let account = {
      account_name: this.accountName,
      account_alias: this.accountAlias,
      account_number: cc,
      account_number_cci: cci,
      available_balance: 0.0,
      countable_balance: 0.0,
      id_currency: this.accountCurrency,
      type: this.accountType,
      creation_date: fc,
      update_date: fu,
    };
    console.log("account", account);
    return account;
  }
  _visualizeAmounts() {}
 
  _reloadAccount() {}

  _openAccountDetail(item) {
    console.log(item.id);
    
     
      let idElement = "[id='"+this.myTrim(item.account_number.replace(/\s/g, "X"))+"']";
      let element = this.querySelector(idElement);
      if (element.style.display == "block") {
        element.style.display = "none";
      } else {
        element.style.display = "block";
      }
   
  }

  myTrim(x) {
    return x.replace(/^\s+|\s+$/gm, "").replace(/-/g,"");
  }

  render() {
    return html`
     <div class="card">
      <div class="card-header text-dark bg-light text-uppercase font-weight-light">
          <h5 class="card-title">
            <svg class="bi" width="24" height="24" fill="currentColor">
              <use
                xlink:href="./resource/bootstrap-icons.svg#credit-card-fill"
              />
            </svg>

            MIS CUENTAS
          </h5>
        </div>
        </div>
        <br>
      <div class="card" style="">
        <div
          class="card-header text-dark bg-light text-uppercase font-weight-light"
        >
          <h5 class="card-title">
            <svg class="bi" width="20" height="28" fill="currentColor">
              <use
                xlink:href="./resource/bootstrap-icons.svg#grid"
              />
            </svg>

            Resumen de cuentas
          </h5>
        </div>
        <ul class="list-group list-group-flush">
          <li class="list-group-item">
            <h5>
              Saldo disponible cumulado (S/.):
              <b class="text-success">${this.acumulateAvailableBalance}</b>
            </h5>
          </li>
          <li class="list-group-item">
            <h5>
              Saldo contable acumulado(S/.):
              <b class="text-info">${this.acumulateCountableleBalance}</b>
            </h5>
          </li>
          
        </ul>
        <div class="card-footer text-right">
          <button
            type="button"
            class="btn btn-bbva btn-lg"
            @click="${this._openNewAccount}"
          >
            <svg class="bi" width="24" height="24" fill="currentColor">
              <use
                xlink:href="./resource/bootstrap-icons.svg#plus-square"
              />
            </svg>

            Crear cuenta
          </button>
          <button
            type="button"
            class="btn btn-bbva btn-lg"
            @click="${this._refreshAccount}"
          >
            <svg class="bi" width="24" height="24" fill="currentColor">
              <use
                xlink:href="./resource/bootstrap-icons.svg#arrow-repeat"
              />
            </svg>
            Refrescar
          </button>
          <!-- <button
              type="button"
              class="btn btn-dark"
              @click=${this._openNewUser}
            >
              <svg class="bi" width="24" height="24" fill="currentColor">
                <use
                  xlink:href="./resource/bootstrap-icons.svg#person-plus-fill"
                />
              </svg>
              Nuevo
            </button>-->
        </div>
      </div>
      <br />
      <div class="card" style="display:none" id="new_account_form">
        <div
          class="card-header text-dark bg-light text-uppercase font-weight-light"
        >
          <h4 class="card-title">
            <svg class="bi" width="28" height="28" fill="currentColor">
              <use
                xlink:href="./resource/bootstrap-icons.svg#plus-square"
              />
            </svg>

            Nueva cuenta
          </h4>
        </div>

        <ul class="list-group list-group-flush">
          <li class="list-group-item">
            <div class="form-row">
              <div class="form-group col-md-3">
                <h5><label for="account_name">Nombre de la cuenta:</label></h5>
                <input
                  type="text"
                  id="account_name"
                  name="account_name"
                  class="form-control inputForm"
                  .value="${this.accountName}"
                />
              </div>
              <div class="form-group col-md-3">
                <h5><label for="account_alias">Alias de la cuenta:</label></h5>
                <input
                  type="text"
                  id="account_alias"
                  name="account_alias"
                  class="form-control inputForm"
                  .value="${this.accountAlias}"
                />
              </div>
              <div class="form-group col-md-3">
                <h5><label for="account_type">Tipo de cuenta</label></h5>
                <select
                  name="account_type"
                  class="custom-select mr-sm-2"
                  id="account_type"
                >
                  <option value="">--Seleccione uno--</option>
                  ${this.listAccuntType && this.listAccuntType.map(
                    (item) =>
                      html`<option .value="${item.parameter_value}">
                        ${item.parameter_name}
                      </option>`
                  )}
                </select>
              </div>
              <div class="form-group col-md-3">
                <h5><label for="account_currency">Divisa</label></h5>
                <select
                  name="account_currency"
                  class="custom-select mr-sm-2"
                  id="account_currency"
                >
                  <option value="">--Seleccione uno--</option>
                  ${this.listAccountCurrency && this.listAccountCurrency.map(
                    (item) =>
                      html`<option .value="${item.parameter_value}">
                        ${item.parameter_name}
                      </option>`
                  )}
                </select>
              </div>
            </div>
          </li>
        </ul>

        <div class="card-footer text-right">
          <button
            type="button"
            class="btn btn-bbva btn-lg"
            @click="${this._createAccount}"
          >
            <svg class="bi" width="24" height="24" fill="currentColor">
              <use
                xlink:href="./resource/bootstrap-icons.svg#box-arrow-in-down"
              />
            </svg>

            Guardar
          </button>
          <button
            type="button"
            class="btn btn-bbva btn-lg"
            @click="${this._cleanNewAccount}"
          >
            <svg class="bi" width="24" height="24" fill="currentColor">
              <use
                xlink:href="./resource/bootstrap-icons.svg#trash"
              />
            </svg>
            Limpiar
          </button>
          <button
            type="button"
            class="btn btn-bbva btn-lg"
            @click="${this._cancelCreateAccount}"
          >
            <svg class="bi" width="24" height="24" fill="currentColor">
              <use
                xlink:href="./resource/bootstrap-icons.svg#x-circle"
              />
            </svg>
            Cancelar
          </button>
          <!-- <button
         type="button"
         class="btn btn-dark"
         @click=${this._openNewUser}
       >
         <svg class="bi" width="24" height="24" fill="currentColor">
           <use
             xlink:href="./resource/bootstrap-icons.svg#person-plus-fill"
           />
         </svg>
         Nuevo
       </button>-->
        </div>
      </div>
      <br />
      <div id="accordion">
        ${this.accounts && this.accounts.map(
          (item) =>
            html` <div class="card ">
                <div
                  class="card-header text-dark bg-light text-uppercase font-weight-light"
                  id="headingOne"
                >
                  <div class="form-row ">
                    <div class="form-group col-sm-6">
                      <div class="input-group">
                        <a href="#"
                          class=""
                          @click=${() => this._openAccountDetail(item)}
                          data-toggle="collapse"
                          data-target="#collapseOne"
                          aria-expanded="true"
                          aria-controls="collapseOne"
                        >
                          <h5>
                            <b
                              >${item.account_name +
                              " (" +
                              item.account_alias +
                              ")"}
                            </b>
                          </h5>
                        </a>
                      </div>
                    </div>
                    <div class="form-group col-sm-6">
                      <label
                        ><h5>
                          ${"Divisa: "}<b> ${item.id_currency}</b>
                        </h5></label
                      >
                    </div>
                  </div>
                  <div class="form-row">
                    <div class="form-group col-sm-6">
                      <label>
                        <h5>${"C.C: "}<b> ${item.account_number}</b></h5></label
                      >
                    </div>
                    <div class="form-group col-sm-6">
                      <label
                        ><h5>
                          ${"Saldo disponible: "}<b class="text-success">
                            ${item.available_balance}</b
                          >
                        </h5></label
                      >
                    </div>
                  </div>
                  <div class="form-row">
                    <div class="form-group col-sm-6">
                      <label
                        ><h5>
                          ${"C.C.I: "}<b> ${item.account_number_cci}</b>
                        </h5></label
                      >
                    </div>
                    <div class="form-group col-sm-6">
                      <label
                        ><h5>
                          ${"Saldo contable: "}<b class="text-info">
                            ${item.countable_balance}</b
                          >
                        </h5></label
                      >
                    </div>
                  </div>
                </div>

                <div
                  id="collapseOne"
                  class="collapseDetail"
                  aria-labelledby="headingOne"
                  data-parent="#accordion"
                  style="display:none"
                  id="${this.myTrim(item.account_number.replace(/\s/g, "X"))}"
                >
                  <div class="card-body">
                    ${item.transactions &&
                    item.transactions.map(
                      (subIntem) => html`
                        <div class="card">
                          <div class="card-body text-light bg-secondary">
                           <h5 class="card-title"><p>
                              <svg style="${subIntem.amount<0 ? 'display:none': 'display:inline'}"
                                class="bi"
                                width="24"
                                height="24"
                                fill="currentColor"
                              >
                                <use
                                  xlink:href="./resource/bootstrap-icons.svg#box-arrow-in-down"
                                />
                              </svg>
                              <svg style="${subIntem.amount>0 ? 'display:none': 'display:inline'}"
                                class="bi"
                                width="24"
                                height="24"
                                fill="currentColor"
                              >
                                <use 
                                  xlink:href="./resource/bootstrap-icons.svg#box-arrow-up"
                                />
                              </svg>
                              ${subIntem.reason}</p>
                            </h5>
                            <p class="card-text">S${subIntem.description}</p>
                          </div>
                          <ul class="list-group list-group-flush">
                            <li class="${subIntem.amount<0 ? this.negativeStyle: this.positiveStyle}">
                              Monto: &nbsp; ${subIntem.amount}
                            </li>
                            <li class="list-group-item">
                              Número de operación: &nbsp; ${subIntem.number}
                            </li>
                            <li class="list-group-item">
                              Fecha de operación: &nbsp; ${subIntem.date}
                            </li>
                          </ul>
                        </div>
                      `
                    )}
                  </div>
                </div>
              </div>

              <span> </span>`
        )}
      </div>
    `;
  }
}

customElements.define("cmp-user-account", CmpUserAccount);
