import { LitElement, css, html } from "lit-element";
import { userDm } from "../dm/user-dm.js";
import { accountDm } from "../dm/account-dm.js";
import { numberUtil } from "../utils/number-util.js";

class CmpUserTransaction extends LitElement {
  createRenderRoot() {
    return this;
  }

  get accountDm() {
    return accountDm();
  }

  get numberUtil() {
    return numberUtil();
  }

  static get properties() {
    return {
      user: { type: Object },
      listAccountGet: { type: Array },
      listAccountSet: { type: Array },
      accountSelectedGet: { type: Object },
      accountSelectedSet: { type: Object },
      operatioResult: { type: Object },
      isAccountCheckGet: { type: Boolean },
      isAccountCheckSet: { type: Boolean },
      amountTransaction: { type: Number },
      reasonTransaction: { type: String },
      errors: { type: Array },
    };
  }

  constructor() {
    super();
    this.user = {};
    this.listAccountGet = [];
    this.listAccountSet = [];
    this.accountSelectedGet = {};
    this.accountSelectedSet = {};
    this.isAccountCheckGet = false;
    this.isAccountCheckSet = false;
    this.amountTransaction = 0;
    this.operatioResult = {};
    (this.reasonTransaction = ""),
      this.addEventListener("send-user", ({ detail }) =>
        this._loadUser(detail)
      );
    this._initPage();
    this.errors= [];
  }

  async _initPage() {
    await this.updateComplete;
    this.querySelector("#paso_uno").style.display = "block";
    this.querySelector("#paso_dos").style.display = "none";
    this.querySelector("#paso_tres").style.display = "none";
  }
  _setStepOne() {
    this.amountTransaction = this.querySelector("#amountTransaction").value;
    this.reasonTransaction = this.querySelector("#reasonTransaction").value;
    if (!this._errorOnStepOne()) {
      this.querySelector("#paso_uno").style.display = "none";
      this.querySelector("#paso_dos").style.display = "block";
      this.querySelector("#paso_tres").style.display = "none";
    }else{
      let cmpAlert = this.parentElement.querySelector("cmp-alert-modal");
    const eventAlert = new CustomEvent("load-alert-component", {
      detail: {
        messageStatus: this.errors[0].typeMessage,
        message: this.errors[0].message,
        messageStyle: this.errors[0].typeClass,
      },
      bubbles: true,
      composed: true,
    });
    cmpAlert.dispatchEvent(eventAlert);
    }
  }
  _errorOnStepOne() {
    let amountRegex = /^([0-9]+\.?[0-9]{0,2})$/;
    this.errors = [];
      
    if (this.accountSelectedGet.id == undefined) {
      this.errors.push({
        message: "Debe seleccionar cuenta cargo.",
        typeMessage: "Danger! ",
        typeClass: "alert alert-danger",
      });
      
      return true;
    }
    if (this.accountSelectedSet.id == undefined) {
     
      this.errors.push({
        message: "Debe seleccionar cuenta abono.",
        typeMessage: "Danger! ",
        typeClass: "alert alert-danger",
      });
      return true;
    }
    if (this.amountTransaction == 0) {
    
      this.errors.push({
        message: "Debe ingresar monto.",
        typeMessage: "Danger! ",
        typeClass: "alert alert-danger",
      });
      return true;
    }
    if (this.reasonTransaction == "") {
     
      this.errors.push({
        message: "Debe ingresar una razon.",
        typeMessage: "Danger! ",
        typeClass: "alert alert-danger",
      });
      return true;
    }
  
      if (!amountRegex.test(this.amountTransaction)) {
        
        this.errors.push({
          message: "ingrese un monto de transaccion valido.",
          typeMessage: "Danger! ",
          typeClass: "alert alert-danger",
        });
        return true;
      }else{
        if (this.accountSelectedGet.available_balance < this.amountTransaction) {
           
            this.errors.push({
              message: "No cuenta con saldo suficiente",
              typeMessage: "Danger! ",
              typeClass: "alert alert-danger",
            });
            return true;
          }
      }
    
  }
  _initLoading() {
    const cmpLoading = this.parentElement.parentElement.parentElement.querySelector("cmp-loading");
    if(cmpLoading!=null){
    const eventLoadingInit = new CustomEvent("init-loading", {
      detail: {},
      bubbles: true,
      composed: true,
    });
    cmpLoading.dispatchEvent(eventLoadingInit);
  }
  }

  _endLoading() {
    
    const cmpLoading = this.parentElement.parentElement.parentElement.querySelector("cmp-loading");
    if(cmpLoading!=null){
    const eventLoadingEnd = new CustomEvent("end-loading", {
      detail: {},
      bubbles: true,
      composed: true,
    });
    cmpLoading.dispatchEvent(eventLoadingEnd);
  }
  }
  async _setStepTwo() {
    this._initLoading();
    this.accountSelectedGet.available_balance = parseFloat(this.accountSelectedGet.available_balance) - parseFloat(this.amountTransaction);
    this.accountSelectedGet.countable_balance = parseFloat(this.accountSelectedGet.countable_balance) - parseFloat(this.amountTransaction);
    this.accountSelectedSet.available_balance = parseFloat(this.accountSelectedSet.available_balance)+ parseFloat(this.amountTransaction);
    this.accountSelectedSet.countable_balance = parseFloat(this.accountSelectedSet.countable_balance)+ parseFloat(this.amountTransaction);

    

    let transactionOut = this._getTransactionBody(
      this.accountSelectedGet,
      "retiro"
    );
    let transactionIn = this._getTransactionBody(
      this.accountSelectedGet,
      "abono"
    );
    transactionOut.id_account = this.accountSelectedGet.id;
    transactionIn.id_account = this.accountSelectedSet.id;

    
    //todo actualizar los account
    let responseOut = await this.accountDm.insertTransaction(
      transactionOut,
      this.accountSelectedGet.id
    );
    let responseIn = await this.accountDm.insertTransaction(
      transactionIn,
      this.accountSelectedSet.id
    );
    this.errors = [];
    if (responseOut.status == 201 && responseIn.status == 201) {
        let respUpAccGet = await this.accountDm.updateAccount(this.accountSelectedGet, this.accountSelectedGet.id_user,this.accountSelectedGet.id);
        let respUpAccSet = await this.accountDm.updateAccount(this.accountSelectedSet, this.accountSelectedSet.id_user,this.accountSelectedSet.id);
      this.operatioResult = {
        numberOpe: transactionOut.number,
        ccOutOpe: this.accountSelectedGet.account_number,
        ccInOpe: this.accountSelectedSet.account_number,
        amountOpe: transactionIn.amount,
        dateOpe: transactionOut.date,
        descriptionOpe: transactionOut.reason,
      };

      this.querySelector("#paso_uno").style.display = "none";
      this.querySelector("#paso_dos").style.display = "none";
      this.querySelector("#paso_tres").style.display = "block";
      this.errors.push({
        message: "La transferencia se ejecutó correctamente.",
        typeMessage: "¡Success! ",
        typeClass: "alert alert-success",
      });
    }else{
      this.errors.push({
        message: "La transferencia no se ejecutó correctamente.",
        typeMessage: "¡Success! ",
        typeClass: "alert alert-success",
      });
    }
    this._endLoading();
    let cmpAlert = this.parentElement.querySelector("cmp-alert-modal");
    const eventAlert = new CustomEvent("load-alert-component", {
      detail: {
        messageStatus: this.errors[0].typeMessage,
        message: this.errors[0].message,
        messageStyle: this.errors[0].typeClass,
      },
      bubbles: true,
      composed: true,
    });
    cmpAlert.dispatchEvent(eventAlert);
  }

  _getTransactionBody(account, tipo) {
    let transaction = {
      number: this.numberUtil._zfill(
        Math.floor(Math.random() * (10000 - 1) + 1),
        4
      ),
      date: this.numberUtil._getStringDate(),
      description: this.reasonTransaction,
      reason: "Tranferencia a cuentas propias",
      amount:
        tipo == "retiro"
          ? -Math.abs(this.amountTransaction)
          : parseFloat(this.amountTransaction),
    };
    console.log("transaction", transaction);
    return transaction;
  }

  _setStepThree() {
    this.listAccountGet = [];
    this.listAccountSet = [];
    this.accountSelectedGet = {};
    this.accountSelectedSet = {};
    this.isAccountCheckGet = false;
    this.isAccountCheckSet = false;
    this.amountTransaction = 0;
    this.reasonTransaction ="";
    this.operatioResult = {};
    let detail = {user:this.user};
      this._loadUser(detail);
     
    this.querySelector("#paso_uno").style.display = "block";
    this.querySelector("#paso_dos").style.display = "none";
    this.querySelector("#paso_tres").style.display = "none";
  }

  _backStepOne() {
    this.querySelector("#paso_uno").style.display = "block";
    this.querySelector("#paso_dos").style.display = "none";
    this.querySelector("#paso_tres").style.display = "none";
  }

  async _loadUser(detail) {
    this.user = detail.user;
    let response = await this.accountDm.getAccountByUserId(this.user.id);
    this.listAccountGet = response.objects;
    console.log("this.accounts", this.accounts);
  }

  _getAccountGet(account) {
    this.accountSelectedSet = {};
    this.accountSelectedGet = account;
    console.log("this", account);
    this.listAccountSet = [];
    for (let i = 0; i < this.listAccountGet.length; i++) {
      if (this.listAccountGet[i].account_number != account.account_number) {
        this.listAccountSet.push(this.listAccountGet[i]);
      }
    }
  }
  _getAccountSet(account) {
    this.accountSelectedSet = account;
    console.log("_getAccountSet", account);
  }

  _limpiar() {
    this.accountSelectedSet = {};
    this.accountSelectedGet ={};
    this.listAccountSet = [];
    this.amountTransaction = 0;
    this.querySelector("#amountTransaction").value = 0;
    this.querySelector("#reasonTransaction").value = "";
    this.reasonTransaction ="";
    console.log("_getAccountSet", account);
  }

  render() {
    return html`
    
      <div class="card" >
        <div
          class="card-header text-dark bg-light text-uppercase font-weight-light"
        >
          <h5 class="card-title">
            <svg class="bi" width="24" height="24" fill="currentColor">
              <use
                xlink:href="./resource/bootstrap-icons.svg#box-arrow-up-right"
              />
            </svg>

            TRANSFERENCIAS
          </h5>
        </div>
      </div>
      <br />

      <div class="card" id="paso_uno">
        <div
          class="card-header text-dark bg-light text-uppercase font-weight-light"
        >
          <h5 class="card-title">
            <svg class="bi" width="24" height="24" fill="currentColor">
              <use
                xlink:href="./resource/bootstrap-icons.svg#arrow-down-up"
              />
            </svg>

            PASO 01 - TRANSFERENCIA A CUANTAS PROPIAS
          </h5>
        </div>

        <div class="card-body">
          <div class="form-row">
            <div class="form-group col-md-6">
              <ul class="list-group" id="list_account_get_get">
                <li
                  class="list-group-item disabled list-group-item list-group-item-action bg-bbva text-light"
                >
                  CUENTA CARGO
                </li>

                ${this.listAccountGet.map(
                  (item) => html`
                    
                     
                       <button type="button" href="#" class="list-group-item list-group-item-action"  @click="${() => this._getAccountGet(item)}">
                        ${item.account_name +
                        " (" +
                        item.account_alias +
                        ") - Divisa: " +
                        item.id_currency +
                        ") - C.C.: " +
                        item.account_number + " Saldo disponible: " +  item.available_balance }   </button>
                   
                       
                    
                  `
                )}
                
              </ul>

              <br />
            </div>

            <div class="form-group col-md-6 ">
              <ul class="list-group">
                <li
                  class="list-group-item disabled list-group-item list-group-item-action bg-bbva text-light"
                >
                  CUENTA DE ABONO
                </li>
                ${this.listAccountSet.map(
                  (item) => html`

                      <button type="button" href="#" class="list-group-item list-group-item-action"  @click="${() => this._getAccountSet(item)}">
                        ${item.account_name +
                        " (" +
                        item.account_alias +
                        ") - Divisa: " +
                        item.id_currency +
                        ") - C.C.: " +
                        item.account_number + " Saldo disponible: " +  item.available_balance }    </button>
                   
                  
                  `
                )}
              </ul>
              <br />
            </div>
          </div>


          





<div class="form-row">
            <div class="form-group col-md-6">
              <ul class="list-group" id="list_account_get_get">
                <li
                  class="list-group-item disabled list-group-item list-group-item-action bg-bbva text-light"
                >
                  CUENTA CARGO SELECCIONADA
                </li>

                <li class="list-group-item"> ${this.accountSelectedGet.account_number != undefined ? this.accountSelectedGet.account_name +
                        " (" +
                        this.accountSelectedGet.account_alias +
                        ") - Divisa: " +
                        this.accountSelectedGet.id_currency +
                        ") - C.C.: " +
                        this.accountSelectedGet.account_number : "" } 

                </li>
                <li class="list-group-item">
                ${this.accountSelectedGet.account_number != undefined ? 'Saldo disponible: '+this.accountSelectedGet.available_balance:""} 
                </li> 
                       
                    
                  
                
                
              </ul>

              <br />
            </div>

            <div class="form-group col-md-6 ">
              <ul class="list-group">
                <li
                  class="list-group-item disabled list-group-item list-group-item-action bg-bbva text-light"
                >
                  CUENTA DE ABONO SELECCIONADA
                </li>
                <li class="list-group-item"> ${this.accountSelectedSet.account_number != undefined ? this.accountSelectedSet.account_name +
                        " (" +
                        this.accountSelectedSet.account_alias +
                        ") - Divisa: " +
                        this.accountSelectedSet.id_currency +
                        ") - C.C.: " +
                        this.accountSelectedSet.account_number :""} 

                </li>
                <li class="list-group-item">
                ${this.accountSelectedSet.account_number != undefined ? "Saldo disponible: " + this.accountSelectedSet.available_balance:""} 
                </li>    
                     
                     
                       
                    
               
              </ul>
              <br />
            </div>
          </div>


          <div class="form-row">
            <div class="form-group col-md-12">
              <div class="card">
                <div class="card-body">
                  <ul class="list-group list-group-flush">
                    <li class="list-group-item">
                      Monto:
                      <input
                        type="number"
                        id="amountTransaction"
                        name="amountTransaction"
                        class="form-control inputForm"
                        .value="${this.amountTransaction}"
                     
                      />
                    </li>
                    <li class="list-group-item">
                      Referencia:
                      <input
                        type="text"
                        id="reasonTransaction"
                        name="reasonTransaction"
                        class="form-control inputForm"
                        .value="${this.reasonTransaction}"
                      />
                    </li>
                    <li class="list-group-item"></li>
                  </ul>
                </div>
                <div class="card-foother">
                  <div class="form-group col-md-12 text-right">
                  <button
                      type="button"
                      class="btn btn-bbva btn-lg"
                      @click="${this._limpiar}"
                    >
                      LIMPIAR
                      <svg
                        class="bi"
                        width="24"
                        height="24"
                        fill="currentColor"
                      >
                        <use
                          xlink:href="./resource/bootstrap-icons.svg#trash-fill"
                        />
                      </svg>
                    </button>
                    <button
                      type="button"
                      class="btn btn-bbva btn-lg"
                      @click="${this._setStepOne}"
                    >
                      TRANSFERIR
                      <svg
                        class="bi"
                        width="24"
                        height="24"
                        fill="currentColor"
                      >
                        <use
                          xlink:href="./resource/bootstrap-icons.svg#box-arrow-right"
                        />
                      </svg>
                    </button>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>




 

      <div class="card" id="paso_dos">
        <div
          class="card-header text-dark bg-light text-uppercase font-weight-light"
        >
          <h5 class="card-title">
            <svg class="bi" width="24" height="24" fill="currentColor">
              <use
                xlink:href="./resource/bootstrap-icons.svg#arrow-down-up"
              />
            </svg>

            PASO 02 - TRANSFERENCIAS A CUENTAS PROPIAS
          </h5>
        </div>

        <div class="card-body">
          <div class="form-row">
            <div class="form-group col-md-5">
              <ul class="list-group">
                <li
                  class="list-group-item disabled list-group-item list-group-item-action bg-bbva text-light"
                >
                  CUANTA A CARGO
                </li>
                <li class="list-group-item">
               
                  <p class="font-weight-lighter">${
                    this.accountSelectedGet.account_name +
                    " (" +
                    this.accountSelectedGet.account_alias +
                    ") "
                  }</p>
                
                  <p class="font-weight-lighter">${
                    "C.C: " + this.accountSelectedGet.account_number
                  }</p>
                
                  <p class="font-weight-lighter">${
                    "Divisa: " +
                    this.accountSelectedGet.id_currency +
                    " (-)" +
                    this.amountTransaction
                  }</p>
                
                  </li>
                  </ul>
            </div>

            <div class="form-group col-md-2 text-center">
                    <p>
                
                <svg
                  class="bi"
                  width="24"
                  height="24"
                  fill="currentColor"
                >
                  <use
                    xlink:href="./resource/bootstrap-icons.svg#arrow-right-square"
                  />
                </svg>
                 
                  </p>
                 <p>
                  <svg
                  class="bi"
                  width="24"
                  height="24"
                  fill="currentColor"
                >
                  <use
                    xlink:href="./resource/bootstrap-icons.svg#arrow-right-square"
                  />
                </svg>
                </p>
                 <p>
                  <svg
                  class="bi"
                  width="24"
                  height="24"
                  fill="currentColor"
                >
                  <use
                    xlink:href="./resource/bootstrap-icons.svg#arrow-right-square"
                  />
                </svg>
                </p>
                 <p>
                  <svg
                  class="bi"
                  width="24"
                  height="24"
                  fill="currentColor"
                >
                  <use
                    xlink:href="./resource/bootstrap-icons.svg#arrow-right-square"
                  />
                </svg>
                </p>
                <p>
                  <svg
                  class="bi"
                  width="24"
                  height="24"
                  fill="currentColor"
                >
                  <use
                    xlink:href="./resource/bootstrap-icons.svg#arrow-right-square"
                  />
                </svg>
                </p>

            </div>
            <div class="form-group col-md-5">
              <ul class="list-group">
                <li
                  class="list-group-item disabled list-group-item list-group-item-action bg-bbva text-light"
                >
                  CUENTA DE ABONO
                </li>
                <li class="list-group-item">
               
                  <p class="font-weight-lighter">${
                    this.accountSelectedSet.account_name +
                    " (" +
                    this.accountSelectedSet.account_alias +
                    ") "
                  }</p>
                
                  <p class="font-weight-lighter">${
                    "C.C: " + this.accountSelectedSet.account_number
                  }</p>
                
                  <p class="font-weight-lighter">${
                    "Divisa: " +
                    this.accountSelectedSet.id_currency +
                    " (+)" +
                    this.amountTransaction
                  }</p>
                
                  </li>
              </ul>
              <br />
            </div>
            
          </div>
          <div class="card-foother">
                  <div class="form-group col-md-12 text-right">
                  <button
                type="button"
                class="btn btn-bbva btn-lg"
                @click="${this._backStepOne}"
              >
                ATRAS
                <svg
                  class="bi"
                  width="24"
                  height="24"
                  fill="currentColor"
                >
                  <use
                    xlink:href="./resource/bootstrap-icons.svg#arrow-return-left"
                  />
                </svg>
              </button>
                    <button
                      type="button"
                      class="btn btn-bbva btn-lg"
                      @click="${this._setStepTwo}"
                    >
                      CONFIRMAR
                      <svg
                        class="bi"
                        width="24"
                        height="24"
                        fill="currentColor"
                      >
                        <use
                          xlink:href="./resource/bootstrap-icons.svg#check"
                        />
                      </svg>
                    </button>
                  </div>
                </div>
                <br>
        </div>
      </div>
      

<div class="card" id="paso_tres"> 
  <div
    class="card-header text-dark bg-light text-uppercase font-weight-light"
  >
    <h5 class="card-title">
      <svg class="bi" width="24" height="24" fill="currentColor">
        <use
          xlink:href="./resource/bootstrap-icons.svg#arrow-down-up"
        />
      </svg>

      PASO 03 - TRANSFERENCIAS A CUENTAS PROPIAS
    </h5>
  </div>

  <div class="card-body">
    <div class="form-row">
      <div class="form-group col-md-12">
        
        <ul class="list-group">
          <li
            class="list-group-item disabled list-group-item list-group-item-action bg-bbva text-light"
          >
            CONSTANCIA DE OPERACIÓN
          </li>
                  <li class="list-group-item"><p>N° de operacion: ${
                    this.operatioResult.numberOpe
                  }</p></li>
          <li class="list-group-item">C.C. A carg: ${
            this.operatioResult.ccOutOpe
          }</li>
          <li class="list-group-item">C.C. Abono: ${
            this.operatioResult.ccInOpe
          }</li>
          <li class="list-group-item">Monto: ${this.operatioResult.amountOpe}</li>
          <li class="list-group-item">Fecha: ${this.operatioResult.dateOpe}</li>
          <li class="list-group-item">Motivo: ${this.operatioResult.descriptionOpe}</li>


          
        </ul>
        <br />
      </div>
      
    </div>
    <div class="card-foother">
            <div class="form-group col-md-12 text-right">
            
              <button
                type="button"
                class="btn btn-bbva btn-lg"
                @click="${this._setStepThree}"
              >
                SALIR
                <svg
                  class="bi"
                  width="24"
                  height="24"
                  fill="currentColor"
                >
                  <use
                    xlink:href="./resource/bootstrap-icons.svg#door-closed"
                  />
                </svg>
              </button>
            </div>
          </div>
          <br>
  </div>
</div>
</br>
     
    `;
  }
}

customElements.define("cmp-user-transaction", CmpUserTransaction);
