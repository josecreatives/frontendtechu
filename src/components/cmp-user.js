import { LitElement, css, html } from "lit-element";
import { userDm } from "../dm/user-dm.js";
import { numberUtil } from "../utils/number-util.js";

class CmpUser extends LitElement {
  createRenderRoot() {
    return this;
  }

  get userDm() {
    return userDm();
  }

  get numberUtil() {
    return numberUtil();
  }

  static get properties() {
    return {
      id: { type: String },
      idClient: { type: String },
      firstName: { type: String },
      lastName: { type: String },
      email: { type: String },
      password: { type: String },
      documentType: { type: String },
      documentNumber: { type: String },
      mobilePhone: { type: String },
      address: { type: String },
      phone: { type: String },
      city: { type: String },
      country: { type: String },
      creationDate: { type: String },
      updateDate: { type: String },
      visibleAmounts: { type: Boolean },
      maximumAmountOperation: { type: String },
      type: { type: String },
      profileCode: { type: String },
      profileName: { type: String },
      employeeCode: { type: String },
      employeePosition: { type: String },
      disabledDate: { type: Boolean },

      message: { type: String },
      typeClass: { type: String },
      errors: { type: Array },
      isActiveSave: { type: Boolean },

      listCountry: { type: Array },
      listCity: { type: Array },
      listProfile: { type: Array },
      listDocumentType: { type: Array },
      listType: { type: Array },
    };
  }

  constructor() {
    super();
    this.id = "";
    this.idClient = "";
    this.firstName = "";
    this.lastName = "";
    this.email = "";
    this.password = "";
    this.documentType = "";
    this.documentNumber = "";
    this.mobilePhone = "";
    this.address = "";
    this.phone = "";
    this.city = "";
    this.country = "";
    this.creationDate = "";
    this.updateDate = "";
    this.visibleAmounts = false;
    this.maximumAmountOperation = "0.00";
    this.type = "";
    this.profileCode = "";
    this.profileName = "";
    this.employeeCode = "";
    this.employeePosition = "";
    this.disabledDate = true;

    this.typeMessage = "";
    this.message = "";
    this.typeClass = "";
    this.errors = [];
    this.isActiveSave = false;
    this.addEventListener("load-user", ({ detail }) => this._loadUser(detail));
    this.addEventListener("load-parameters", ({ detail }) => this._loadParameters(detail));

    this.listCountry = [];
    this.listCity = [];
    this.listProfile = [];
    this.listDocumentType = [];
    this.listType = [];
    //this._loadParameters();
  }

  _loadParameters() {
    this.listCountry = JSON.parse(sessionStorage.getItem("parametersCountry"));
    // this.listCitySearch = JSON.parse(sessionStorage.getItem('parametersCity'));
    this.listProfile = JSON.parse(sessionStorage.getItem("parametersProfiles"));
    this.listDocumentType = JSON.parse(
      sessionStorage.getItem("parametersDocumentType")
    );
    this.listDocumentType = JSON.parse(
      sessionStorage.getItem("parametersDocumentType")
    );
    this.listType = JSON.parse(sessionStorage.getItem("parametersType"));

    
  }

  _initLoading() {
    const cmpLoading = this.parentElement.parentElement.parentElement.parentElement.querySelector("cmp-loading");
    if(cmpLoading!=null){
    const eventLoadingInit = new CustomEvent("init-loading", {
      detail: {},
      bubbles: true,
      composed: true,
    });
    cmpLoading.dispatchEvent(eventLoadingInit);
  }
  }

  _endLoading() {
    
    const cmpLoading = this.parentElement.parentElement.parentElement.parentElement.querySelector("cmp-loading");
    if(cmpLoading!=null){
    const eventLoadingEnd = new CustomEvent("end-loading", {
      detail: {},
      bubbles: true,
      composed: true,
    });
    cmpLoading.dispatchEvent(eventLoadingEnd);
  }
  }

  _loadUser(detail) {
    this.id = detail.id;
    this.idClient = detail.id_client;
    this.firstName = detail.first_name;
    this.lastName = detail.last_name;
    this.email = detail.email;
    this.password = detail.password;
    this.documentType = detail.document_type;
    this.documentNumber = detail.document_number;
    this.mobilePhone = detail.mobile_phone;
    this.address = detail.address;
    this.phone = detail.phone;
    this.city = detail.city;
    this.country = detail.country;
    this.creationDate = detail.creation_date;
    this.updateDate = detail.update_date;
    this.visibleAmounts = detail.visible_amounts;
    this.maximumAmountOperation = detail.maximum_amount_operations;
    this.type = detail.type;
    this.profileCode = detail.profile.code;
    this.profileName = detail.profile.name;
    this.employeeCode = detail.employee.code;
    this.employeePosition = detail.employee.position;

    this.isActiveSave = false;
    this._getLoadCity(this.country);
    this.querySelector("#document_type").value = this.documentType;
    this.querySelector("#country").value = this.country;
    this.querySelector("#profile_code").value = this.profileCode;
    this.querySelector("#type").value = this.type;
    this.querySelector("#city").value = this.city;
    this._setCity();
    //this._endLoading();
  }

  async _setCity() {
    await this.updateComplete;
    this.querySelector("#city").value = this.city;
  }
  _getLoadCity(id) {
    this.listCity = [];
    let idCountry = id;
    let listCitySession = JSON.parse(sessionStorage.getItem("parametersCity"));
    for (let i = 0; i < listCitySession.length; i++) {
      if (listCitySession[i].parameter_parent_code == idCountry) {
        this.listCity.push(listCitySession[i]);
      }
    }
  }
  

  _mapperUser() {
    let userObject = {
      id: this.id,
      id_client: this.querySelector("#id_client").value,
      document_type: this.querySelector("#document_type").value,
      document_number: this.querySelector("#document_number").value,
      first_name: this.querySelector("#first_name").value,
      last_name: this.querySelector("#last_name").value,
      country: this.querySelector("#country").value,
      city: this.querySelector("#city").value,
      address: this.querySelector("#address").value,
      mobile_phone: this.querySelector("#mobile_phone").value,
      phone: this.querySelector("#phone").value,
      email: this.querySelector("#email").value,
      type: this.querySelector("#type").value,
      maximum_amount_operations: this.querySelector(
        "#maximum_amount_operations"
      ).value,
      employee: {
        code: this.querySelector("#employee_code").value,
        position: this.querySelector("#employee_position").value,
      },
      profile: {
        code: this.querySelector("#profile_code").value,
      },
      visible_amounts:
        this.querySelector("#visible_amounts").value == "on" ? true : false,
      creation_date: this.querySelector("#creation_date").value,
      update_date: this.numberUtil._getTodayDate()
    };
    console.log("userObject", userObject);
    return userObject;
  }
  _errorOnUpdateUser() {
    let message = [];
    let emailRegex = /^(([^<>()[\]\.,;:\s@\"]+(\.[^<>()[\]\.,;:\s@\"]+)*)|(\".+\"))@(([^<>()[\]\.,;:\s@\"]+\.)+[^<>()[\]\.,;:\s@\"]{2,})$/i;
    let amountRegex = /^([0-9]+\.?[0-9]{0,2})$/;
    let numberRegex = /^\d+$/;
    let user = this._mapperUser();

    if (user.id_client == "") {
      message.push({
        message: "El id de Cliente es requerido.",
        typeMessage: "¡Danger! ",
        typeClass: "alert alert-danger",
      });
    }
    if (user.document_type == "") {
      message.push({
        message: "El tipo de documento es requerido.",
        typeMessage: "¡Danger! ",
        typeClass: "alert alert-danger",
      });
    }
    if (user.document_number == "") {
      message.push({
        message: "El número de documento es requerido.",
        typeMessage: "¡Danger! ",
        typeClass: "alert alert-danger",
      });
    }
    if (user.first_name == "") {
      message.push({
        message: "Los nombres son requeridos",
        typeMessage: "¡Danger! ",
        typeClass: "alert alert-danger",
      });
    }
    if (user.last_name == "") {
      message.push({
        message: "Los apellidos son requeridos",
        typeMessage: "¡Danger! ",
        typeClass: "alert alert-danger",
      });
    }
    if (user.country == "") {
      message.push({
        message: "Es pais es requerido",
        typeMessage: "¡Danger! ",
        typeClass: "alert alert-danger",
      });
    }
    if (user.city == "") {
      message.push({
        message: "La ciudad es requerida",
        typeMessage: "¡Danger! ",
        typeClass: "alert alert-danger",
      });
    }
    if (user.address == "") {
      message.push({
        message: "La direccion es requerida",
        typeMessage: "¡Danger! ",
        typeClass: "alert alert-danger",
      });
    }
    if (user.mobile_phone == "") {
      message.push({
        message: "El númer de celular es requerido",
        typeMessage: "¡Danger! ",
        typeClass: "alert alert-danger",
      });
    }
    if (user.email == "") {
      message.push({
        message: "El email es requerido",
        typeMessage: "¡Danger! ",
        typeClass: "alert alert-danger",
      });
    }
    if (user.phone == "") {
      message.push({
        message: "El telefono fijo es requerido",
        typeMessage: "¡Danger! ",
        typeClass: "alert alert-danger",
      });
    }
    if (user.profile.code == "") {
      message.push({
        message: "El código de perfil es requerido",
        typeMessage: "¡Danger! ",
        typeClass: "alert alert-danger",
      });
    }
    if (user.type == "") {
      message.push({
        message: "El tipo de cliente es requerido.",
        typeMessage: "¡Danger! ",
        typeClass: "alert alert-danger",
      });
    }
    if (user.maximum_amount_operations == "") {
      message.push({
        message: "El monto máximo de operaciones es requerido.",
        typeMessage: "¡Danger! ",
        typeClass: "alert alert-danger",
      });
    }
    if (!emailRegex.test(user.email)) {
      message.push({
        message: "Ingrese un email valido.",
        typeMessage: "¡Danger! ",
        typeClass: "alert alert-danger",
      });
    }
    if (!amountRegex.test(user.maximum_amount_operations)) {
      message.push({
        message: "Ingrese un monto limite de operaciones valido.",
        typeMessage: "¡Danger! ",
        typeClass: "alert alert-danger",
      });
    }
    if (!numberRegex.test(user.mobile_phone)) {
      message.push({
        message: "Ingrese un numero de celular valido.",
        typeMessage: "¡Danger! ",
        typeClass: "alert alert-danger",
      });
    }
    if (!numberRegex.test(user.phone)) {
      message.push({
        message: "Ingrese un numero de telèfono fijo valido.",
        typeMessage: "¡Danger! ",
        typeClass: "alert alert-danger",
      });
    }

    return message;
  }

  async _updateUser() {
    this._initLoading();

    let errores = this._errorOnUpdateUser();
    if (errores.length === 0) {
      let body = this._mapperUser();

      let response = await this.userDm.updateUser(body, body.id);

      if (response.status == 200) {
        this.errors = [];
        this.errors.push({
          message: "El registro se actualizó correctamente.",
          typeMessage: "¡Success! ",
          typeClass: "alert alert-success",
        });
        this._setEventReloadTableUser();
      } else {
        this.errors = [];
        this.errors.push({
          message: "No se pudo actualizar el registro.",
          typeMessage: "Danger! ",
          typeClass: "alert alert-danger",
        });
      }
    } else {
      this.errors = errores;
    }

    this._endLoading();
  }

  _setEventReloadTableUser() {
    let event = new CustomEvent("reload-table-user", {
      detail: { status: "200" },
    });
    this.dispatchEvent(event);
  }

  _cleanUser() {
    this.id = "";
    this.idClient = "";
    this.firstName = "";
    this.lastName = "";
    this.email = "";
    this.password = "";
    this.documentType = "";
    this.documentNumber = "";
    this.mobilePhone = "";
    this.address = "";
    this.phone = "";
    this.city = "";
    this.country = "";
    this.creationDate = "";
    this.updateDate = "";
    this.visibleAmounts = false;
    this.maximumAmountOperation = 0.0;
    this.type = "";
    this.profileCode = "";
    this.profileName = "";
    this.employeeCode = "";
    this.employeePosition = "";
    this.errors = [];
    this.isActiveSave = false;
    this.listCity = [];
    this.querySelector("#document_type").value = "";
    this.querySelector("#country").value = "";
    this.querySelector("#profile_code").value = "";
    this.querySelector("#type").value = "";
    this.querySelector("#city").value = "";
  }

  _exitUser() {
    this._cleanUser();
    this.renderRoot.querySelector("#idDialogUser").style.display = "none";
  }

  render() {
    return html`
      <div class="popup" id="idDialogUser">
        <div class="modal-dialog modal-xl" name="dialogUser">
          <div class="card">
          <div class="card-header text-dark bg-light text-uppercase font-weight-light">
          <h5 class="card-title">
            <svg class="bi" width="24" height="24" fill="currentColor">
              <use
                xlink:href="./resource/bootstrap-icons.svg#pencil-fill"
              />
            </svg>

            Usuario
          </h5>
        </div>
            <div class="card-body ">
              ${
                this.errors &&
                this.errors.map(
                  (error) => html`
                    <div class="${error.typeClass}">
                      <strong>${error.typeMessage}</strong> ${error.message}
                    </div>
                  `
                )
              }
              <form>
              <div class="form-row">
              <div class="form-group col-md-2">
                <label for="document_type">Tipo de documento</label>

                <select
                  name="document_type"
                  class="custom-select mr-sm-2"
                  id="document_type"
                  name="document_type"
                >
                  <option value="">--Seleccione uno--</option>
                  ${this.listDocumentType && this.listDocumentType.map(
                    (item) =>
                      html`<option .value="${item.parameter_value}">
                        ${item.parameter_name}
                      </option>`
                  )}
                </select>
              </div>
              <div class="form-group col-md-2">
                <label for="document_number">Nº de documento</label>
                <input
                  type="text"
                  class="form-control inputForm"
                  id="document_number"
                  name="document_number"
                  .value="${this.documentNumber}"
                />
              </div>
              <div class="form-group col-md-4">
                <label for="first_name">Nombres</label>
                <input
                  type="text"
                  class="form-control inputForm"
                  id="first_name"
                  name="first_name"
                  .value="${this.firstName}"
                />
              </div>

              <div class="form-group col-md-4">
                <label for="last_name">Apellidos</label>
                <input
                  type="text"
                  class="form-control inputForm"
                  id="last_name"
                  name="last_name"
                  .value="${this.lastName}"
                />
              </div>
            </div>
            <div class="form-row">
              <div class="form-group col-md-2">
                <label for="country">Pais</label>
                <select
                  name="country"
                  class="custom-select mr-sm-2"
                  id="country"
                  @input=${this._getLoadCity}
                >
                  <option value="">--Seleccione uno--</option>
                  ${this.listCountry && this.listCountry.map(
                    (item) =>
                      html`<option .value="${item.parameter_value}">
                        ${item.parameter_name}
                      </option>`
                  )}
                </select>
              </div>

              <div class="form-group col-md-2">
                <label for="city">Ciudad</label>
                <select
                  name="city"
                  class="custom-select mr-sm-2"
                  id="city"
                >
                  <option value="">--Seleccione uno--</option>
                  ${this.listCity && this.listCity.map(
                    (item) =>
                      html`<option .value="${item.parameter_value}">
                        ${item.parameter_name}
                      </option>`
                  )}
                </select>
              </div>
              <div class="form-group col-md-4">
                <label for="address">Dirección</label>
                <input
                  type="text"
                  class="form-control inputForm"
                  id="address"
                  name="address"
                  .value="${this.address}"
                />
              </div>
              <div class="form-group col-md-2">
                <label for="mobile_phone">N° Celular</label>
                <input
                  type="text"
                  class="form-control inputForm"
                  id="mobile_phone"
                  name="mobile_phone"
                  .value="${this.mobilePhone}"
                />
              </div>
              <div class="form-group col-md-2">
                <label for="phone">Teléfono fijo</label>
                <input
                  type="text"
                  class="form-control inputForm"
                  id="phone"
                  name="phone"
                  .value="${this.phone}"
                />
              </div>
              
            </div>
            <div class="form-row">
            <div class="form-group col-md-4">
              

            <label for="email">Email</label>
      <div class="input-group">
        <div class="input-group-prepend">
          <span class="input-group-text" id="validationTooltipUsernamePrepend">@</span>
        </div>
        <input type="text" class="form-control" id="email" .value="${
          this.email
        }" name="email" aria-describedby="validationTooltipUsernamePrepend" >
       
      </div>
              </div>
              
              <div class="form-group col-md-4">
                <label for="profile_code">Perfil</label>
                <select
                  name="profile_code"
                  class="custom-select mr-sm-2"
                  id="profile_code"
                >
                  <option value="">--Seleccione uno--</option>
                  ${this.listProfile && this.listProfile.map(
                    (item) =>
                      html`<option .value="${item.parameter_value}">
                        ${item.parameter_name}
                      </option>`
                  )}
                </select>
              </div>
              
              <div class="form-group col-md-2">
                <label for="type">Tipo cliente</label>
                <select
                  name="type"
                  class="custom-select mr-sm-2"
                  id="type"
                >
                  <option value="">--Seleccione uno--</option>
                  ${this.listType && this.listType.map(
                    (item) =>
                      html`<option .value="${item.parameter_value}">
                        ${item.parameter_name}
                      </option>`
                  )}
                </select>
              </div>
              <div class="form-group col-md-2">
                <label for="maximum_amount_operations">Monto Max. Ope.</label>
                <input 
                  type="text"
                  class="form-control inputForm"
                  id="maximum_amount_operations"
                  name="maximum_amount_operations"

                  data-inputmask="'alias': 'currency'"
                  .value="${this.maximumAmountOperation}"
                  
                />
              </div>
              
            </div>
            
            <div class="form-row">
            <div class="form-group col-md-2">
                <label for="employee_code">Cod. Empleado</label>
                <input
                  type="text"
                  class="form-control inputForm"
                  id="employee_code"
                  name="employee_code"
                  .value="${this.employeeCode}"
                />
              </div>
              <div class="form-group col-md-2">
                <label for="employee_position">Cargo</label>
                <input
                  type="text"
                  class="form-control inputForm"
                  id="employee_position"
                  name="employee_position"
                  .value="${this.employeePosition}"
                />
              </div>
            
              <div class="form-group col-md-2">
                <label for="id_client">Cod. Cliente</label>
                <input
                  type="text"
                  class="form-control inputForm"
                  id="id_client"
                  name="id_client"
                  .value="${this.idClient}"
                />
              </div>
              <div class="form-group col-md-2">
              <div class="custom-control custom-checkbox">
              <input class="form-check-input" type="checkbox" id="visible_amounts" ?checked="${
                this.visibleAmounts
              }" />
              <label class="form-check-label" for="visible_amounts">
                Activar visualizar montos.
              </label>
            </div>
        </div>   
        
        

        <div class="form-group col-md-2">
                <label for="creation_date">F. creación</label>
                <input ?disabled="${this.disabledDate}"
                  type="date"
                  id="creation_date"
                  name="creation_date"
                  class="form-control inputForm"
                  
                  .value="${this.creationDate}"
                />
              </div>
              <div class="form-group col-md-2">
                <label for="update_date">F. ultima modificación</label>
                <input  ?disabled="${this.disabledDate}"
                  type="date"
                  id="update_date"
                  name="update_date"
                  class="form-control inputForm"
                 
                  placeholder="yyyy-mm-dd"
                  .value="${this.updateDate}"
                />
              </div>
        
              </div>
          </form> 






              </form>
            </div>
            <div class="card-footer">
              <div class="card-body text-right">
               
                  <button
                    ?disabled="${this.isActiveSave}"
                    type="button"
                    class="btn btn-bbva btn-lg"
                    @click=${this._updateUser}
                  >
                    Guardar
                    <svg class="bi" width="24" height="24" fill="currentColor">
                      <use
                        xlink:href="./resource/bootstrap-icons.svg#box-arrow-in-down"
                      />
                    </svg>
                  </button>
                  <!--<button
                    type="button"
                    class="btn btn-dark"
                    @click=${this._cleanUser}
                  >
                    Limpiar
                    <svg class="bi" width="24" height="24" fill="currentColor">
                      <use
                        xlink:href="./resource/bootstrap-icons.svg#trash-fill"
                      />
                    </svg>
                  </button>-->
                  <button
                    type="button"
                    class="btn btn-bbva btn-lg"
                    data-dismiss="modal"
                    name="btnExitNewUser"
                    @click=${this._exitUser}
                  >
                    Salir
                    <svg class="bi" width="24" height="24" fill="currentColor">
                      <use
                        xlink:href="./resource/bootstrap-icons.svg#door-closed-fill"
                      />
                    </svg>
                  </button>
                
              </div>
            </div>
          </div>
        </div>
      </div>
    `;
  }
}

customElements.define("cmp-user", CmpUser);
