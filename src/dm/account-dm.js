class AccountDm {
    async getAccountByUserId(idUser) {
      let queryParams = 'https://fierce-reaches-83730.herokuapp.com/techU/v1/users/'+idUser+'/accounts';
      let listObjects = {};
      let response = await fetch(queryParams);
      if (response.ok) {
        if (response.status == 200) {
          let json = await response.json();
          listObjects.objects = json;
          listObjects.status = response.status;
        } else {
          listObjects.objects = [];
          listObjects.status = 204;
        }
      } else {
        listObjects.objects = [];
        listObjects.status = 500;
      }
      return listObjects;
    }

    async insertAccount(body,idUser) {
        let object={};
        let queryParams = 'https://fierce-reaches-83730.herokuapp.com/techU/v1/users/'+idUser+'/accounts';
        let response = await fetch(queryParams, {
          method: "POST",
          body: JSON.stringify(body), 
          headers: {
            "Content-Type": "application/json",
          }});
          if (response.ok) {
            if (response.status == 201) {
              let json = await response.json();
              object.json = json;
              object.status = response.status;
            } else{
              object.json = "{}";
              object.status = response.status;
            }
          }else{
            object.json = "{}";
            object.status = response.status;
          }
          return object;
      }

      async updateAccount(body,idUser,idAccount) {
        let object={};
        let queryParams = 'https://fierce-reaches-83730.herokuapp.com/techU/v1/users/'+idUser+'/accounts/'+idAccount;
        let response = await fetch(queryParams, {
          method: "PUT",
          body: JSON.stringify(body), 
          headers: {
            "Content-Type": "application/json",
          }});
          if (response.ok) {
            if (response.status == 200) {
              let json = await response.json();
              object.json = json;
              object.status = response.status;
            } else{
              object.json = "{}";
              object.status = response.status;
            }
          }else{
            object.json = "{}";
            object.status = response.status;
          }
          return object;
      }
  

      async insertTransaction(body,idAccount) {
        let object={};
        let queryParams = 'https://fierce-reaches-83730.herokuapp.com/techU/v1/accounts/'+idAccount+'/transactions';
        let response = await fetch(queryParams, {
          method: "POST",
          body: JSON.stringify(body), 
          headers: {
            "Content-Type": "application/json",
          }});
          if (response.ok) {
            if (response.status == 201) {
              let json = await response.json();
              object.json = json;
              object.status = response.status;
            } else{
              object.json = "{}";
              object.status = response.status;
            }
          }else{
            object.json = "{}";
            object.status = response.status;
          }
          return object;
      }
    
  }


  
  let accountDm = () => new AccountDm();
  export { accountDm };
  