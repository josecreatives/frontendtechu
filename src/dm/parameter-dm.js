class ParameterDm {
  async _getParameter() {
    let listObjects = {};
    let response = await fetch("https://fierce-reaches-83730.herokuapp.com/techU/v1/parameters");
    if (response.ok) {
      if (response.status == 200) {
        let json = await response.json();
        listObjects.objects = json;
        listObjects.status = response.status;
      } else {
        listObjects.objects = [];
        listObjects.status = 204;
      }
    } else {
      listObjects.objects = [];
      listObjects.status = 500;
    }
    return listObjects;
  }
}
let parameterDm = () => new ParameterDm();
export { parameterDm };
