class UserDm {
  async getUsers(params) {
    let queryParams = this.getUrlParams(
      "https://fierce-reaches-83730.herokuapp.com/techU/v1/users",
      params
    );
    let listObjects = {};
    let response = await fetch(queryParams);
    if (response.ok) {
      if (response.status == 200) {
        let json = await response.json();
        listObjects.objects = json;
        listObjects.status = response.status;
      } else {
        listObjects.objects = [];
        listObjects.status = 204;
      }
    } else {
      listObjects.objects = [];
      listObjects.status = 500;
    }
    return listObjects;
  }

  async getUserById(id) {
    let url = "https://fierce-reaches-83730.herokuapp.com/techU/v1/users/" + id;
   
    let listObjects = {};
    let response = await fetch(url, {
      method: "GET", 
      headers: {
        "Content-Type": "application/json",
      }});
    if (response.ok) {
      if (response.status == 200) {
        let json = await response.json();
        listObjects.objects = json;
        listObjects.status = response.status;
      } else {
        listObjects.objects = [];
        listObjects.status = 204;
      }
    } else {
      listObjects.objects = [];
      listObjects.status = 500;
    }
    return listObjects;
  }

  async singIn(detail){
    let url = "https://fierce-reaches-83730.herokuapp.com/techU/v1/singin";
    let listObjects = {};
    let response = await fetch(url, {
      method: "POST",
      body: JSON.stringify(detail), 
      headers: {
        "Content-Type": "application/json",
      }});
    if (response.ok) {
      if (response.status == 200) {
        let json = await response.json();
        listObjects.json = json;
        listObjects.status = response.status;
      } else {
        listObjects.json = {};
        listObjects.status = 204;
      }
    } else {
      listObjects.json = {};
      listObjects.status = 500;
    }
    console.log(listObjects);
    return listObjects;
  }
  
  async updateUser(body, id) {
    let object={};
    let url = "https://fierce-reaches-83730.herokuapp.com/techU/v1/users/" + id;
    let response = await fetch(url, {
      method: "PUT",
      body: JSON.stringify(body), 
      headers: {
        "Content-Type": "application/json",
      }});
      if (response.ok) {
        if (response.status == 200) {
          let json = await response.json();
          object.json = json;
          object.status = response.status;
        } else{
          object.json = "{}";
          object.status = response.status;
        }
      }else{
        object.json = "{}";
        object.status = response.status;
      }
      return object;
  }


  async deleteUser(id) {
    let object={};
    let url = "https://fierce-reaches-83730.herokuapp.com/techU/v1/users/" + id;
    let response = await fetch(url, {
      method: "DELETE",
      headers: {
        "Content-Type": "application/json",
      }});
      if (response.ok) {
        if (response.status == 204) {
          let json = {};
          object.json = json;
          object.status = response.status;
        } else{
          object.json = "{}";
          object.status = response.status;
        }
      }else{
        object.json = "{}";
        object.status = response.status;
      }
      return object;
  }



  async insertUser2(body) {
    let url = "https://fierce-reaches-83730.herokuapp.com/techU/v1/usersDB2";
    return await fetch(url, {
      method: "POST", // or 'PUT'
      body: JSON.stringify(body), // data can be `string` or {object}!
      headers: {
        "Content-Type": "application/json",
      },
    }).then((res) => ({
      jsonRes: res.json(),
      status: res.status,
      statusMessage: res.statusText,
    }));
  }

  async deleteUser2(id) {
    let url = "https://fierce-reaches-83730.herokuapp.com/techU/v1/usersDB/" + id;
    return await fetch(url, {
      method: "DELETE", // or 'PUT'
      headers: {
        "Content-Type": "application/json",
      },
    }).then((res) => ({
      jsonRes: res.json(),
      status: res.status,
      statusMessage: res.statusText,
    }));
  }

  

  getUrlParams(path, params) {
    let urlParams = Object.keys(params)
      .map(function (k) {
        return encodeURIComponent(k) + "=" + encodeURIComponent(params[k]);
      })
      .join("&");
    if (urlParams.length === 0) {
      return `${path}`;
    }
    return `${path}?${urlParams}`;
  }
}
let userDm = () => new UserDm();
export { userDm };
