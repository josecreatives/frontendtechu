class NumberUtil {
  _getTodayDate() {
    return this._formatDate(new Date());
  }

  _getPreMonthDate() {
    let endDate = new Date() - 24 * 30 * 24 * 60 * 60 * 1000;
    return this._formatDate(endDate);
  }

  _getStringDate() {
    var today = new Date();
    var dd = today.getDate();

    var mm = today.getMonth() + 1;
    var yyyy = today.getFullYear();
    if (dd < 10) {
      dd = "0" + dd;
    }

    if (mm < 10) {
      mm = "0" + mm;
    }
    today = mm + "-" + dd + "-" + yyyy;
    console.log(today);
    today = mm + "/" + dd + "/" + yyyy;
    console.log(today);
    today = dd + "-" + mm + "-" + yyyy;
    console.log(today);
    console.log(yyyy + "/" + mm + "/" + dd);
    return today = yyyy + "-" + mm + "-" + dd;
    
  }

  _formatDate(date) {
    var d = new Date(date),
      month = "" + (d.getMonth() + 1),
      day = "" + d.getDate(),
      year = d.getFullYear();

    if (month.length < 2) month = "0" + month;
    if (day.length < 2) day = "0" + day;

    return [year, month, day].join("-");
  }
  _zfill(number, width) {
    var numberOutput = Math.abs(number); /* Valor absoluto del número */
    var length = number.toString().length; /* Largo del número */
    var zero = "0"; /* String de cero */

    if (width <= length) {
      if (number < 0) {
        return "-" + numberOutput.toString();
      } else {
        return numberOutput.toString();
      }
    } else {
      if (number < 0) {
        return "-" + zero.repeat(width - length) + numberOutput.toString();
      } else {
        return zero.repeat(width - length) + numberOutput.toString();
      }
    }
  }
}
  let numberUtil = () => new NumberUtil();
  export { numberUtil };
  