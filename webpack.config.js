const HtmlWebpackPlugin = require('html-webpack-plugin');
const CopyWebpackPlugin = require('copy-webpack-plugin');

module.exports = ({ mode }) => {
    return {
        mode,
        plugins: [
            new HtmlWebpackPlugin({
                template: './index.html',
                filename: 'index.html',
                minify: {
                    collapseWhitespace: true
                }
            }),
            new CopyWebpackPlugin({
                patterns: [
                    {
                        context: 'node_modules/@webcomponents/webcomponentsjs',
                        from: '**/*.js',
                        to: 'webcomponents'
                    },
                    { 
                        from: 'resource', to: 'resource' }
                ]
            }),
        ],
        devtool: mode === 'development' ? 'source-map' : 'none'
    };
};

